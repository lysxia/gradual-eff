open import Type.Constants

module Gradual2CorePrecision (ℂ : Constants) where

open Constants ℂ

open import Type ℂ
open import Type.Precision ℂ
open import Type.Consistency ℂ
open import Type.Subtyping ℂ
open import Type.ConsistentSubtyping ℂ
open import Type.Context ℂ
open import Core ℂ as Core hiding (_＠_)
open import Core.Substitution ℂ
open import Gradual ℂ as Gradual
open import ContextPrecision ℂ
open import GradualPrecision ℂ
open import CorePrecision ℂ
open import Gradual2Core ℂ

open import Function.Base using (_∘_)
open import Data.Fin.Base using (Fin; suc)
open import Data.List.Base using (List; []; _∷_)
open import Data.List.Relation.Unary.All as All using (All; []; _∷_)
open import Data.List.Relation.Unary.Any using (here; there)
open import Data.Vec.Base as Vec using (Vec)
open import Data.Vec.Relation.Unary.All as VecAll using ([]; _∷_) renaming (All to VecAll)
open import Data.Nat.Base using (ℕ; zero; suc)
open import Data.Product using (_,_)
open import Relation.Nullary using (yes; no)
open import Relation.Binary.PropositionalEquality using (_≡_; refl)

private
  variable
    Γ Δ : Context
    A A₁ A₂ B B₁ B₂ A⇒B : Typeᵛ
    P P₁ P₂ Q Q₁ Q₂ : Typeᶜ
    E E₁ E₂ F F₁ F₂ : Effects
    e : 𝔼
    ι ι′ ι″ : Base

--

liftᶜ-≤ : ∀ {M₁ : Γ ⊢ᶜ P₁} {M₂ : Δ ⊢ᶜ P₂} →
  M₁ ≤-C M₂ →
  liftᶜ {A = A₁} M₁ ≤-C liftᶜ {A = A₂} M₂
liftᶜ-≤ = _

--

castsᶜ-≤ : ∀ {M₁ : Γ ⊢ᶜ P₁} {M₂ : Δ ⊢ᶜ P₂} {p₁ : P₁ ≲ᶜ Q₁} {p₂ : P₂ ≲ᶜ Q₂} →
  Q₁ ≤ᶜ Q₂ →
  M₁ ≤-C M₂ →
  castsᶜ p₁ M₁ ≤-C castsᶜ p₂ M₂
castsᶜ-≤ = _

{- Gradual should be replaced something closer to Core, just not fine-grained CBV and as an extrinsinc typing relation
translate-≤ : ∀ {M₁ M₂} →
  Γ ≤ᴳ Δ →
  Γ ⊢ P₁ ⦂ M₁ ≤ᵍ Δ ⊢ P₂ ⦂ M₂ →
  --------------------------
  translate M₁ ≤-C translate M₂
translate-≤ Γ≤ (Var E≤F v) = return E≤F (` v)
translate-≤ Γ≤ (perform e∈¿E₁ e∈¿E₂ M) = translate-≤ Γ≤ M >>= perform (` Z) ＠⟨ _≤ᶜ_.effects (≤ᵍ-≤ Γ≤ M) ⟩ ≤-refl
translate-≤ Γ≤ (N · M) = translate-≤ Γ≤ N >>= (liftᶜ-≤ (translate-≤ Γ≤ M) >>= (` S Z · ` Z))
translate-≤ Γ≤ (_＠_ {p₁ = p₁} {p₂ = p₂} M Q≤) =
  let M = (translate-≤ Γ≤ M) in castsᶜ-≤ {p₁ = p₁} {p₂ = p₂} Q≤ M
translate-≤ Γ≤ (Let M N) = translate-≤ Γ≤ M >>= translate-≤ (Γ≤ ▷ _≤ᶜ_.returns (≤ᵍ-≤ Γ≤ M)) N
-}
