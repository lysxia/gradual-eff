open import Type.Constants
module Static (ℂ : Constants) where

open Constants ℂ

open import Type ℂ
import Type.Context
open import Type.Precision ℂ
open import Type.ConsistentSubtyping ℂ
open import Untyped ℂ
import Gradual3
open module Gradual = Gradual3 ℂ using (Context; ∅; _▷_⦂_; _∋_⦂_; Z; S_⦂_; Hooks)

open import Function.Base using (_∘_; case_of_)
open import Data.String.Base as String using (String)
open import Data.List.Base as List using (List; []; _∷_; _++_)
open import Data.List.Relation.Unary.All using (All; []; _∷_)
open import Data.Empty using (⊥)
open import Data.Product using (_×_; _,_; proj₁; proj₂; Σ; ∃; Σ-syntax; ∃-syntax)
open import Relation.Binary.PropositionalEquality using (_≡_; _≢_; refl)
open import Data.Vec.Base using (Vec)

private
  variable
    Γ : Context
    A B : Typeᵛ
    P Q : Typeᶜ
    E Eh F : Effects
    X Y : Name
    ?X ?Xf ?Y : Pat
    M N : Term
    H : Handler
    e : 𝔼
    ι : Base

infixl 2 _⊢_⦂_ _⊢ʰ_⦂_⟹_

data _⊢_⦂_ (Γ : Context) : Term → Typeᶜ → Set
data _⊢ʰ_⦂_⟹_ (Γ : Context) : Handler → Typeᵛ → Typeᶜ → Set

data _⊢_⦂_ Γ where
  Var :
    Γ ∋ X ⦂ A →
    -------------------
    Γ ⊢ Var X ⦂ ⟨ ε ⟩ A

  App : ∀ {E F} →
    Γ ⊢ N ⦂ ⟨ ¡ E ⟩ A ⇒ ⟨ ¡ F ⟩ B →
    F ⊆ E →
    Γ ⊢ M ⦂ ⟨ ¡ E ⟩ A →
    ---------------------
    Γ ⊢ App N M ⦂ ⟨ ¡ E ⟩ B

  Prf : ∀ {E} →
    e ∈ E →
    Γ ⊢ M ⦂ ⟨ ¡ E ⟩ request e →
    ------------------------------
    Γ ⊢ Prf e M ⦂ ⟨ ¡ E ⟩ response e

  $_ : ∀ (κ : rep ι) →
    -------------------
    Γ ⊢ $ κ ⦂ ⟨ ε ⟩ $ ι

  Lam :
    Γ ▷ ?X ⦂ A ⊢ M ⦂ P →
    ----------------------------
    Γ ⊢ Lam ?X A M ⦂ ⟨ ε ⟩ (A ⇒ P)

  Rec :
    Γ ▷ ?Xf ⦂ A ⇒ P ▷ ?X ⦂ A ⊢ M ⦂ P →
    ----------------------------
    Γ ⊢ Rec ?Xf ?X A P M ⦂ ⟨ ε ⟩ (A ⇒ P)

  Let : ∀ {E} →
    Γ         ⊢ M ⦂ ⟨ ¡ E ⟩ A →
    Γ ▷ ?X ⦂ A ⊢ N ⦂ ⟨ ¡ E ⟩ B →
    -------------------------
    Γ ⊢ Let ?X M N ⦂ ⟨ ¡ E ⟩ B

  Han : ∀ {E} →
    (let Eh = Hooks H) →
    Γ ⊢ M ⦂ ⟨ ¡ (Eh ++ E) ⟩ A →
    Γ ⊢ʰ H ⦂ A ⟹ ⟨ ¡ E ⟩ B →
    ----------------------
    Γ ⊢ Han M H ⦂ ⟨ ¡ E ⟩ B

  Annᵛ :
    Γ ⊢ M ⦂ ⟨ E ⟩ A →
    ---------------
    Γ ⊢ Annᵛ M A ⦂ ⟨ E ⟩ A

  Annᵉ :
    Γ ⊢ M ⦂ ⟨ F ⟩ A →
    ---------------
    Γ ⊢ Annᵉ M E ⦂ ⟨ E ⟩ A

PerformClause : ∀ (Γ : Context) (P : Typeᶜ) → 𝔼 × Pat × Pat × Term -> Set
PerformClause Γ P = (λ{ (e , ?X , ?K , M) → (Γ ▷ ?X ⦂ request e ▷ ?K ⦂ response e ⇒ P) ⊢ M ⦂ P})

data _⊢ʰ_⦂_⟹_ Γ where
  MkHandler :
    (let record { return-clause = (?X , M) ; perform-clauses = Ms } = H) →
    Γ ▷ ?X ⦂ A ⊢ M ⦂ P →
    All (PerformClause Γ P) Ms →
    ---------------------------------
    Γ ⊢ʰ H ⦂ A ⟹ P

data Staticᵉ : Effects → Set where
  ¡_ : ∀ E → Staticᵉ (¡ E)

data Staticᵛ : Typeᵛ → Set
record Staticᶜ (P : Typeᶜ) : Set

data Staticᵛ where
  $_ : ∀ ι → Staticᵛ ($ ι)
  _⇒_ : Staticᵛ A → Staticᶜ P → Staticᵛ (A ⇒ P)

record Staticᶜ P where
  inductive
  constructor ⟨_⟩_
  field
    effects : Staticᵉ (effects P)
    returns : Staticᵛ (returns P)
  
data Annotated : Term → Set
record AnnotatedHandler (H : Handler) : Set

data Annotated where
  Var : ∀ X → Annotated (Var X)
  Let : Annotated M → Annotated N → Annotated (Let ?X M N)
  $_ : (κ : rep ι) → Annotated ($ κ)
  App : Annotated N → Annotated M → Annotated (App N M)
  Annᵛ : Annotated M → Staticᵛ A → Annotated (Annᵛ M A)
  Annᵉ : Annotated M → Staticᵉ E → Annotated (Annᵉ M E)
  Lam : Staticᵛ A → Annotated M → Annotated (Lam ?X A M)
  Rec : Staticᵛ A → Staticᶜ P → Annotated M → Annotated (Rec ?Xf ?X A P M)
  Han : Annotated M → AnnotatedHandler H → Annotated (Han M H)
  Prf : Staticᵛ (request e) → Staticᵛ (response e) → Annotated M → Annotated (Prf e M)

AnnotatedClause : 𝔼 × Pat × Pat × Term → Set
AnnotatedClause (e , _ , _ , M) = Staticᵛ (request e) × Staticᵛ (response e) × Annotated M

record AnnotatedHandler H where
  inductive
  field
    return-clause : Annotated (proj₂ (Handler.return-clause H))
    perform-clauses : All AnnotatedClause (Handler.perform-clauses H)

open AnnotatedHandler

infixl 2 _▷_

data StaticContext : Context → Set where
  ∅ : StaticContext ∅
  _▷_ : StaticContext Γ → Staticᵛ A → StaticContext (Γ ▷ ?X ⦂ A)

Static-∋ : StaticContext Γ → Γ ∋ X ⦂ A → Staticᵛ A
Static-∋ (Γ ▷ A) Z = A
Static-∋ (Γ ▷ A) (S _ ⦂ x) = Static-∋ Γ x

Static-≤ᵉ : E ≤ᵉ F → Staticᵉ F → E ≡ F
Static-≤ᵉ ¡¡ (¡ _) = refl

Static-≤ : A ≤ B → Staticᵛ B → A ≡ B
Static-≤ᶜ : P ≤ᶜ Q → Staticᶜ Q → P ≡ Q

Static-≤ ($ _) ($ _) = refl
Static-≤ (A≤ ⇒ P≤) (A ⇒ P) with Static-≤ A≤ A | Static-≤ᶜ P≤ P
... | refl | refl = refl
-- TODO: sum and tycon

Static-≤ᶜ (⟨ ¡¡ ⟩ A≤) (⟨ ¡ E ⟩ A) with Static-≤ A≤ A
... | refl = refl

open Gradual hiding (PerformClause) renaming (_⊢_⦂_ to _⊢ᵍ_⦂_; _⊢ʰ_⦂_⟹_ to _⊢ᵍʰ_⦂_⟹_)

toStatic : StaticContext Γ → Annotated M → Γ ⊢ᵍ M ⦂ P → (Γ ⊢ M ⦂ P) × Staticᶜ P
toStaticʰ : StaticContext Γ → Staticᵛ A → AnnotatedHandler H → Γ ⊢ᵍʰ H ⦂ A ⟹ P → (Γ ⊢ʰ H ⦂ A ⟹ P) × Staticᶜ P

toStatic Γ (Var X) (Var ∋X) = Var ∋X , ⟨ ¡ [] ⟩ Static-∋ Γ ∋X

toStatic Γ (App M N) (App ⊢M F⊆E ⊢N) with toStatic Γ M ⊢M | toStatic Γ N ⊢N
toStatic Γ (App _ _) (App ⊢M (¡¡ F⊆E) ⊢N) | M , ⟨ _ ⟩ (A ⇒ ⟨ ¡ F ⟩ B) | N , ⟨ E ⟩ _ = App M F⊆E N , ⟨ E ⟩ B

toStatic Γ (Lam A M) (Lam ⊢M) with toStatic (Γ ▷ A) M ⊢M
... | ⊢M , P = Lam ⊢M , ⟨ ¡ [] ⟩ A ⇒ P

toStatic Γ (Rec A P M) (Rec ⊢M) with toStatic (Γ ▷ A ⇒ P ▷ A) M ⊢M
... | ⊢M , P = Rec ⊢M , ⟨ ¡ [] ⟩ A ⇒ P

toStatic Γ (Annᵛ M A) (Annᵛ ⊢M ≤A) with toStatic Γ M ⊢M | Static-≤ ≤A A
... | ⊢M , ⟨ E ⟩ _ | refl = Annᵛ ⊢M , ⟨ E ⟩ A
toStatic Γ (Annᵉ M E) (Annᵉ ⊢M ≤E) with toStatic Γ M ⊢M | Static-≤ᵉ ≤E E
... | ⊢M , ⟨ _ ⟩ A | refl = Annᵉ ⊢M , ⟨ E ⟩ A
toStatic Γ (Let M N) (Let ⊢M ⊢N) with toStatic Γ M ⊢M
... | ⊢M , ⟨ ¡ _ ⟩ A with toStatic (Γ ▷ A) N ⊢N
... | ⊢N , P = Let ⊢M ⊢N , P
toStatic Γ (Prf req-e res-e M) (Prf e∈E ⊢M) with e∈E | toStatic Γ M ⊢M
... | ∈¿¡ e∈E | ⊢M , ⟨ ¡ E ⟩ _ = Prf e∈E ⊢M , ⟨ ¡ E ⟩ res-e
toStatic Γ (Han M H) (Han ⊢M ⊢H) with toStatic Γ M ⊢M
... | ⊢M , ⟨ _ ⟩ A with toStaticʰ Γ A H ⊢H
... | ⊢H , P@(⟨ ¡ _ ⟩ _) = Han ⊢M ⊢H , P
toStatic Γ ($ κ) ($ _) = $ κ , ⟨ ¡ [] ⟩ $ _
toStatic Γ M (_⇑ ⊢M ≤P) with toStatic Γ M ⊢M
... | ⊢M , P = case Static-≤ᶜ ≤P P of λ{ refl → ⊢M , P }

toStaticʰ Γ A H (MkHandler ⊢M ⊢Ns) with toStatic (Γ ▷ A) (return-clause H) ⊢M
... | ⊢M , P = MkHandler ⊢M (toStatics (perform-clauses H) ⊢Ns) , P
  where
    toStatics : ∀ {Ns} → All AnnotatedClause Ns → All (Gradual.PerformClause _ _) Ns → All (PerformClause _ _) Ns
    toStatics [] [] = []
    toStatics ((B₁ , B₂ , N) ∷ Ns) (⊢N ∷ ⊢Ns) = proj₁ (toStatic (Γ ▷ B₁ ▷ (B₂ ⇒ P)) N ⊢N) ∷ toStatics Ns ⊢Ns
