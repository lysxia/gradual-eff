open import Type.Constants
module Gradual (ℂ : Constants) where

open Constants ℂ

open import Type ℂ
open import Type.Context ℂ
open import Type.ConsistentSubtyping ℂ

open import Data.Fin.Base using (Fin)
open import Data.String using (String)
open import Data.List.Base as List using (List)
open import Data.List.Relation.Unary.All using (All)
import Data.Vec.Base as Vec
open import Data.Vec.Relation.Unary.All as VecAll using () renaming (All to VecAll)
open import Data.Maybe using (Maybe; just; nothing)
open import Data.Product using (_×_; _,_; proj₁; proj₂; Σ; ∃; Σ-syntax; ∃-syntax)
open import Relation.Binary.PropositionalEquality using (_≡_)
open import Relation.Nullary using (Dec; yes; no)
open import Relation.Binary using (Decidable)

infix  4 _⊢_ _⊢_/_==>_
infix  6 _·_
infix  6 _⦅_⦆_
infix  8 `_
infixl 5 _＠_

data _⊢_ (Γ : Context) : Typeᶜ → Set
record _⊢_/_==>_ (Γ : Context) (A : Typeᵛ) (Eh : List 𝔼) (Q : Typeᶜ) : Set

private
  variable
    Γ : Context
    A A₁ A₂ B A⇒B : Typeᵛ
    P Q : Typeᶜ
    E F : Effects
    e : 𝔼
    ι ι′ ι″ : Base

data _⊢_ Γ where
  `_ :
      Γ ∋ A
      -----
    → Γ ⊢ ⟨ E ⟩ A

  _·_ :
      Γ ⊢ ⟨ E ⟩ A ⇒ ⟨ E ⟩ B
    → Γ ⊢ ⟨ E ⟩ A
      ---------
    → Γ ⊢ ⟨ E ⟩ B

  _⦅_⦆_ :
      Γ ⊢ ⟨ E ⟩ $ ι
    → (rep ι → rep ι′ → rep ι″)
    → Γ ⊢ ⟨ E ⟩ $ ι′
      ----------------------
    → Γ ⊢ ⟨ E ⟩ $ ι″

  perform :
      e ∈¿ E
    → Γ ⊢ ⟨ E ⟩ request e
      -----
    → Γ ⊢ ⟨ E ⟩ response e

  ƛ_ :
      Γ ▷ (A ⇒ P) ▷ A ⊢ P
      ---------
    → Γ ⊢ ⟨ E ⟩ A ⇒ P

  Let : (let ⟨ E ⟩ _ = P)
    → Γ ⊢ ⟨ E ⟩ A
    → Γ ▷ A ⊢ P
      ---------
    → Γ ⊢ P

  $_ :
      rep ι
      -------
    → Γ ⊢ ⟨ E ⟩ $ ι

  if : (let ⟨ E ⟩ _ = P)
    → Γ ⊢ ⟨ E ⟩ $ ′𝔹
    → (M N : Γ ⊢ P)
      ------
    → Γ ⊢ P

  handle_by_ : ∀ {Eh}
      (let ⟨ E ⟩ _ = P)
      (let Q = ⟨ Eh ++¿ E ⟩ A)
    → Γ ⊢ Q
    → Γ ⊢ A / Eh ==> P
      -----
    → Γ ⊢ P

  _,_ :
      Γ ⊢ ⟨ E ⟩ A₁
    → Γ ⊢ ⟨ E ⟩ A₂
      ------
    → Γ ⊢ ⟨ E ⟩ (A₁ ′× A₂)

  unpair : (let ⟨ E ⟩ _ = P)
    → Γ ⊢ ⟨ E ⟩ (A₁ ′× A₂)
    → Γ ▷ A₁ ▷ A₂ ⊢ P
      -----
    → Γ ⊢ P

  con : ∀ {Cs As}
    → (i : Fin (List.length Cs))
    → Γ ⊢ ⟨ E ⟩ Vec.lookup As i
    → Γ ⊢ ⟨ E ⟩ sum Cs As

  Case : ∀ (let ⟨ E ⟩ _ = P) {Cs As}
    → Γ ⊢ ⟨ E ⟩ sum Cs As
    → VecAll (λ A → Γ ▷ A ⊢ P) As
      ------
    → Γ ⊢ P

  wrap : {T : 𝕋} {As : TySub (arity T) ∅}
    → Γ ⊢ ⟨ E ⟩ subTy As (tyDef T)
    → Γ ⊢ ⟨ E ⟩ tycon T As

  Unwrap : ∀ {T As}
    → Γ ⊢ ⟨ E ⟩ tycon T As
      --------------
    → Γ ⊢ ⟨ E ⟩ subTy As (tyDef T)

  -- Type annotation (M : ⟨ F ⟩ A)
  _＠_ :
      Γ ⊢ P
    → P ≲ᶜ Q
      ------
    → Γ ⊢ Q

Perform-Clauses : List 𝔼 → Context → Typeᶜ → Set
Perform-Clauses Hooks Γ Q = All (λ e → Γ ▷ request e ▷ (response e ⇒ Q) ⊢ Q) Hooks

record _⊢_/_==>_ Γ A Hooks Q where
  inductive
  field
    return-clause : Γ ▷ A ⊢ Q
    perform-clauses : Perform-Clauses Hooks Γ Q

open _⊢_/_==>_ public
