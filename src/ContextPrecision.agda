open import Type.Constants
module ContextPrecision (ℂ : Constants) where

open Constants ℂ

open import Type ℂ
open import Type.Context ℂ
open import Type.Precision ℂ

private
  variable
    Γ Δ : Context
    A A₁ A₂ B B₁ B₂ A⇒B : Typeᵛ
    P P₁ P₂ Q Q₁ Q₂ : Typeᶜ
    E E₁ E₂ F F₁ F₂ : Effects
    e : 𝔼
    ι ι′ ι″ : Base

infix 1 _≤ᴳ_

data _≤ᴳ_ : (_ _ : Context) → Set where
  ∅ : ∅ ≤ᴳ ∅
  _▷_ : Γ ≤ᴳ Δ → A₁ ≤ A₂ → Γ ▷ A₁ ≤ᴳ Δ ▷ A₂

infix 1 _∋_⦂_≤ˣ_∋_⦂_ _≤ˣ_

data _∋_⦂_≤ˣ_∋_⦂_ : ∀ Γ A₁ → Γ ∋ A₁ → ∀ Δ A₂ → Δ ∋ A₂ → Set where
  Z :
    Γ ▷ A₁ ∋ A₁ ⦂ Z ≤ˣ Δ ▷ A₂ ∋ A₂ ⦂ Z
  S_ : ∀ {v₁ v₂} →
    Γ ∋ A₁ ⦂ v₁ ≤ˣ Δ ∋ A₂ ⦂ v₂ →
    Γ ▷ B₁ ∋ A₁ ⦂ S v₁ ≤ˣ Δ ▷ B₂ ∋ A₂ ⦂ S v₂

_≤ˣ_ : Γ ∋ A₁ → Δ ∋ A₂ → Set
_≤ˣ_ = _ ∋ _ ⦂_≤ˣ _ ∋ _ ⦂_

≤ˣ-≤ : ∀ {v₁ v₂} → Γ ≤ᴳ Δ → Γ ∋ A₁ ⦂ v₁ ≤ˣ Δ ∋ A₂ ⦂ v₂ → A₁ ≤ A₂
≤ˣ-≤ (_ ▷ A≤) Z = A≤
≤ˣ-≤ (Γ≤ ▷ _) (S v) = ≤ˣ-≤ Γ≤ v

≤ˣ-refl : ∀ {v₁} → Γ ∋ A₁ ⦂ v₁ ≤ˣ Γ ∋ A₁ ⦂ v₁
≤ˣ-refl {v₁ = Z} = Z
≤ˣ-refl {v₁ = S v} = S ≤ˣ-refl
