open import Type.Constants
module GradualPrecision (ℂ : Constants) where

open Constants ℂ

open import Type ℂ
open import Type.Context ℂ
open import Type.Precision ℂ
open import Type.ConsistentSubtyping ℂ
open import Gradual ℂ
open import ContextPrecision ℂ

private
  variable
    Γ Δ : Context
    A A₁ A₂ B B₁ B₂ A⇒B : Typeᵛ
    P P₁ P₂ Q Q₁ Q₂ : Typeᶜ
    E E₁ E₂ F F₁ F₂ : Effects
    e : 𝔼
    ι ι′ ι″ : Base

infix 2 _⊢_⦂_

data GradualTerm : Set where
  _⊢_⦂_ : ∀ Γ P → Γ ⊢ P → GradualTerm

infix 1 _≤ᵍ_

data _≤ᵍ_ : (_ _ : GradualTerm) → Set where
  Var : ∀ {v₁ v₂} →
    E ≤ᵉ F →
    Γ ∋ A ⦂ v₁ ≤ˣ Δ ∋ B ⦂ v₂ →
    ----------------------------
    Γ ⊢ ⟨ E ⟩ A ⦂ ` v₁ ≤ᵍ Δ ⊢ ⟨ F ⟩ B ⦂ ` v₂

  _·_ : ∀ {N₁ N₂ M₁ M₂} →
    Γ ⊢ ⟨ E₁ ⟩ A₁ ⇒ ⟨ E₁ ⟩ B₁ ⦂ N₁ ≤ᵍ Δ ⊢ ⟨ E₂ ⟩ A₂ ⇒ ⟨ E₂ ⟩ B₂ ⦂ N₂ →
    Γ ⊢ ⟨ E₁ ⟩ A₁ ⦂ M₁ ≤ᵍ Δ ⊢ ⟨ E₂ ⟩ A₂ ⦂ M₂ →
    -------------------------
    Γ ⊢ ⟨ E₁ ⟩ B₁ ⦂ N₁ · M₁ ≤ᵍ Δ ⊢ ⟨ E₂ ⟩ B₂ ⦂ N₂ · M₂

  perform : ∀ {e M₁ M₂} →
    (e₁ : e ∈¿ E₁) →
    (e₂ : e ∈¿ E₂) →
    Γ ⊢ ⟨ E₁ ⟩ request e ⦂ M₁ ≤ᵍ Δ ⊢ ⟨ E₂ ⟩ request e ⦂ M₂ →
    -----
    Γ ⊢ ⟨ E₁ ⟩ response e ⦂ perform e₁ M₁ ≤ᵍ Δ ⊢ ⟨ E₂ ⟩ response e ⦂ perform e₂ M₂

  Let : ∀ {M₁ M₂ N₁ N₂} →
    Γ ⊢ ⟨ E₁ ⟩ A₁ ⦂ M₁ ≤ᵍ Δ ⊢ ⟨ E₂ ⟩ A₂ ⦂ M₂ →
    Γ ▷ A₁ ⊢ ⟨ E₁ ⟩ B₁ ⦂ N₁ ≤ᵍ Δ ▷ A₂ ⊢ ⟨ E₂ ⟩ B₂ ⦂ N₂ →
    ------
    Γ ⊢ ⟨ E₁ ⟩ B₁ ⦂ Let M₁ N₁ ≤ᵍ Δ ⊢ ⟨ E₂ ⟩ B₂ ⦂ Let M₂ N₂

  _＠_ : ∀ {M₁ M₂}
    {p₁ : P₁ ≲ᶜ Q₁}
    {p₂ : P₂ ≲ᶜ Q₂} →
    Γ ⊢ P₁ ⦂ M₁ ≤ᵍ Δ ⊢ P₂ ⦂ M₂ →
    Q₁ ≤ᶜ Q₂ →
    ------
    Γ ⊢ Q₁ ⦂ M₁ ＠ p₁ ≤ᵍ Δ ⊢ Q₂ ⦂ M₂ ＠ p₂

≤ᵍ-≤ : ∀ {M₁ M₂} → Γ ≤ᴳ Δ → Γ ⊢ P₁ ⦂ M₁ ≤ᵍ Δ ⊢ P₂ ⦂ M₂ → P₁ ≤ᶜ P₂
≤ᵍ-≤ Γ≤ (Var E≤F v) = ⟨ E≤F ⟩ ≤ˣ-≤ Γ≤ v
≤ᵍ-≤ Γ≤ (N · M) with ≤ᵍ-≤ Γ≤ N
... | ⟨ _ ⟩ (_ ⇒ P≤) = P≤
≤ᵍ-≤ Γ≤ (perform _ _ M) with ≤ᵍ-≤ Γ≤ M
... | ⟨ E≤ ⟩ _ = ⟨ E≤ ⟩ ≤-refl
≤ᵍ-≤ Γ≤ (Let M N) with ≤ᵍ-≤ Γ≤ M
... | ⟨ _ ⟩ A≤ = ≤ᵍ-≤ (Γ≤ ▷ A≤) N
≤ᵍ-≤ Γ≤ (_ ＠ Q≤) = Q≤
