open import Type.Constants

module Type.ConsistentSubtyping (ℂ : Constants) where

open Constants ℂ

open import Type ℂ
open import Type.Precision ℂ
open import Type.Consistency ℂ
open import Type.Subtyping ℂ
open import Utils using (_≟_; _⊆_; _⊆?_; ⊆-refl; _yes→_no→_; _Reflects_⟵_; _Reflects₂_×_⟵_)

open import Level using () renaming (zero to ℓ₀)
import Data.List.Base as List
open import Data.Vec.Base as Vec using (Vec; []; _∷_)
open import Data.Vec.Relation.Unary.All as Vec using (All; []; _∷_)
open import Data.Vec.Relation.Binary.Pointwise.Inductive as Vec using (Pointwise; []; _∷_)
open import Data.Product using (_×_; _,_; proj₁; proj₂; <_,_>; uncurry; Σ; ∃; Σ-syntax; ∃-syntax)
open import Function.Base using (_∘_; flip)
open import Relation.Nullary using (Dec; yes; no)
import Relation.Nullary.Decidable as Dec
open import Relation.Nullary.Product using (_×-dec_)
open import Relation.Binary using (Rel)
open import Relation.Binary.Construct.Composition using (_;_)
open import Relation.Binary
  using (Rel; Reflexive; Transitive; Decidable; _Preserves_⟶_; _Preserves₂_⟶_⟶_)
  renaming (_⇒_ to _⟹_)
open import Relation.Binary.PropositionalEquality using (_≡_; refl)

private
  variable
    E F : Effects
    P Q : Typeᶜ
    A A₁ A₂ B B₁ B₂ : Typeᵛ
    ι : Base

-- * Consistent subtyping

infix 4 _≲_ _≲ᶜ_

data _≲_ : Rel Typeᵛ ℓ₀
record _≲ᶜ_ (P Q : Typeᶜ) : Set

_≲+_ : ∀ {n} → Rel (Vec Typeᵛ n) ℓ₀
_≲+_ = Pointwise _≲_

data _≲_ where
  ≲★ : A ≲ ★
  ★≲ : ★ ≲ A
  $_ : ∀ ι
      ---------
    → $ ι ≲ $ ι
  _⇒_ :
      B ≲  A
    → P ≲ᶜ Q
      -------
    → A ⇒ P ≲ B ⇒ Q
  _′×_ :
      A₁ ≲ B₁
    → A₂ ≲ B₂
      -------
    → A₁ ′× A₂ ≲ B₁ ′× B₂
  sum : ∀ {Cs As Bs}
    → As ≲+ Bs
      -----
    → sum Cs As ≲ sum Cs Bs
  tycon : ∀ {T As}
      -----
    → tycon T As ≲ tycon T As

record _≲ᶜ_ P Q where
  inductive
  constructor ⟨_⟩_
  field
    effects : P .effects ⊆¿ Q .effects
    returns : P .returns ≲  Q .returns

≲-refl : Reflexive _≲_

≲ᶜ-refl : Reflexive _≲ᶜ_
≲ᶜ-refl = ⟨ ⊆¿-refl ⟩ ≲-refl

≲+-refl : ∀ {n} → Reflexive (_≲+_ {n})
≲+-refl {_} {[]} = []
≲+-refl {_} {A ∷ As} = ≲-refl ∷ ≲+-refl

≲-refl {★} = ≲★
≲-refl {$ ι} = $ ι
≲-refl {A ⇒ P} = ≲-refl ⇒ ≲ᶜ-refl
≲-refl {A ′× B} = ≲-refl ′× ≲-refl
≲-refl {tycon T As} = tycon
≲-refl {sum Cs As} = sum ≲+-refl

infix 4 _≲?_ _≲ᶜ?_

_≲?_ : Decidable _≲_
_≲ᶜ?_ : Decidable _≲ᶜ_

_≲+?_ : ∀ {n} → Decidable (_≲+_ {n = n})
_≲+?_ [] [] = yes []
_≲+?_ (A ∷ As) (B ∷ Bs) with A ≲? B ×-dec As ≲+? Bs
... | yes (≲B , ≲Bs) = yes (≲B ∷ ≲Bs)
... | no ¬≲ = no λ{ (≲B ∷ ≲Bs) → ¬≲ (≲B , ≲Bs) }

★ ≲? _ = yes ★≲
_ ≲? ★ = yes ≲★
$ ι ≲? $ ι′ with ι ≟ ι′
... | yes refl = yes ($ ι)
... | no ¬≡ = no λ{ ($ _) → ¬≡ refl }
(A ⇒ P) ≲? (B ⇒ Q) = (B ≲? A ×-dec P ≲ᶜ? Q) yes→ uncurry _⇒_ no→ λ{ (A ⇒ P) → (A , P) }
(A ′× B) ≲? (A′ ′× B′) = (A ≲? A′ ×-dec B ≲? B′) yes→ uncurry _′×_ no→ λ{ (A ′× B) → (A , B) }
A@(tycon _ _) ≲? B@(tycon _ _) with A ≡? B
... | yes refl = yes tycon
... | no ¬≡ = no λ{ tycon → ¬≡ refl }
sum Cs As ≲? sum Ds Bs with Cs ≟ Ds
... | no ¬≡ = no λ{ (sum _) → ¬≡ refl }
... | yes refl = (As ≲+? Bs) yes→ sum no→ λ{ (sum As) → As }
$ _ ≲? _ ⇒ _ = no λ()
$ _ ≲? _ ′× _ = no λ()
_ ⇒ _ ≲? $ _ = no λ()
_ ′× _ ≲? $ _ = no λ()
_ ⇒ _ ≲? _ ′× _ = no λ()
_ ′× _ ≲? _ ⇒ _ = no λ()
tycon _ _ ≲? $ _ = no λ()
tycon _ _ ≲? _ ′× _ = no λ()
tycon _ _ ≲? _ ⇒ _ = no λ()
tycon _ _ ≲? sum _ _ = no λ()
_ ⇒ _  ≲? tycon _ _ = no λ()
_ ′× _ ≲? tycon _ _ = no λ()
$ _    ≲? tycon _ _ = no λ()
sum _ _ ≲? $ _ = no λ()
sum _ _ ≲? _ ′× _ = no λ()
sum _ _ ≲? _ ⇒ _ = no λ()
sum _ _ ≲? tycon _ _ = no λ()
_ ⇒ _  ≲? sum _ _ = no λ()
_ ′× _ ≲? sum _ _ = no λ()
$ _    ≲? sum _ _ = no λ()

⟨ E ⟩ A ≲ᶜ? ⟨ F ⟩ B = (E ⊆¿? F ×-dec A ≲? B) yes→ uncurry ⟨_⟩_ no→ λ{ (⟨ E ⟩ A) → (E , A) }

⊆¿-≥⊑≤ : _⊆¿_ ⟹ (flip _≤ᵉ_ ; (_⊑ᵉ_ ; _≤ᵉ_))
⊆¿-≥⊑≤ (¡¡ E⊆F) = _ , ¡¡ , _ , ¡ E⊆F    , ¡¡
⊆¿-≥⊑≤ ¡¿       = _ , ¡¡ , _ , ¡ ⊆-refl , ¡¿
⊆¿-≥⊑≤ ¿¡       = _ , ¡¿ , _ , ¡ ⊆-refl , ¡¡
⊆¿-≥⊑≤ ¿¿       = _ , ¿¿ , _ , ¿        , ¿¿

≲-≥⊑≤ : _≲_ ⟹ (flip _≤_ ; (_⊑_ ; _≤_))

≲-≥⊑≤ᶜ : _≲ᶜ_ ⟹ (flip _≤ᶜ_ ; (_⊑ᶜ_ ; _≤ᶜ_))
≲-≥⊑≤ᶜ (⟨ E ⟩ A) with ⊆¿-≥⊑≤ E | ≲-≥⊑≤ A
... | _ , ≤E , _ , ⊑F′ , ≤F | _ , ≤A , _ , ⊑B′ , ≤B = _ , ⟨ ≤E ⟩ ≤A , _ , ⟨ ⊑F′ ⟩ ⊑B′ , ⟨ ≤F ⟩ ≤B

≲-≥⊑≤+ : ∀ {n} → _≲+_ {n} ⟹ (flip _≤+_ ; (_⊑+_ ; _≤+_))
≲-≥⊑≤+ [] = _ , [] , _ , [] , []
≲-≥⊑≤+ (A ∷ As) with ≲-≥⊑≤ A | ≲-≥⊑≤+ As
... | _ , ≤A , _ , ⊑B′ , ≤B | _ , ≤As , _ , ⊑Bs′ , ≤Bs = _ , ≤A ∷ ≤As , _ , ⊑B′ ∷ ⊑Bs′ , ≤B ∷ ≤Bs

≲-≥⊑≤ ≲★ = _ , ≤-refl , _ , ⊑-refl , A≤★
≲-≥⊑≤ ★≲ = _ , A≤★ , _ , ⊑-refl , ≤-refl
≲-≥⊑≤ ($ ι) = _ , $ ι , _ , $ ι , $ ι
≲-≥⊑≤ (B≲A ⇒ P≲Q) with ≲-≥⊑≤ B≲A | ≲-≥⊑≤ᶜ P≲Q
... | _ , ≤B , _ , ⊑A′ , ≤A | _ , ≤P , _ , P′⊑ , ≤Q = _ , ≤A ⇒ ≤P , _ , ⊑A′ ⇒ P′⊑ , ≤B ⇒ ≤Q
≲-≥⊑≤ (A≲B₁ ′× A≲B₂) with ≲-≥⊑≤ A≲B₁ | ≲-≥⊑≤ A≲B₂
... | _ , ≤A₁ , _ , ⊑B₁′ , ≤B₁ | _ , ≤A₂ , _ , ⊑B₂′ , ≤B₂ = _ , ≤A₁ ′× ≤A₂ , _ , ⊑B₁′ ′× ⊑B₂′ , ≤B₁ ′× ≤B₂
≲-≥⊑≤ tycon = _ , tycon _ _ , _ , tycon _ _ , tycon _ _
≲-≥⊑≤ (sum ≲Bs) with ≲-≥⊑≤+ ≲Bs
... | _ , ≤As , _ , ⊑Bs′ , ≤Bs = _ , sum ≤As , _ , sum ⊑Bs′ , sum ≤Bs

≤-≲ : A ≤ B → A ≲ B
≥-≲ : B ≤ A → A ≲ B

≤-≲ᶜ : P ≤ᶜ Q → P ≲ᶜ Q
≤-≲ᶜ (⟨ E ⟩ A) = ⟨ ≤-⊆¿ E ⟩ ≤-≲ A

≥-≲ᶜ : Q ≤ᶜ P → P ≲ᶜ Q
≥-≲ᶜ (⟨ E ⟩ A) = ⟨ ≥-⊆¿ E ⟩ ≥-≲ A

≤-≲+ : ∀ {n} → _≤+_ {n = n} ⟹ _≲+_
≤-≲+ [] = []
≤-≲+ (A ∷ As) = ≤-≲ A ∷ ≤-≲+ As

≥-≲+ : ∀ {n} → flip (_≤+_ {n = n}) ⟹ _≲+_
≥-≲+ [] = []
≥-≲+ (A ∷ As) = ≥-≲ A ∷ ≥-≲+ As

≤-≲ ★ = ≲★
≤-≲ (_ ⇑ g) = ≲★
≤-≲ ($ _) = $ _
≤-≲ (A ⇒ P) = ≥-≲ A ⇒ ≤-≲ᶜ P
≤-≲ (A ′× B) = ≤-≲ A ′× ≤-≲ B
≤-≲ (tycon _ _) = tycon
≤-≲ (sum As) = sum (≤-≲+ As)

≥-≲ ★ = ≲★
≥-≲ (_ ⇑ g) = ★≲
≥-≲ ($ _) = $ _
≥-≲ (A ⇒ P) = ≤-≲ A ⇒ ≥-≲ᶜ P
≥-≲ (A ′× B) = ≥-≲ A ′× ≥-≲ B
≥-≲ (tycon _ _) = tycon
≥-≲ (sum As) = sum (≥-≲+ As)
