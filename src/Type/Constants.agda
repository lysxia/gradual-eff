module Type.Constants where

open import Type.Raw.RawConstants public
import Type.Raw

open import Data.Nat using (ℕ)
open import Data.List as List using (List)
open import Data.Fin.Base using (Fin)
open import Data.Vec.Base as Vec using (Vec)

infix 1 _𝔼-⇒_

record 𝔼-signature (raw : RawConstants) : Set where
  constructor _𝔼-⇒_
  open RawConstants raw public
  open Type.Raw raw
  field
    request : Typeᵛ ∅
    response : Typeᵛ ∅

record Constants : Set₁ where
  field
    raw : RawConstants
  open RawConstants raw public
  open Type.Raw raw
  field
    𝔼-sig : 𝔼 → 𝔼-signature raw
    tyDef : (T : 𝕋) → Typeᵛ (arity T)
