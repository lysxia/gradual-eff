module Type.DefaultConstants where

open import Utils using (DecEq)

open import Type.Constants
open import Type.Base

open import Data.Empty using (⊥)
open import Data.String using (String)
open import Data.List.Base using ([]; _∷_)
open import Data.Nat.Base using (ℕ)

module _ {𝕋} ⦃ DecEq-𝕋 : DecEq 𝕋 ⦄ {arity : 𝕋 → ℕ} where

  ℂ-𝔼-String : RawConstants
  ℂ-𝔼-String = record { 𝔼 = String ; 𝕋 = 𝕋 ; arity = arity }

  open import Type.Raw ℂ-𝔼-String

  𝔼-String-sig : String → 𝔼-signature ℂ-𝔼-String
  𝔼-String-sig = λ where
    "get"    → $𝕌 𝔼-⇒ $ℕ
    "set"    → $ℕ 𝔼-⇒ $𝕌
    "choose" → $𝕌 𝔼-⇒ $𝔹
    "fail"   → $𝕌 𝔼-⇒ ′∅
    "yield"  → $𝕌 𝔼-⇒ $𝕌
    "fork "  → ($𝕌 ⇒  ⟨ ¡ ("fork" ∷ "yield" ∷ "print" ∷ []) ⟩ $𝕌) 𝔼-⇒ $𝕌
    "print"  → $ℕ 𝔼-⇒ $𝕌
    _        → ★  𝔼-⇒ ★

basic-ℂ-raw : RawConstants
basic-ℂ-raw = record
  { 𝔼 = String
  ; 𝕋 = ⊥
  ; arity = λ() }

open import Type.Raw basic-ℂ-raw

basic-ℂ : Constants
basic-ℂ = record
  { raw = basic-ℂ-raw
  ; 𝔼-sig = 𝔼-String-sig
  ; tyDef = λ()
  }

open Constants basic-ℂ public
