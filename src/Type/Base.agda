module Type.Base where

open import Generics

open import Data.Bool.Base using () renaming (Bool to 𝔹)
open import Data.Nat.Base using (ℕ)
open import Data.Unit.Base using (⊤)
open import Data.Empty using (⊥)

-- Base types
data Base : Set where
  ′𝕌 : Base
  ′ℕ : Base
  ′𝔹 : Base

BaseD : HasDesc Base
BaseD = deriveDesc Base

rep : Base → Set
rep ′𝕌  =  ⊤
rep ′ℕ  =  ℕ
rep ′𝔹  =  𝔹

instance
  DecEq-Base : DecEq Base
  DecEq-Base = deriveDecEq BaseD
