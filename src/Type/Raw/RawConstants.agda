module Type.Raw.RawConstants where

open import Generics using (DecEq)

open import Data.Nat using (ℕ)
open import Data.Nat public
  using (suc)
  renaming (ℕ to TypeContext; zero to ∅)
open import Data.Fin public
  using (zero; suc)
  renaming (Fin to _⊢★)

record RawConstants : Set₁ where
  field
    𝔼 : Set
    𝕋 : Set
    ⦃ DecEq-𝔼 ⦄ : DecEq 𝔼
    ⦃ DecEq-𝕋 ⦄ : DecEq 𝕋

    arity : 𝕋 → ℕ
