open import Type.Constants

module Type.Context (ℂ : Constants) where

open import Type ℂ

open import Data.List.Base as List using (List; []; _∷_)

-- ** Contexts

infixl 6 _▷_

-- Contexts (lists of value types)
data Context : Set where
  ∅   : Context
  _▷_ : Context → Typeᵛ → Context

infix  4 _∋_
infix  9 S_

-- Variables (indices into contexts)
data _∋_ : Context → Typeᵛ → Set where

  Z : ∀ {Γ A}
      ----------
    → Γ ▷ A ∋ A

  S_ : ∀ {Γ A B}
    → Γ ∋ A
      ---------
    → Γ ▷ B ∋ A

-- * Operations

_▷++_ : Context → List Typeᵛ → Context
Γ ▷++ [] = Γ
Γ ▷++ (A ∷ As) = (Γ ▷ A) ▷++ As
