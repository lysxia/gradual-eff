open import Type.Constants

module Type.Precision (ℂ : Constants) where

open Constants ℂ

open import Type ℂ
open import Utils using (_≟_; _⊆_; ⊆-refl)

open import Data.Nat.Base using (zero; suc)
open import Data.List.Base using (List)
open import Data.Vec.Base as Vec using (Vec; []; _∷_)
open import Data.Vec.Relation.Unary.All as Vec using (All; []; _∷_)
open import Data.Vec.Relation.Binary.Pointwise.Inductive as Vec using (Pointwise; []; _∷_)
open import Data.Product using (_×_; _,_; proj₁; proj₂; Σ; ∃; Σ-syntax; ∃-syntax)
open import Data.Sum using (_⊎_; inj₁; inj₂)
open import Relation.Nullary using (¬_; Dec; yes; no)
open import Relation.Nullary.Product using (_×-dec_)
open import Relation.Binary using (Rel; Decidable; Transitive)
open import Relation.Binary.PropositionalEquality using (_≡_; _≢_; refl; sym; trans)

-- * Precision

-- Ground types
data Ground : Typeᵛ → Set where
  $_  :
       (ι : Base)
       ------------
     → Ground ($ ι)

  ★⇒¿★ : Ground (★ ⇒ ⟨ ¿ ⟩ ★)

  ★′×★ : Ground (★ ′× ★)

  sum★ : ∀ {Cs As} → All (★ ≡_) As → Ground (sum Cs As)

  tycon : ∀ (T : 𝕋) 𝓼 → Ground (tycon T 𝓼) -- TODO: cong through type arguments

ground : ∀ {G} → (g : Ground G) → Typeᵛ
ground {G = G} g  =  G

uniqueG : ∀ {G} → (g : Ground G) → (h : Ground G) → g ≡ h
uniqueG ($ ι) ($ .ι) = refl
uniqueG ★′×★  ★′×★   = refl
uniqueG ★⇒¿★  ★⇒¿★   = refl
uniqueG (sum★ gs) (sum★ hs) with unique★s gs hs
  where unique★s : ∀ {n} {Gs : Vec Typeᵛ n} → (gs hs : All (★ ≡_) Gs) → gs ≡ hs
        unique★s [] [] = refl
        unique★s (refl ∷ gs) (refl ∷ hs) with unique★s gs hs
        ... | refl = refl
... | refl = refl
uniqueG (tycon _ _) (tycon _ _) = refl

G≢★ : ∀ {G} → (g : Ground G) → G ≢ ★
G≢★ () refl

infix 4 _≤ᵉ_
infix 4 _≤ᶜ_
infix 4 _≤_
-- infix 4 _≤ᶜ?_
infixl 5 _⇑_

-- Precision of effect rows
data _≤ᵉ_ : Effects → Effects → Set where
  ¡¿ : ∀ {E} → ¡ E ≤ᵉ ¿
  ¿¿ : ¿ ≤ᵉ ¿
  ¡¡ : ∀ {E} → ¡ E ≤ᵉ ¡ E

≤¿ : {E : Effects} → E ≤ᵉ ¿
≤¿ {¿} = ¿¿
≤¿ {¡ E} = ¡¿

≤ᵉ-refl : ∀ {E} → E ≤ᵉ E
≤ᵉ-refl {¿} = ¿¿
≤ᵉ-refl {¡ E} = ¡¡

-- Precision
-- TODO: should it contain subtyping?
-- cf Jeremy, "subtyping&precision should commute"?
data _≤_ : Typeᵛ → Typeᵛ → Set
record _≤ᶜ_ (P : Typeᶜ) (Q : Typeᶜ) : Set

_≤+_ : ∀ {n} → Rel (Vec Typeᵛ n) _
_≤+_ = Pointwise _≤_

data _≤_ where

  ★ : ★ ≤ ★

  $_ : ∀ ι
      -----
    → $ ι ≤ $ ι

  _⇑_ : ∀ {A G}
    → A ≤ G
    → Ground G
      -----
    → A ≤ ★

  _′×_ : ∀ {A B A′ B′}
    → A ≤ A′
    → B ≤ B′
      ------
    → A ′× B ≤ A′ ′× B′

  _⇒_ : ∀ {A P A′ P′}
    → A ≤ A′
    → P ≤ᶜ P′
      ---------------
    → A ⇒ P ≤ A′ ⇒ P′

  sum : ∀ {Cs : List _} {As Bs}
    → As ≤+ Bs
      --------
    → sum Cs As ≤ sum Cs Bs

  tycon : ∀ (T : 𝕋) 𝓼
      ---------------------
    → tycon T 𝓼 ≤ tycon T 𝓼

record _≤ᶜ_ P Q where
  inductive
  constructor ⟨_⟩_
  field
    effects : P .effects ≤ᵉ Q .effects
    returns : P .returns ≤  Q .returns

≤-refl : ∀ {A} → A ≤ A
≤ᶜ-refl : ∀ {P} → P ≤ᶜ P

≤-refl {★} = ★
≤-refl {$ ι} = $ ι
≤-refl {A ′× B} = ≤-refl ′× ≤-refl
≤-refl {A ⇒ P} = ≤-refl ⇒ ≤ᶜ-refl
≤-refl {sum Cs As} = sum Pointwise-≤-refl
  where
    Pointwise-≤-refl : ∀ {n} {As : Vec Typeᵛ n} → Pointwise _≤_ As As
    Pointwise-≤-refl {As = []} = []
    Pointwise-≤-refl {As = _ ∷ _} = ≤-refl ∷ Pointwise-≤-refl
≤-refl {tycon _ _} = tycon _ _

≤ᶜ-refl {⟨ E ⟩ F} = ⟨ ≤ᵉ-refl ⟩ ≤-refl

G≤★ : ∀ {G} → Ground G → G ≤ ★
G≤★ g = ≤-refl ⇑ g

¬★≤G : ∀ {G} → Ground G → ¬ (★ ≤ G)
¬★≤G ($ _) ()

★≤ : ∀ {A} → ★ ≤ A → A ≡ ★
★≤ {★} p  =  refl
★≤ {$ ι} ()
★≤ {A ⇒ ⟨ E ⟩ B} ()

All-★ : ∀ {n} → All {A = Typeᵛ} (★ ≡_) (Vec.replicate {n = n} ★)
All-★ {n = zero} = []
All-★ {n = suc n} = refl ∷ All-★

★⊎G : ∀ A → (A ≡ ★) ⊎ ∃[ G ](Ground G × A ≤ G)

A≤★ : ∀ {A} → A ≤ ★
A≤★ {A} with ★⊎G A
... | inj₁ refl = ★
... | inj₂ (_ , g , A≤) = A≤ ⇑ g

★⊎G ★        =  inj₁ refl
★⊎G ($ ι)    =  inj₂ ($ ι , $ ι , $ ι)
★⊎G (A ′× B) =  inj₂ (_ , ★′×★ , A≤★ ′× A≤★)
★⊎G (A ⇒ ⟨ E ⟩ B)  =  inj₂ (★ ⇒ ⟨ ¿ ⟩  ★ , ★⇒¿★ , A≤★ ⇒ ⟨ ≤¿ ⟩ A≤★)
★⊎G (sum Cs As) = inj₂ (sum Cs (Vec.replicate ★) , sum★ All-★ , sum Pointwise-≤★)
  where Pointwise-≤★ : ∀ {n} {As : Vec Typeᵛ n} → Pointwise _≤_ As (Vec.replicate {n = n} ★)
        Pointwise-≤★ {As = []} = []
        Pointwise-≤★ {As = _ ∷ _} = A≤★ ∷ Pointwise-≤★
★⊎G (tycon T 𝓼) = inj₂ (tycon T 𝓼 , tycon T 𝓼 , tycon T 𝓼)

≢★ : ∀ {A G} → Ground G → A ≤ G → A ≢ ★
≢★ g A≤G A≡★ rewrite A≡★ = ¬★≤G g A≤G

Ground? : ∀(A : Typeᵛ) → Dec (Ground A)
Ground? ★     =  no λ()
Ground? ($ ι) =  yes ($ ι)
Ground? (A ′× B) with A ≡? ★ ×-dec B ≡? ★
... | yes (refl , refl) = yes ★′×★
... | no ¬≡ = no λ{★′×★ → ¬≡ (refl , refl)}
Ground? (A ⇒ ⟨ E ⟩ B) with A ≡? ★ ×-dec E ≡ᵉ? ¿ ×-dec B ≡? ★
... | yes (refl , refl , refl) = yes ★⇒¿★
... | no ¬≡ = no λ{★⇒¿★ → ¬≡ (refl , refl , refl)}
Ground? (sum Cs As) with All-★? As
  where All-★? : ∀ {n} (As : Vec Typeᵛ n) → Dec (All (★ ≡_) As)
        All-★? [] = yes []
        All-★? (A ∷ As) with ★ ≡? A ×-dec All-★? As
        ... | yes (★≡A , ★≡As) = yes (★≡A ∷ ★≡As)
        ... | no ¬All-★ = no λ{ (★≡A ∷ ★≡As) → ¬All-★ (★≡A , ★≡As) }
... | yes gs = yes (sum★ gs)
... | no ¬All-★ = no λ{ (sum★ All-★) → ¬All-★ All-★ }
Ground? (tycon T 𝓼) = yes (tycon T 𝓼)

infix 4 _≤ᵉ?_
infix 4 _≤?_

_≤ᵉ?_ : Decidable _≤ᵉ_
¿ ≤ᵉ? ¿ = yes ¿¿
¡ E ≤ᵉ? ¿ = yes ¡¿
¿ ≤ᵉ? ¡ _ = no λ ()
¡ E ≤ᵉ? ¡ F with E ≟ F
... | yes refl = yes ¡¡
... | no ¬E≡F  = no λ { ¡¡ → ¬E≡F refl }

_≤?_ : Decidable _≤_
_ ≤? ★ = yes A≤★
($ ι) ≤? ($ ι′) with ι ≟ ι′
... | yes refl =  yes ($ _)
... | no  ι≢ι′ =  no  λ{($ _) → ι≢ι′ refl}
(A ′× B) ≤? (A′ ′× B′) with A ≤? A′ ×-dec B ≤? B′
... | yes (A≤ , B≤) = yes (A≤ ′× B≤)
... | no ¬≤ = no λ{(A≤ ′× B≤) → ¬≤ (A≤ , B≤)}
(A ⇒ ⟨ E ⟩ B) ≤? (A′ ⇒ ⟨ E′ ⟩ B′) with A ≤? A′ ×-dec E ≤ᵉ? E′ ×-dec B ≤? B′
... | yes (A≤ , E≤ , B≤) = yes (A≤ ⇒ ⟨ E≤ ⟩ B≤)
... | no ¬≤ = no λ{(A≤ ⇒ ⟨ E≤ ⟩ B≤) → ¬≤ (A≤ , E≤ , B≤)}
(sum Cs As) ≤? (sum Ds Bs) with Cs ≟ Ds
... | yes refl with Pointwise-≤? As Bs
  where Pointwise-≤? : ∀ {n} (As Bs : Vec Typeᵛ n) → Dec (Pointwise _≤_ As Bs)
        Pointwise-≤? [] [] = yes []
        Pointwise-≤? (A ∷ As) (B ∷ Bs) with A ≤? B ×-dec Pointwise-≤? As Bs
        ... | yes (A≤ , As≤) = yes (A≤ ∷ As≤)
        ... | no ¬≤ = no λ{ (A≤ ∷ As≤) → ¬≤ (A≤ , As≤) }
...            | yes As≤ = yes (sum As≤)
...            | no ¬As≤ = no λ{ (sum As≤) → ¬As≤ As≤ }
(sum Cs As) ≤? (sum Ds Bs) | no ¬≡ = no λ{ (sum _) → ¬≡ refl }
A@(tycon T 𝓼) ≤? B@(tycon U 𝓼′) with A ≡? B
... | yes refl = yes (tycon T 𝓼)
... | no ¬≡ = no λ{ (tycon _ _) → ¬≡ refl }
★ ≤? ($ ι)     =  no λ()
★ ≤? (_ ′× _)  =  no λ()
★ ≤? (A ⇒ P)   =  no λ()
★ ≤? tycon _ _ =  no λ()
($ ι) ≤? (_ ′× _)   = no λ()
($ ι) ≤? (A ⇒ P)    = no λ()
(_ ′× _) ≤? ($ _)   = no λ()
(_ ′× _) ≤? (_ ⇒ _) = no λ()
(A ⇒ P) ≤? ($ ι)    = no λ()
(_ ⇒ _) ≤? (_ ′× _) = no λ()
tycon _ _ ≤? ($ ι)    = no λ()
tycon _ _ ≤? (_ ′× _) = no λ()
tycon _ _ ≤? (A ⇒ P)  = no λ()
($ ι)    ≤? tycon _ _ = no λ()
(_ ′× _) ≤? tycon _ _ = no λ()
(A ⇒ P)  ≤? tycon _ _ = no λ()
sum _ _ ≤? ($ ι)     = no λ()
sum _ _ ≤? (_ ′× _)  = no λ()
sum _ _ ≤? (A ⇒ P)   = no λ()
sum _ _ ≤? tycon _ _ = no λ()
★         ≤? sum _ _ = no λ()
($ ι)     ≤? sum _ _ = no λ()
(_ ′× _)  ≤? sum _ _ = no λ()
(A ⇒ P)   ≤? sum _ _ = no λ()
tycon _ _ ≤? sum _ _ = no λ()

-- * Properties of _≤_

≤ᵉ-trans : ∀ {A B C} → A ≤ᵉ B → B ≤ᵉ C → A ≤ᵉ C
≤ᵉ-trans ¡¡ ¡¡ = ¡¡
≤ᵉ-trans ¡¡ ¡¿ = ¡¿
≤ᵉ-trans ¡¿ ¿¿ = ¡¿
≤ᵉ-trans ¿¿ ¿¿ = ¿¿

≤-trans : ∀ {A B C} → A ≤ B → B ≤ C → A ≤ C
≤ᶜ-trans : ∀ {A B C} → A ≤ᶜ B → B ≤ᶜ C → A ≤ᶜ C
≤-trans A≤B ★ = A≤B
≤-trans ($ _) ($ _) = $ _
≤-trans (A ′× B) (A′ ′× B′) = ≤-trans A A′ ′× ≤-trans B B′
≤-trans (A ⇒ P) (A′ ⇒ P′) = ≤-trans A A′ ⇒ ≤ᶜ-trans P P′
≤-trans A (B ⇑ g) = ≤-trans A B ⇑ g
≤-trans (tycon _ _) (tycon _ _) = tycon _ _
≤-trans (sum As) (sum Bs) = sum (Pointwise-≤-trans As Bs)
  where Pointwise-≤-trans : ∀ {n} → Transitive (Pointwise _≤_ {n = n})
        Pointwise-≤-trans [] [] = []
        Pointwise-≤-trans (A ∷ As) (B ∷ Bs) = ≤-trans A B ∷ Pointwise-≤-trans As Bs

≤ᶜ-trans (⟨ E ⟩ A) (⟨ F ⟩ B) = ⟨ ≤ᵉ-trans E F ⟩ (≤-trans A B)

≤-⊆¿ : ∀ {E F} → E ≤ᵉ F → E ⊆¿ F
≤-⊆¿ ¡¡ = ¡¡ ⊆-refl
≤-⊆¿ ¡¿ = ¡¿
≤-⊆¿ ¿¿ = ¿¿

≥-⊆¿ : ∀ {E F} → F ≤ᵉ E → E ⊆¿ F
≥-⊆¿ ¡¡ = ¡¡ ⊆-refl
≥-⊆¿ ¡¿ = ¿¡
≥-⊆¿ ¿¿ = ¿¿

---

All-★-inv : ∀ {n} {As : Vec Typeᵛ n} → All (★ ≡_) As → Vec.replicate ★ ≡ As
All-★-inv [] = refl
All-★-inv (refl ∷ As) with All-★-inv As
... | refl = refl

unique-All-★ : ∀ {n} {As Bs : Vec Typeᵛ n} → All (★ ≡_) As → All (★ ≡_) Bs → As ≡ Bs
unique-All-★ As≡★ Bs≡★ = trans (sym (All-★-inv As≡★)) (All-★-inv Bs≡★)

unique-grounding : ∀ {A G₁ G₂} → Ground G₁ → A ≤ G₁ → Ground G₂ → A ≤ G₂ → G₁ ≡ G₂
unique-grounding ($ _) ($ _) ($ _) ($ _) = refl
unique-grounding ★⇒¿★ (_ ⇒ _) ★⇒¿★ (_ ⇒ _) = refl
unique-grounding ★′×★ (_ ′× _) ★′×★ (_ ′× _) = refl
unique-grounding (sum★ Gs₁) (sum A≤₁) (sum★ Gs₂) (sum A≤₂) with unique-All-★ Gs₁ Gs₂
... | refl = refl
unique-grounding (tycon _ _) (tycon _ _) (tycon _ _) (tycon _ _) = refl
