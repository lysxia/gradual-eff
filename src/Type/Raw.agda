open import Utils using (DecEq; _≟_)
open import Type.Raw.RawConstants

module Type.Raw (ℂ : RawConstants) (open RawConstants ℂ) where

open import Type.Base
open import Utils using (_∈_; _∈?_; _⊆_; _⊆?_; ⊆-refl)

open import Data.String.Base as String using (String)
import Data.String.Properties as String
open import Data.List.Base as List using (List; []; _∷_; _++_)
open import Data.List.Relation.Unary.Any using (here; there)
open import Data.Vec.Base as Vec using (Vec; []; _∷_)
open import Data.Product using (_×_; _,_)
open import Relation.Binary using (Decidable)
open import Relation.Binary.PropositionalEquality using (_≡_; refl)
open import Relation.Nullary using (Dec; yes; no)
open import Relation.Nullary.Product using (_×-dec_)

private
  variable
    Γ Δ : TypeContext
    e : 𝔼

-- * Effects and Types

-- ** Effects

infix 8 ¡_

data Effects : Set where
  ¿ : Effects
  ¡_ : List 𝔼 -> Effects

pattern ε = ¡ []

_++¿_ : List 𝔼 → Effects → Effects
_ ++¿ ¿ = ¿
E ++¿ (¡ F) = ¡ (E ++ F)

-- ** Types

module _ (Γ : TypeContext) where

  data Typeᵛ : Set
  record Typeᶜ : Set

  infixr 7 _⇒_
  infixr 6 _′×_
  infix  8 $_

  -- Value types
  data Typeᵛ where
    ★ : Typeᵛ
    $_ : (ι : Base) → Typeᵛ
    _⇒_ : (A : Typeᵛ) → (P : Typeᶜ) → Typeᵛ
    _′×_ : Typeᵛ → Typeᵛ → Typeᵛ
    sum : (Cs : List String) → Vec Typeᵛ (List.length Cs) → Typeᵛ
    tycon : (T : 𝕋) → Vec Typeᵛ (arity T) → Typeᵛ
    `_ : Γ ⊢★ → Typeᵛ

  infix 7 ⟨_⟩_

  -- Computation types
  --
  -- Inductive records have definitional eta equality,
  -- so Agda terms that pattern-match on records don't get stuck,
  -- which means that we get to pattern-match less.
  record Typeᶜ where
    inductive
    constructor ⟨_⟩_
    field
      effects : Effects
      returns : Typeᵛ

  open Typeᶜ public

--

pattern $𝕌 = $ ′𝕌
pattern $ℕ = $ ′ℕ
pattern $𝔹 = $ ′𝔹

′∅ : ∀ {Γ} → Typeᵛ Γ
′∅ = sum [] []

TySub : (_ _ : TypeContext) → Set
TySub Γ Δ = Vec (Typeᵛ Δ) Γ

subTy : TySub Γ Δ → Typeᵛ Γ → Typeᵛ Δ
subTyᶜ : TySub Γ Δ → Typeᶜ Γ → Typeᶜ Δ
subTys : ∀ {n} → TySub Γ Δ → Vec (Typeᵛ Γ) n → Vec (Typeᵛ Δ) n

subTy σ ★ = ★
subTy σ ($ ι) = $ ι
subTy σ (A ⇒ P) = subTy σ A ⇒ subTyᶜ σ P
subTy σ (A ′× B) = subTy σ A ′× subTy σ B
subTy σ (sum Cs As) = sum Cs (subTys σ As)
subTy σ (tycon T As) = tycon T (subTys σ As)
subTy σ (` a) = Vec.lookup σ a

subTys σ [] = []
subTys σ (A ∷ As) = subTy σ A ∷ subTys σ As

subTyᶜ σ (⟨ E ⟩ A) = ⟨ E ⟩ subTy σ A

--

infix 5 _∈¿_

data _∈¿_ (e : 𝔼) : Effects → Set where
  ∈¿¿ : e ∈¿ ¿
  ∈¿¡ : {E : List 𝔼} → e ∈ E → e ∈¿ ¡ E

infix 5 _⊆¿_

data _⊆¿_ : (E F : Effects) → Set where
  ¿¿ : ¿ ⊆¿ ¿
  ¿¡ : ∀ {E} → ¿ ⊆¿ ¡ E
  ¡¿ : ∀ {E} → ¡ E ⊆¿ ¿
  ¡¡ : ∀ {E F} → E ⊆ F → ¡ E ⊆¿ ¡ F

ε⊆¿ : ∀ {E} → ε ⊆¿ E
ε⊆¿ {¡ _} = ¡¡ λ()
ε⊆¿ {¿} = ¡¿

⊆¿-refl : ∀ {E} → E ⊆¿ E
⊆¿-refl {¿} = ¿¿
⊆¿-refl {¡ _} = ¡¡ ⊆-refl

∈¿-⊆¿ : ∀ {e} {E} → e ∈¿ E → ¡ (e ∷ []) ⊆¿ E
∈¿-⊆¿ ∈¿¿ = ¡¿
∈¿-⊆¿ (∈¿¡ e∈E) = ¡¡ λ{ (here refl) → e∈E }

-- ** Decidable equality

infix 4 _≡ᶜ?_
infix 4 _≡?_

_≡ᵉ?_ : Decidable {A = Effects} _≡_
¿ ≡ᵉ? ¿ = yes refl
(¡ E) ≡ᵉ? (¡ F) with E ≟ F
... | yes refl = yes refl
... | no E≢F = no λ { refl → E≢F refl }
(¡ _) ≡ᵉ? ¿ = no λ()
¿ ≡ᵉ? (¡ _) = no λ()

_≡?_  : Decidable {A = Typeᵛ Γ} _≡_
_≡ᶜ?_ : Decidable {A = Typeᶜ Γ} _≡_
_≡*?_ : ∀ {n} → Decidable {A = Vec (Typeᵛ Γ) n} _≡_

★       ≡? ★            = yes refl
★       ≡? ($ _)        = no λ()
★       ≡? (_ ′× _)     = no λ()
★       ≡? (_ ⇒ _)      = no λ()
($ _)   ≡? ★            = no λ()
($ _)   ≡? (_ ′× _)     = no λ()
($ ι)   ≡? ($ κ) with ι ≟ κ
... | yes refl =  yes refl
... | no  ι≢κ  =  no  (λ{refl → ι≢κ refl})
($ _)   ≡? (_ ⇒ _) =  no λ()
(_ ′× _) ≡? ★      =  no λ()
(_ ′× _) ≡? ($ _)  =  no λ()
(A ′× B) ≡? (A′ ′× B′) with A ≡? A′ ×-dec B ≡? B′
... | yes (refl , refl) = yes refl
... | no ¬≡ = no λ{ refl → ¬≡ (refl , refl) }
(_ ′× _) ≡? (_ ⇒ _) = no λ()
(_ ⇒ _) ≡? ★        = no λ()
(_ ⇒ _) ≡? ($ _)    = no λ()
(_ ⇒ _) ≡? (_ ′× _) = no λ()
(A ⇒ P) ≡? (A′ ⇒ P′) with A ≡? A′ ×-dec P ≡ᶜ? P′
... | yes (refl , refl) = yes refl
... | no  ¬≡ = no λ{ refl → ¬≡ (refl , refl) }
` a ≡? ` b with a ≟ b
... | yes refl = yes refl
... | no ¬≡ = no λ{ refl → ¬≡ refl }
tycon T As ≡? tycon U Bs with T ≟ U
... | yes refl with As ≡*? Bs
...            | yes refl = yes refl
...            | no ¬≡ = no λ{ refl → ¬≡ refl }
tycon T As ≡? tycon U Bs | no ¬≡ = no λ{ refl → ¬≡ refl }
sum Cs As ≡? sum Ds Bs with Cs ≟ Ds
... | yes refl with As ≡*? Bs
...            | yes refl = yes refl
...            | no ¬≡ = no λ{ refl → ¬≡ refl }
sum Cs As ≡? sum Ds Bs | no ¬≡ = no λ{ refl → ¬≡ refl }
★ ≡? ` _ = no λ()
$ _ ≡? ` _ = no λ()
(_ ⇒ _) ≡? ` _ = no λ()
(_ ′× _) ≡? ` _ = no λ()
` _ ≡? ★ = no λ()
` _ ≡? $ _ = no λ()
` _ ≡? (_ ⇒ _) = no λ()
` _ ≡? (_ ′× _) = no λ()
★ ≡? tycon _ _ = no λ()
$ _ ≡? tycon _ _ = no λ()
(_ ⇒ _) ≡? tycon _ _ = no λ()
(_ ′× _) ≡? tycon _ _ = no λ()
` _ ≡? tycon _ _ = no λ()
tycon _ _ ≡? ★ = no λ()
tycon _ _ ≡? $ _ = no λ()
tycon _ _ ≡? (_ ⇒ _) = no λ()
tycon _ _ ≡? (_ ′× _) = no λ()
tycon _ _ ≡? ` _ = no λ()
sum _ _ ≡? ★ = no λ()
sum _ _ ≡? $ _ = no λ()
sum _ _ ≡? (_ ⇒ _) = no λ()
sum _ _ ≡? (_ ′× _) = no λ()
sum _ _ ≡? ` _ = no λ()
sum _ _ ≡? tycon _ _ = no λ()
★         ≡? sum _ _ = no λ()
$ _       ≡? sum _ _ = no λ()
(_ ⇒ _)   ≡? sum _ _ = no λ()
(_ ′× _)  ≡? sum _ _ = no λ()
` _       ≡? sum _ _ = no λ()
tycon _ _ ≡? sum _ _ = no λ()

[] ≡*? [] = yes refl
(A ∷ As) ≡*? (B ∷ Bs) with A ≡? B ×-dec As ≡*? Bs
... | yes (refl , refl) = yes refl
... | no ¬≡ = no λ{ refl → ¬≡ (refl , refl) }

⟨ E ⟩ A ≡ᶜ? ⟨ E′ ⟩ A′ with E ≡ᵉ? E′ ×-dec A ≡? A′
... | yes (refl , refl) = yes refl
... | no ¬≡ = no λ { refl → ¬≡ (refl , refl) }

infix 4 _⊆¿?_

_⊆¿?_ : Decidable _⊆¿_
¿ ⊆¿? ¿ = yes ¿¿
¿ ⊆¿? ¡ _ = yes ¿¡
¡ _ ⊆¿? ¿ = yes ¡¿
(¡ E) ⊆¿? (¡ F) with E ⊆? F
... | yes E⊆F = yes (¡¡ E⊆F)
... | no ¬E⊆F = no λ{ (¡¡ E⊆F) → ¬E⊆F E⊆F }

_∈¿?_ : ∀ e E → Dec (e ∈¿ E)
e ∈¿? ¿ = yes ∈¿¿
e ∈¿? (¡ E) with e ∈? E
... | yes e∈E = yes (∈¿¡ e∈E)
... | no ¬e∈E = no λ{ (∈¿¡ e∈E) → ¬e∈E e∈E }
