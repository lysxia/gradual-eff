open import Type.Constants

module Type.Consistency (ℂ : Constants) where

open Constants ℂ

open import Type ℂ
open import Type.Precision ℂ
open import Utils using (_≟_)

open import Data.Vec.Base as Vec using (Vec; []; _∷_)
open import Data.Vec.Relation.Unary.All as Vec using (All; []; _∷_)
open import Data.Vec.Relation.Binary.Pointwise.Inductive as Vec using (Pointwise; []; _∷_)
open import Data.Product using (_,_)
open import Function.Base using (flip)
open import Relation.Binary.Construct.Composition using (_;_)
open import Relation.Nullary using (Dec; yes; no)
open import Relation.Nullary.Product using (_×-dec_)
open import Relation.Binary using (Rel; Reflexive; Symmetric; Decidable)
  renaming (_⇒_ to _⟹_)
open import Relation.Binary.PropositionalEquality using (_≡_; refl)

infix 4 _≈ᵉ_

infix 4 _≈_
infix 4 _≈ᶜ_

-- Consistency relation
_≈_ : Rel Typeᵛ _
_≈_ = flip _≤_ ; _≤_

_≈ᶜ_ : Rel Typeᶜ _
_≈ᶜ_ = flip _≤ᶜ_ ; _≤ᶜ_

_≈ᵉ_ : Rel Effects _
_≈ᵉ_ = flip _≤ᵉ_ ; _≤ᵉ_

★≈ : ∀ {A} → ★ ≈ A
★≈ = _ , A≤★ , ≤-refl

≈★ : ∀ {A} → A ≈ ★
≈★ = _ , ≤-refl , A≤★

_≈-⇒_ : ∀ {A A′ P P′} → A ≈ A′ → P ≈ᶜ P′ → (A ⇒ P) ≈ (A′ ⇒ P′)
(_ , ≤A , ≤A′) ≈-⇒ (_ , ≤P , ≤P′) = _ , ≤A ⇒ ≤P , ≤A′ ⇒ ≤P′

_≈-×_ : ∀ {A A′ B B′} → A ≈ A′ → B ≈ B′ → (A ′× B) ≈ (A′ ′× B′)
(_ , ≤A , ≤A′) ≈-× (_ , ≤B , ≤B′) = _ , ≤A ′× ≤B , ≤A′ ′× ≤B′

≈-$_ : ∀ ι → $ ι ≈ $ ι
≈-$ ι = $ ι , $ ι , $ ι

≈-⟨_⟩_ : ∀ {E E′ A A′} → E ≈ᵉ E′ → A ≈ A′ → ⟨ E ⟩ A ≈ᶜ ⟨ E′ ⟩ A′
≈-⟨ (_ , ≤E , ≤E′) ⟩ (_ , ≤A , ≤A′) = _ , ⟨ ≤E ⟩ ≤A , ⟨ ≤E′ ⟩ ≤A′

¿≈ : ∀ {E} → ¿ ≈ᵉ E
¿≈ = _ , ≤¿ , ≤ᵉ-refl

≈¿ : ∀ {E} → E ≈ᵉ ¿
≈¿ = _ , ≤ᵉ-refl , ≤¿

≈-refl : Reflexive _≈_
≈-refl = _ , ≤-refl , ≤-refl

≈ᶜ-refl : Reflexive _≈ᶜ_
≈ᶜ-refl = _ , ≤ᶜ-refl , ≤ᶜ-refl

≈ᵉ-refl : Reflexive _≈ᵉ_
≈ᵉ-refl = _ , ≤ᵉ-refl , ≤ᵉ-refl

≈-sym : ∀ {ℓ} {A : Set} {_≤_ : Rel A ℓ} → Symmetric (flip _≤_ ; _≤_)
≈-sym (_ , ≤A , ≤B) = (_ , ≤B , ≤A)

-- * Decidable relations

infix 4 _≈?_
infix 4 _≈ᶜ?_

_≈ᵉ?_ : Decidable _≈ᵉ_
¿ ≈ᵉ? _ = yes ¿≈
_ ≈ᵉ? ¿ = yes ≈¿
(¡ E) ≈ᵉ? (¡ F) with E ≟ F
... | yes refl = yes ≈ᵉ-refl
... | no ¬E≡F  = no λ{ (_ , ¡¡ , ¡¡) → ¬E≡F refl }

_≈+_ : ∀ {n} → Rel (Vec Typeᵛ n) _
_≈+_ {n} = flip (Pointwise {A = Typeᵛ} _≤_ {m = n} {n = n}) ; Pointwise {A = Typeᵛ} _≤_ {n = n}

_≈?_ : Decidable _≈_
_≈ᶜ?_ : Decidable _≈ᶜ_

Pointwise-≈? : ∀ {n} → Decidable (_≈+_ {n = n})
Pointwise-≈? [] [] = yes ([] , [] , [])
Pointwise-≈? (A ∷ As) (B ∷ Bs) with A ≈? B ×-dec Pointwise-≈? As Bs
... | yes ((_ , ≤A , ≤B) , (_ , ≤As , ≤Bs)) = yes (_ , ≤A ∷ ≤As , ≤B ∷ ≤Bs)
... | no ¬≈ = no λ{ (_ , ≤A ∷ ≤As , ≤B ∷ ≤Bs) → ¬≈ ((_ , ≤A , ≤B) , (_ , ≤As , ≤Bs)) }

★ ≈? _ = yes ★≈
_ ≈? ★ = yes ≈★
($ ι) ≈? ($ κ) with ι ≟ κ
... | yes refl = yes (≈-$ ι)
... | no ¬ι≡κ = no λ { (_ , $ _ , $ _) → ¬ι≡κ refl }
(A ⇒ P) ≈? (A′ ⇒ P′) with A ≈? A′ ×-dec P ≈ᶜ? P′
... | yes (A≈A′ , P≈P′) = yes (A≈A′ ≈-⇒ P≈P′)
... | no ¬≈ = no λ { (_ , ≤A ⇒ ≤P , ≤A′ ⇒ ≤P′) → ¬≈ ((_ , ≤A , ≤A′) , (_ , ≤P , ≤P′)) }
(A ′× B) ≈? (A′ ′× B′) with A ≈? A′ ×-dec B ≈? B′
... | yes (A≈A′ , B≈B′) = yes (A≈A′ ≈-× B≈B′)
... | no ¬≈ = no λ { (_ , ≤A ′× ≤B , ≤A′ ′× ≤B′) → ¬≈ ((_ , ≤A , ≤A′) , (_ , ≤B , ≤B′)) }
A@(tycon _ _) ≈? B@(tycon _ _) with A ≡? B
... | yes refl = yes (_ , tycon _ _ , tycon _ _)
... | no ¬≡ = no λ{ (_ , tycon _ _ , tycon _ _) → ¬≡ refl }
sum Cs As ≈? sum Ds Bs with Cs ≟ Ds
... | no ¬≡ = no λ{ (_ , sum _ , sum _) → ¬≡ refl }
... | yes refl with Pointwise-≈? As Bs
...            | yes (_ , ≤As , ≤Bs) = yes (_ , sum ≤As , sum ≤Bs)
...            | no ¬≈ = no λ{ (_ , sum ≤As , sum ≤Bs) → ¬≈ (_ , ≤As , ≤Bs) }
($ _) ≈? (_ ⇒ _) = no λ{(_ , $ _ , ())}
($ _) ≈? (_ ′× _) = no λ{(_ , $ _ , ())}
(_ ⇒ _) ≈? ($ _) = no λ{(_ , _ ⇒ _ , ())}
(_ ⇒ _) ≈? (_ ′× _) = no λ{(_ , _ ⇒ _ , ())}
(_ ′× _) ≈? ($ _) = no λ{(_ , _ ′× _ , ())}
(_ ′× _) ≈? (_ ⇒ _) = no λ{(_ , _ ′× _ , ())}
tycon _ _ ≈? ($ _)    = no λ{(_ , tycon _ _ , ())}
tycon _ _ ≈? (_ ⇒ _)  = no λ{(_ , tycon _ _ , ())}
tycon _ _ ≈? (_ ′× _) = no λ{(_ , tycon _ _ , ())}
($ _)    ≈? tycon _ _ = no λ{(_ , $ _ , ())}
(_ ⇒ _)  ≈? tycon _ _ = no λ{(_ , _ ⇒ _ , ())}
(_ ′× _) ≈? tycon _ _ = no λ{(_ , _ ′× _ , ())}
sum _ _ ≈? ($ _)     = no λ{(_ , sum _ , ())}
sum _ _ ≈? (_ ⇒ _)   = no λ{(_ , sum _ , ())}
sum _ _ ≈? (_ ′× _)  = no λ{(_ , sum _ , ())}
sum _ _ ≈? tycon _ _ = no λ{(_ , sum _ , ())}
($ _)     ≈? sum _ _ = no λ{(_ , $ _ , ())}
(_ ⇒ _)   ≈? sum _ _ = no λ{(_ , _ ⇒ _ , ())}
(_ ′× _)  ≈? sum _ _ = no λ{(_ , _ ′× _ , ())}
tycon _ _ ≈? sum _ _ = no λ{(_ , tycon _ _ , ())}

⟨ E ⟩ A ≈ᶜ? ⟨ F ⟩ B with E ≈ᵉ? F ×-dec A ≈? B
... | yes (E≈F , A≈B) = yes (≈-⟨ E≈F ⟩ A≈B)
... | no ¬≈ = no λ { (_ , ⟨ ≤E ⟩ ≤A , ⟨ ≤F ⟩ ≤B) → ¬≈ ((_ , ≤E , ≤F) , (_ , ≤A , ≤B)) }

≤-≈ : _≤_ ⟹ _≈_
≤-≈ ≤A = _ , ≤-refl , ≤A

≤-≈ᶜ : _≤ᶜ_ ⟹ _≈ᶜ_
≤-≈ᶜ ≤A = _ , ≤ᶜ-refl , ≤A

≤-≈ᵉ : _≤ᵉ_ ⟹ _≈ᵉ_
≤-≈ᵉ ≤A = _ , ≤ᵉ-refl , ≤A

≥-≈ : flip _≤_ ⟹ _≈_
≥-≈ ≤A = _ , ≤A , ≤-refl

≥-≈ᶜ : flip _≤ᶜ_ ⟹ _≈ᶜ_
≥-≈ᶜ ≤A = _ , ≤A , ≤ᶜ-refl

≥-≈ᵉ : flip _≤ᵉ_ ⟹ _≈ᵉ_
≥-≈ᵉ ≤A = _ , ≤A , ≤ᵉ-refl
