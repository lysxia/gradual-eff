open import Type.Constants

module Type.Subtyping (ℂ : Constants) where

open Constants ℂ

open import Type ℂ
open import Utils using (_∈_; _⊆_; ⊆-refl)

open import Function.Base using (_∘_)
open import Data.List.Base as List using (List; []; _∷_; length)
open import Data.List.Relation.Unary.Any as Any using (here; there)
open import Data.Vec.Base as Vec using (Vec; []; _∷_)
open import Data.Vec.Relation.Binary.Pointwise.Inductive using (Pointwise; []; _∷_)
open import Relation.Binary using (REL; Rel; Reflexive; Symmetric; Decidable)
open import Relation.Binary.PropositionalEquality.Core using (_≡_; refl; sym; cong₂; subst)

-- * Subtyping

infix 3 _⊑ᵉ_
infix 3 _⊑_
infix 3 _⊑ᶜ_

data _⊑ᵉ_ : Effects → Effects → Set where
  ¿ : ¿ ⊑ᵉ ¿
  ¡_ : ∀ {E F} → E ⊆ F → ¡ E ⊑ᵉ ¡ F

data _⊑_ : (A B : Typeᵛ) → Set
record _⊑ᶜ_ (P Q : Typeᶜ) : Set

_⊑+_ : ∀ {n} → Rel (Vec Typeᵛ n) _
_⊑+_ = Pointwise _⊑_

data _⊑_ where
  ★ : ★ ⊑ ★
  $_ : ∀ ι → $ ι ⊑ $ ι
  _⇒_ : ∀ {A B P Q} → B ⊑ A → P ⊑ᶜ Q → A ⇒ P ⊑ B ⇒ Q
  _′×_ : ∀ {A₁ B₁ A₂ B₂} → A₁ ⊑ B₁ → A₂ ⊑ B₂ → A₁ ′× A₂ ⊑ B₁ ′× B₂
  sum : ∀ {Cs : List _} {As Bs} → As ⊑+ Bs → sum Cs As ⊑ sum Cs Bs
  tycon : (T : 𝕋) (𝓼 : _) → tycon T 𝓼 ⊑ tycon T 𝓼

record _⊑ᶜ_ P Q where
  inductive
  constructor ⟨_⟩_
  field
    effects : P .effects ⊑ᵉ Q .effects
    returns : P .returns ⊑ Q .returns

⊑ᵉ-refl : Reflexive _⊑ᵉ_
⊑ᵉ-refl {¿} = ¿
⊑ᵉ-refl {¡ _} = ¡ ⊆-refl

⊑-refl : Reflexive _⊑_
⊑ᶜ-refl : Reflexive _⊑ᶜ_

⊑-refl {★} = ★
⊑-refl {$ ι} = $ ι
⊑-refl {A ⇒ B} = ⊑-refl ⇒ ⊑ᶜ-refl
⊑-refl {A ′× B} = ⊑-refl ′× ⊑-refl
⊑-refl {sum Cs As} = sum ⊑+-refl
  where
    ⊑+-refl : ∀ {n} {As : Vec Typeᵛ n} → As ⊑+ As
    ⊑+-refl {As = []} = []
    ⊑+-refl {As = A ∷ As} = ⊑-refl ∷ ⊑+-refl
⊑-refl {tycon _ _} = tycon _ _

⊑ᶜ-refl = ⟨ ⊑ᵉ-refl ⟩ ⊑-refl
