open import Type.Constants
module Core.Progress (ℂ : Constants) where

open Constants ℂ

open import Type ℂ
open import Type.Precision ℂ
open import Type.Subtyping ℂ
open import Type.Context ℂ
open import Core ℂ
open import Core.Substitution ℂ
open import Utils

open import Function.Base using (case_of_)
open import Data.Nat.Base using (ℕ; zero; suc; _+_)
open import Data.Fin.Base using (Fin)
open import Data.Bool.Base using (true; false; if_then_else_) renaming (Bool to 𝔹)
open import Data.List.Base using (List; _∷_; []; _++_)
open import Data.List.Relation.Unary.All as All using (All; _∷_; [])
open import Data.Vec.Base using (Vec)
open import Data.Vec.Relation.Unary.All as VecAll using () renaming (All to VecAll)
open import Data.Vec.Relation.Binary.Pointwise.Inductive as VecPw using (Pointwise; []; _∷_)
open import Data.Product using (_×_; _,_; proj₁; proj₂; Σ; ∃; Σ-syntax; ∃-syntax)
open import Relation.Binary.PropositionalEquality using (_≡_; _≢_; refl; subst; sym)
open import Relation.Nullary using (¬_; Dec; yes; no)

private
  variable
    Γ : Context
    A A′ B B′ : Typeᵛ
    P Q : Typeᶜ
    E E′ F : Effects
    ±q : E ⊆¿ F
    ±p : A => B
    V : Γ ⊢ᵛ A

-- * Operational semantics

-- ** Evaluation contexts

-- An evaluation context
--     Γ ⊢⟦ P ⟧ Q
-- is a "hole" to be plugged with a computation of type Q
-- to yield a computation of type P.

infix 1 _⊢⟦_⟧¹_
infix 1 _⊢⟦_⟧_

-- Frames, or one-constructor contexts
data _⊢⟦_⟧¹_ (Γ : Context) : (P Q : Typeᶜ) → Set where

  □>>=_ :
      Γ ▷ A ⊢ᶜ ⟨ E ⟩ B
      -----
    → Γ ⊢⟦ ⟨ E ⟩ A ⟧¹ ⟨ E ⟩ B

  handle□by_ :
      Γ ⊢ʰ P ==> Q
      -----
    → Γ ⊢⟦ P ⟧¹ Q

  □＠⟨_⟩ :
      E ⊆¿ F
      -----
    → Γ ⊢⟦ ⟨ E ⟩ A ⟧¹ ⟨ F ⟩ A

-- Notations:
-- - C ⟦ M ⟧     plug a hole (□ in) C with a term M
-- - c ⟦ M ⟧¹    idem, but replacing C with a single frame c
-- - C ⟦ c ⟧□¹   idem, but replacing M with a single frame c
--               (and this is actually a constructor for the data type _⊢⟦_⟧_)
-- - C ⟦ C′ ⟧□   idem, but replacing M with another hole C′
-- - C ⟦& C′ ⟧□  idem, but C and C′ are holes that do not handle a given effect
--               (technically, they are proofs of "do not handle a given effect"
--               for some unnamed contexts)

infix 9 _⟦_⟧
infix 9 _⟦_⟧¹
infix 9 _¹⟦_⟧□

-- Plug a frame
_⟦_⟧¹ : ∀ {Γ P Q} → Γ ⊢⟦ P ⟧¹ Q → Γ ⊢ᶜ P → Γ ⊢ᶜ Q
(□>>= N) ⟦ M ⟧¹ = M >>= N
(handle□by H) ⟦ M ⟧¹ = handle M by H
(□＠⟨ q ⟩) ⟦ M ⟧¹ = M ＠⟨ q ⟩

-- TODO: simplify names

data handled-⊆¿ (e : 𝔼) : ∀ {E F} → E ⊆¿ F → Set where
  bad-downcast : ∀ {F} → ¬ e ∈ F → handled-⊆¿ e (¿¡ {E = F})

handled-⊆¿-? : ∀ {E F} e (p : E ⊆¿ F) → Dec (handled-⊆¿ e p)
handled-⊆¿-? e (¿¡ {E = F}) with e ∈? F
... | yes e∈F = no λ { (bad-downcast ¬e∈F) → ¬e∈F e∈F }
... | no ¬e∈F = yes (bad-downcast ¬e∈F)
handled-⊆¿-? e ¿¿ = no λ()
handled-⊆¿-? e ¡¿ = no λ()
handled-⊆¿-? e (¡¡ _) = no λ()

data unhandled¹ (e : 𝔼) {Γ} : ∀ {P Q} → Γ ⊢⟦ P ⟧¹ Q → Set where
  □>>=_ : ∀ {A P}
    → (M : Γ ▷ A ⊢ᶜ P)
      ------
    → unhandled¹ e (□>>= M)

  handle□by_ : ∀ {P Q}
    → {H : Γ ⊢ʰ P ==> Q}
    → ¬ e ∈ Hooks H
      -----
    → unhandled¹ e (handle□by H)

  □＠⟨_⟩ : ∀ {q : E ⊆¿ F}
    → ¬ handled-⊆¿ e q
      ------
    → unhandled¹ e {P = ⟨ E ⟩ A} (□＠⟨ q ⟩)

---

data _⊢⟦_⟧_ (Γ : Context) (P₀ : Typeᶜ) : Typeᶜ → Set where

  □ : Γ ⊢⟦ P₀ ⟧ P₀

  _¹⟦_⟧□ : ∀ {P Q}
    → Γ ⊢⟦ P ⟧¹ Q
    → Γ ⊢⟦ P₀ ⟧ P
      ------
    → Γ ⊢⟦ P₀ ⟧ Q

_⟦_⟧ : ∀{Γ P Q} → Γ ⊢⟦ P ⟧ Q → Γ ⊢ᶜ P → Γ ⊢ᶜ Q
□ ⟦ M ⟧         = M
c ¹⟦ C ⟧□ ⟦ M ⟧ = c ⟦ C ⟦ M ⟧ ⟧¹

data unhandled (e : 𝔼) {Γ P₀} : ∀ {Q} → Γ ⊢⟦ P₀ ⟧ Q → Set where
  □ : unhandled e □

  _¹⟦_⟧□ : ∀ {P Q C} {c : Γ ⊢⟦ P ⟧¹ Q}
    → unhandled¹ e c
    → unhandled e C
      -----
    → unhandled e (c ¹⟦ C ⟧□)

ren□¹ : Renaming₂ _⊢⟦_⟧¹_
ren□¹ ρ (□>>= M) = □>>= renᶜ (ren▷ ρ) M
ren□¹ ρ (handle□by H) = handle□by renʰ ρ H
ren□¹ ρ (□＠⟨ q ⟩) = □＠⟨ q ⟩

ren□ : Renaming₂ _⊢⟦_⟧_
ren□ ρ □ = □
ren□ ρ (c ¹⟦ C ⟧□) = ren□¹ ρ c ¹⟦ ren□ ρ C ⟧□

lift□ : ∀ {Γ A} → Reframing₂ _⊢⟦_⟧_ Γ (Γ ▷ A)
lift□ = ren□ S_

infix  6 _==>_

-- Decomposing casts
data _==>_ : Typeᵛ → Typeᵛ → Set where

  id : ∀ {A}
      -------
    → A ==> A

  _′×_ : ∀ {A B A′ B′}
    → A => A′
    → B => B′
      -----
    → (A ′× B) ==> (A′ ′× B′)

  _⇒⟨_⟩_ : ∀ {A A′ E E′ B B′}
    → A′ => A
    → E ⊆¿ E′
    → B => B′
      -----------------
    → A ⇒ ⟨ E ⟩ B ==> A′ ⇒ ⟨ E′ ⟩ B′

  sum : ∀ {Cs} {As Bs}
    → Pointwise _=>_ As Bs
    → sum Cs As ==> sum Cs Bs

  other : ∀ {A B}
      -------
    → A ==> B

split : ∀ {A B} → A => B → A ==> B
split id      = id
split (+ ★)   = id
split (- ★)   = id
split (* ★)   = id
split (+ $ _) = id
split (- $ _) = id
split (* $ _) = id
split (+ tycon _ _) = id
split (- tycon _ _) = id
split (* tycon _ _) = id
split (+ sum as) = sum (VecPw.map +_ as)
split (- sum as) = sum (flip-VecPw (VecPw.map -_ as))
split (* sum as) = sum (VecPw.map *_ as)
split (+ a ′× b) = (+ a) ′× (+ b)
split (- a ′× b) = (- a) ′× (- b)
split (+ s ⇒ ⟨ e ⟩ t)  =  (- s) ⇒⟨ up e ⟩ (+ t)
  where up : ∀ {E F} → E ≤ᵉ F → E ⊆¿ F
        up ¡¿ = ¡¿
        up ¡¡ = ⊆¿-refl
        up ¿¿ = ¿¿
split (- s ⇒ ⟨ e ⟩ t)  =  (+ s) ⇒⟨ down e ⟩ (- t)
  where down : ∀ {E F} → E ≤ᵉ F → F ⊆¿ E
        down ¡¿ = ¿¡
        down ¡¡ = ⊆¿-refl
        down ¿¿ = ¿¿
split (+ p ⇑ g)  = other
split (- p ⇑ g)  = other
split (* a ′× b) = (* a) ′× (* b)
split (* a ⇒ ⟨ e ⟩ b) = (* a) ⇒⟨ sub e ⟩ (* b)
  where sub : ∀ {E F} → E ⊑ᵉ F → E ⊆¿ F
        sub (¡ E⊆F) = ¡¡ E⊆F
        sub ¿ = ¿¿

-- f ＠ A ⇒ ⟨ E ⟩ B => A′ ⇒ ⟨ E′ ⟩ B′
-- ↦
-- λ x → (f (x ＠ A′ => A) @ ⟨ E ⟩ B => ⟨ E′ ⟩ B′)
wrap-λ : A′ => A → E ⊆¿ E′ → B => B′ → Γ ⊢ᵛ A ⇒ ⟨ E ⟩ B → Γ ⊢ᵛ A′ ⇒ ⟨ E′ ⟩ B′
wrap-λ ∓s ±e ±t M =
  ƛ (((` Z) ＠ ∓s)
    >>= (renᵛ (λ x → S S S x) M · (` Z) ＠⟨ ±e ⟩ ±t))

infixr 1 _↦_
data _↦_ {Γ : Context} : ∀ {P} → Γ ⊢ᶜ P → Γ ⊢ᶜ P → Set where
  β : ∀ {A P} {M} {N : Γ ▷ A ⇒ P ▷ A ⊢ᶜ P}
      ----------
    → (ƛ N) · M ↦ N [ liftᵛ M ] [ ƛ N ]

  δ : ∀ {ι ι′ ι″} {_⊕_ : rep ι → rep ι′ → rep ι″} {k : rep ι} {k′ : rep ι′}
      --------------------------------------------
    → _⦅_⦆_ ($ k) _⊕_ ($ k′) ↦ return ($ (k ⊕ k′))

  unpair-pair : ∀ {A B P} {V W} {M : Γ ▷ A ▷ B ⊢ᶜ P}
      ------
    → unpair (V , W) M ↦ M [ liftᵛ W ] [ V ]

  if-bool : ∀ {b P} {M N : Γ ⊢ᶜ P}
      ------
    → (if ($ b) M N) ↦ (if b then M else N)

  return->>= : ∀ {A P V} {M : Γ ▷ A ⊢ᶜ P}
      ------
    → return V >>= M ↦ M [ V ]

  handle-return : ∀ {E A P} {V : Γ ⊢ᵛ A}
      {H : Γ ⊢ʰ ⟨ E ⟩ A ==> P}
      ------
    → handle return V by H ↦ H .return-clause [ V ]

  handle-op : ∀ {e P Q C V}
      {H : Γ ⊢ʰ P ==> Q}
    → (e∈Hooks : e ∈ H .Hooks)
      -- TODO: Hooks may contain multiple clauses for the same effect,
      -- and the current semantics let you pick any clause here.
      -- We should probably forbid the list of handled effects from containing duplicates.
    → unhandled e C
      ------
    → handle C ⟦ perform e V ⟧ by H
      ↦  All.lookup (H .perform-clauses) e∈Hooks [ liftᵛ (ƛ renᶜ (ren▷ S_) (handle lift□ C ⟦ return (` Z) ⟧ by liftʰ H)) ] [ V ]
    -- handle C[perform e V] by H     (where H = { perform e X K -> M })
    -- ↦ M[{ƛ Y -> handle C[return Y] by H}/K][V/X]
    --
    -- DeBruijn forces us to substitute the continuation first:
    -- each clause is in a context Γ |> request e |> (response e -> R)

  ＠⟨⟩-blame : ∀ {e E} {V} {C : Γ ⊢⟦ _ ⟧ ⟨ ¿ ⟩ A}
    → ¬ e ∈ E
    → unhandled e C
      ------
    → C ⟦ perform e V ⟧ ＠⟨ ¿¡ {E = E} ⟩ ↦ blame

  ＠⟨⟩-return : ∀ {±q : E ⊆¿ F}
      --------
    → return V ＠⟨ ±q ⟩ ↦ return V

  ＠-id : ∀ {V} {±p : A => A}
    → split ±p ≡ id
      ------
    → _＠_ {E = E} V ±p ↦ return V

  ＠-× : ∀ {A B A′ B′} {V : Γ ⊢ᵛ A} {W} {±p} {±a : A => A′} {±b : B => B′}
    → split ±p ≡ ±a ′× ±b
      -----
    → _＠_ {E = E} (V , W) ±p ↦ (V ＠ ±a >>= (liftᵛ W ＠ ±b >>= return (` (S Z) , ` Z)))

  -- close (apply cast on function)
  ＠-⇒ : ∀ {E0 E E′ A A′ B B′} {M : Γ ⊢ᵛ A ⇒ ⟨ E ⟩ B}
      {∓s : A′ => A} {±t : B => B′} {±e} {±p : A ⇒ ⟨ E ⟩ B => A′ ⇒ ⟨ E′ ⟩ B′}
    → split ±p ≡ ∓s ⇒⟨ ±e ⟩ ±t
      ------
    → _＠_ {E = E0} M ±p ↦ return (wrap-λ ∓s ±e ±t M)

  ＠-sum : ∀ {Cs As Bs} {±p : sum Cs As => sum Cs Bs} {±ps} {i} {M}
    → split ±p ≡ sum ±ps
      -----
    → _＠_ {E = E} (con i M) ±p ↦ M ＠ VecPw.lookup ±ps i >>= return (con i (` Z))

  expand : ∀{A G} {V : Γ ⊢ᵛ A} {p : A ≤ G}
    → (g : Ground G)
      -------------------------------
    → _＠_ {E = E} V (+ p ⇑ g) ↦ V ＠ (+ p) >>= return (` Z ⇑ g)

  collapse : ∀{G A} {V : Γ ⊢ᵛ G} {p : A ≤ G}
    → (g : Ground G)
      --------------------------------
    → _＠_ {E = E} (V ⇑ g) (- p ⇑ g) ↦ V ＠ (- p)

  collide  : ∀{G H A} {V : Γ ⊢ᵛ G} {p : A ≤ H}
    → (g : Ground G)
    → (h : Ground H)
    → G ≢ H
      -----------------------------
    → _＠_ {E = E} (V ⇑ g) (- p ⇑ h) ↦ blame

  Case-con : ∀ {P} {Cs} {As} {i} {M} {Ms : VecAll (λ A → Γ ▷ A ⊢ᶜ P) As}
    → Case (con {Cs = Cs} {As = As} i M) Ms ↦ (VecAll.lookup i Ms) [ M ]

  Open-close : ∀ {E} {T} {As} {M}
    → Open {E = E} (close {T = T} {As = As} M) ↦ return M

infix 1 _—→_

data _—→_ {Γ P} : Γ ⊢ᶜ P → Γ ⊢ᶜ P → Set where

  ξξ : ∀ {Q} {M N : Γ ⊢ᶜ Q}
    → (C : Γ ⊢⟦ Q ⟧ P)
    → M ↦ N
      --------
    → C ⟦ M ⟧ —→ C ⟦ N ⟧

-- * Progress / Evaluator

-- A constructive proof of progress is an evaluator for an intrinsically typed
-- language. Below are two constructions following different approaches:
-- 1. From the point of view of implementing an evaluator, "CEK style".
--    This is the "step" function.
-- 2. From the point of view of proving progress, "direct style".
--    This is the "progress" function.

-- ** Direct style progress proof

data Step{P} : ∅ ⊢ᶜ P → Set where

  -- M steps to some new term N.
  Red : ∀ {M N} → (M —→ N) → Step M

  -- Otherwise, M is a normal form, of the following well-defined shapes (it is not "stuck")

  -- M = return V   terminates with a value.
  Return : ∀ {V} → Step (return V)

  -- M = C ⟦ blame ⟧   raises blame.
  Blame : ∀ {Q} (C : ∅ ⊢⟦ Q ⟧ P) → Step (C ⟦ blame ⟧)

  -- M = C ⟦ perform e V ⟧   raises an unhandled effect.
  -- (Note: unhandled e C implies that e ∈¿ E (TODO: prove this), in other words, effects not in E
  -- must be handled, so they cannot lead to this outcome)
  Unhandled : ∀ e {V C} → unhandled e C → Step (C ⟦ perform e V ⟧)

progress± :
    (V : ∅ ⊢ᵛ A)
  → (±p : A => B)
    --------------------
  → ∃[ M ](_＠_ {E = E} V ±p ↦ M)
progress± v ±p with split ±p in eq
progress± v     _ | id              = _ , ＠-id eq
progress± (_ , _) _ | _ ′× _        = _ , ＠-× eq
progress± (ƛ _) _ | _ ⇒⟨ _ ⟩ _      = _ , ＠-⇒ eq
progress± (con _ _) _ | sum _       = _ , ＠-sum eq
progress± v       (+ _ ⇑ g) | other = _ , expand g
progress± (v ⇑ g) (- _ ⇑ h) | other
    with ground g ≡? ground h
... | yes refl rewrite uniqueG g h = _ , collapse h
... | no  G≢H                      = _ , collide g h G≢H
progress± v     (* p) | other with eq
progress± v     (* ★) | other | ()  -- ???

pattern ⟦_⟧>>=_      C M = (□>>= M)      ¹⟦ C ⟧□
pattern handle⟦_⟧by_ C H = (handle□by H) ¹⟦ C ⟧□
pattern ⟦_⟧＠⟨_⟩     C q = (□＠⟨ q ⟩)    ¹⟦ C ⟧□

progress : ∀ {P} (M : ∅ ⊢ᶜ P) → Step M
progress (return V) = Return
progress (perform e V) = Unhandled e □
progress blame = Blame □
progress (ƛ N · M) = Red (ξξ □ β)
progress ($ k₁ ⦅ f ⦆ $ k₂) = Red (ξξ □ δ)
progress (unpair (V , W) M) = Red (ξξ □ unpair-pair)
progress (if ($ b) M N) = Red (ξξ □ if-bool)
progress (M >>= N) with progress M
... | Return = Red (ξξ □ return->>=)
... | Blame C = Blame (⟦ C ⟧>>= N)
... | Red (ξξ C M↦M′) = Red (ξξ (⟦ C ⟧>>= N) M↦M′)
... | Unhandled e e//R = Unhandled e (⟦ e//R ⟧>>= N)
progress (M ＠⟨ q ⟩) with progress M
... | Blame C = Blame (⟦ C ⟧＠⟨ q ⟩)
... | Red (ξξ C M↦M′) = Red (ξξ (⟦ C ⟧＠⟨ q ⟩) M↦M′)
... | Return = Red (ξξ □ ＠⟨⟩-return)
... | Unhandled e e//R = case handled-⊆¿-? e q of λ where
      (yes (bad-downcast ¬e∈F)) → Red (ξξ □ (＠⟨⟩-blame ¬e∈F e//R))
      (no e//q) → Unhandled e (⟦ e//R ⟧＠⟨ e//q ⟩)
progress (V ＠ p) with progress± V p
... | _ , M↦N = Red (ξξ □ M↦N)
progress (handle M by H) with progress M
... | Return = Red (ξξ □ handle-return)
... | Blame C = Blame (handle⟦ C ⟧by H)
... | Red (ξξ C M↦M′) = Red (ξξ (handle⟦ C ⟧by H) M↦M′)
... | Unhandled e e//R with e ∈? Hooks H
...     | yes e∈H = Red (ξξ □ (handle-op e∈H e//R))
...     | no ¬e∈H = Unhandled e (handle⟦ e//R ⟧by ¬e∈H)
progress (Case (con _ _) _) = Red (ξξ □ Case-con)
progress (Open (close _)) = Red (ξξ □ Open-close)
