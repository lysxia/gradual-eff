open import Type.Constants

module Core.Substitution (ℂ : Constants) where

open Constants ℂ

open import Type ℂ
open import Type.Context ℂ
open import Core ℂ

open import Function.Base using (_∘_; case_of_)
open import Data.List.Relation.Unary.All as All using (All; _∷_; [])
open import Data.List.Base using (List; []; _∷_)
open import Data.Fin.Base using (Fin)
open import Data.Vec.Base using (Vec)
open import Data.Vec.Relation.Unary.All using ([]; _∷_) renaming (All to VecAll)
open import Data.Nat.Base using (zero; suc)
open import Data.Product using (_,_)
open import Data.Unit.Base using (tt)

private
  variable
    Γ Δ : Context

-- * Substitution

-- ** Renamings

-- Renaming
_→ᴿ_ : Context → Context → Set
Γ →ᴿ Δ = ∀ {A} → Γ ∋ A → Δ ∋ A

-- Substitution
_→ˢ_ : Context → Context → Set
Γ →ˢ Δ = ∀ {A} → Γ ∋ A → Δ ⊢ᵛ A

-- A "reframing" f : Reframing _⊢_ Γ Δ is a function which translates
-- a term Γ ⊢ A to a new context Δ ⊢ A.
-- Reframing and Reframing₂ provide a template to construct the types of
-- (renamings|substitutions) for (values|computations|handlers|holes).
Reframing : ∀ {X T : Set} → (_⊢_ : X → T → Set) → (Γ Δ : X) → Set
Reframing _⊢_ Γ Δ = ∀ {A} → Γ ⊢ A → Δ ⊢ A

Reframing₂ : ∀ {X T₁ T₂ : Set} → (_⊢_-_ : X → T₁ → T₂ → Set) → (Γ Δ : X) → Set
Reframing₂ _⊢_-_ Γ Δ = ∀ {A B} → Γ ⊢ A - B → Δ ⊢ A - B

→ᴿ-id : ∀ {Γ} → Γ →ᴿ Γ
→ᴿ-id x = x

ren▷ : ∀ {Γ Δ A}
  → (Γ →ᴿ Δ)
    ----------------------------
  → ((Γ ▷ A) →ᴿ (Δ ▷ A))
ren▷ ρ Z      =  Z
ren▷ ρ (S x)  =  S (ρ x)

ren▷++ : ∀ {Γ Δ As}
  → (Γ →ᴿ Δ)
    ----------------------------
  → ((Γ ▷++ As) →ᴿ (Δ ▷++ As))
ren▷++ {As = []} ρ = ρ
ren▷++ {As = A ∷ As} ρ = ren▷++ {As = As} (ren▷ ρ)

Renaming : ∀ {T} → (Context → T → Set) → Set
Renaming _⊢_ = ∀ {Γ Δ} → (Γ →ᴿ Δ) → Reframing _⊢_ Γ Δ

Renaming₂ : ∀ {T₁ T₂} → (Context → T₁ → T₂ → Set) → Set
Renaming₂ _⊢_ = ∀ {Γ Δ} → (Γ →ᴿ Δ) → Reframing₂ _⊢_ Γ Δ

renᵛ : Renaming _⊢ᵛ_
renᶜ : Renaming _⊢ᶜ_
renʰ : Renaming₂ _⊢ʰ_==>_

renᵛ ρ (` x)          = ` (ρ x)
renᵛ ρ (ƛ N)          = ƛ (renᶜ (ren▷ (ren▷ ρ)) N)
renᵛ ρ ($ k)          = $ k
renᵛ ρ (M , N)        = renᵛ ρ M , renᵛ ρ N
renᵛ ρ (M ⇑ g)        = (renᵛ ρ M) ⇑ g
renᵛ ρ (con i M)      = con i (renᵛ ρ M)
renᵛ ρ (close M)       = close (renᵛ ρ M)

renᶜ ρ (V ＠ ±p)       = (renᵛ ρ V) ＠ ±p
renᶜ ρ (M ＠⟨ ±q ⟩)    = (renᶜ ρ M) ＠⟨ ±q ⟩
renᶜ ρ blame           = blame
renᶜ ρ (return M)      = return (renᵛ ρ M)
renᶜ ρ (N >>= M)       = renᶜ ρ N >>= renᶜ (ren▷ ρ) M
renᶜ ρ (perform e M)   = perform e (renᵛ ρ M)
renᶜ ρ (handle M by H) = handle renᶜ ρ M by renʰ ρ H
renᶜ ρ (L · M)         = (renᵛ ρ L) · (renᵛ ρ M)
renᶜ ρ (L ⦅ _⊕_ ⦆ M)   = (renᵛ ρ L) ⦅ _⊕_ ⦆ (renᵛ ρ M)
renᶜ ρ (unpair M N)    = unpair (renᵛ ρ M) (renᶜ (ren▷ (ren▷ ρ)) N)
renᶜ ρ (if V M N)      = if (renᵛ ρ V) (renᶜ ρ M) (renᶜ ρ N)
renᶜ {Γ} {Δ} ρ (Case {P} M Ns) = Case (renᵛ ρ M) (ren-Cases ρ Ns)
  where
    ren-Cases : ∀ {n} {As : Vec _ n} →
      (Γ →ᴿ Δ) →
      VecAll (λ A → (Γ ▷ A) ⊢ᶜ P) As →
      VecAll (λ A → (Δ ▷ A) ⊢ᶜ P) As
    ren-Cases ρ [] = []
    ren-Cases ρ (N ∷ Ns) = renᶜ (ren▷ ρ) N ∷ ren-Cases ρ Ns
renᶜ ρ (Open M)      = Open (renᵛ ρ M)

ren-clauses : ∀ {F} → Renaming (Perform-Clauses F)
ren-clauses ρ [] = []
ren-clauses ρ (x ∷ xs) = renᶜ (ren▷ (ren▷ ρ)) x ∷ ren-clauses ρ xs

renʰ ρ H = record
  { Hooks = H .Hooks
  ; Hooks-contained = H .Hooks-contained
  ; perform-clauses = ren-clauses ρ (H .perform-clauses)
  ; return-clause = renᶜ (ren▷ ρ) (H .return-clause) }

liftᵛ : ∀ {Γ A} → Reframing _⊢ᵛ_ Γ (Γ ▷ A)
liftᵛ = renᵛ S_

liftᶜ : ∀ {Γ A} → Reframing _⊢ᶜ_ Γ (Γ ▷ A)
liftᶜ = renᶜ S_

liftʰ : ∀ {Γ A} → Reframing₂ _⊢ʰ_==>_ Γ (Γ ▷ A)
liftʰ = renʰ S_

liftʰ² : ∀ {Γ A B} → Reframing₂ _⊢ʰ_==>_ Γ (Γ ▷ A ▷ B)
liftʰ² M = liftʰ (liftʰ M)

sub▷ : ∀ {Γ Δ A}
  → (Γ →ˢ Δ)
    --------------------------
  → ((Γ ▷ A) →ˢ (Δ ▷ A))
sub▷ σ Z      =  ` Z
sub▷ σ (S x)  =  liftᵛ (σ x)

sub▷++ : ∀ {Γ Δ As}
  → (Γ →ˢ Δ)
    ----------------------------
  → ((Γ ▷++ As) →ˢ (Δ ▷++ As))
sub▷++ {As = []} ρ = ρ
sub▷++ {As = A ∷ As} ρ = sub▷++ {As = As} (sub▷ ρ)

Substitution : ∀ {T} → (Context → T → Set) → Set
Substitution _⊢_ = ∀ {Γ Δ} → (Γ →ˢ Δ) → Reframing _⊢_ Γ Δ

Substitution₂ : ∀ {T₁ T₂} → (Context → T₁ → T₂ → Set) → Set
Substitution₂ _⊢_ = ∀ {Γ Δ} → (Γ →ˢ Δ) → Reframing₂ _⊢_ Γ Δ

subᵛ : Substitution _⊢ᵛ_
subᶜ : Substitution _⊢ᶜ_
subʰ : Substitution₂ _⊢ʰ_==>_

subᵛ σ (` v) = σ v
subᵛ σ (ƛ M) = ƛ (subᶜ (sub▷ (sub▷ σ)) M)
subᵛ σ ($ k) = $ k
subᵛ σ (M , N)   = subᵛ σ M , subᵛ σ N
subᵛ σ (M ⇑ p)   = subᵛ σ M ⇑ p
subᵛ σ (con i M) = con i (subᵛ σ M)
subᵛ σ (close M)       = close (subᵛ σ M)

subᶜ σ (V ＠ p)        = subᵛ σ V ＠ p
subᶜ σ (M ＠⟨ q ⟩)     = subᶜ σ M ＠⟨ q ⟩
subᶜ σ (return M)      = return (subᵛ σ M)
subᶜ σ (M >>= N)       = subᶜ σ M >>= subᶜ (sub▷ σ) N
subᶜ σ (perform e V)   = perform e (subᵛ σ V)
subᶜ σ (handle M by H) = handle subᶜ σ M by subʰ σ H
subᶜ σ blame           = blame
subᶜ σ (N · M)         = subᵛ σ N · subᵛ σ M
subᶜ σ (M ⦅ f ⦆ N)     = subᵛ σ M ⦅ f ⦆ subᵛ σ N
subᶜ σ (unpair M N)    = unpair (subᵛ σ M) (subᶜ (sub▷ (sub▷ σ)) N)
subᶜ σ (if V M N)      = if (subᵛ σ V) (subᶜ σ M) (subᶜ σ N)
subᶜ {Γ} {Δ} σ (Case {P} M Ns) = Case (subᵛ σ M) (sub-Cases σ Ns)
  where
    sub-Cases : ∀ {n} {As : Vec _ n} →
      (Γ →ˢ Δ) →
      VecAll (λ A → (Γ ▷ A) ⊢ᶜ P) As →
      VecAll (λ A → (Δ ▷ A) ⊢ᶜ P) As
    sub-Cases σ [] = []
    sub-Cases σ (N ∷ Ns) = subᶜ (sub▷ σ) N ∷ sub-Cases σ Ns
subᶜ σ (Open M)      = Open (subᵛ σ M)

sub-clauses : ∀ {F} → Substitution (Perform-Clauses F)
sub-clauses σ [] = []
sub-clauses σ (x ∷ xs) = subᶜ (sub▷ (sub▷ σ)) x ∷ sub-clauses σ xs

subʰ σ H = record
  { Hooks = H .Hooks
  ; Hooks-contained = H .Hooks-contained
  ; perform-clauses = sub-clauses σ (H .perform-clauses)
  ; return-clause = subᶜ (sub▷ σ) (H .return-clause) }

infixl 2 _[_]
infixl 2 _[_]++

-- Substitution for β
_[_] : ∀ {Γ A P} → Γ ▷ A ⊢ᶜ P → Γ ⊢ᵛ A → Γ ⊢ᶜ P
N [ M ] = subᶜ (λ { Z → M ; (S n) → ` n }) N

subs : ∀ {Γ As} → All (Γ ⊢ᵛ_) As → (Γ ▷++ As) →ˢ Γ
subs = go (λ x → ` x)
  where
    go : ∀ {Γ Δ As} → (Δ →ˢ Γ) → All (Γ ⊢ᵛ_) As → (Δ ▷++ As) →ˢ Γ
    go σ [] = σ
    go σ (M ∷ Ms) = go (λ { Z → M ; (S n) → σ n }) Ms

_[_]++ : ∀ {Γ As P} → Γ ▷++ As ⊢ᶜ P → All (Γ ⊢ᵛ_) As → Γ ⊢ᶜ P
N [ Ms ]++ = subᶜ (subs Ms) N
