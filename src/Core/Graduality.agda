{-# OPTIONS --show-implicit #-}
open import Type.Constants
module Core.Graduality (ℂ : Constants) where

open Constants ℂ

open import Type ℂ
open import Type.Precision ℂ
open import Type.Subtyping ℂ
open import Type.Context ℂ
open import Core ℂ
open import Core.Substitution ℂ
open import Core.Progress ℂ hiding (_==>_)
open import ContextPrecision ℂ using (∅)
open import CorePrecision ℂ
open import Utils

open import Function.Base using (case_of_)
open import Data.Nat.Base using (ℕ; zero; suc; _+_)
open import Data.Fin.Base using (Fin)
open import Data.Bool.Base using (true; false; if_then_else_) renaming (Bool to 𝔹)
open import Data.List.Base using (List; _∷_; []; _++_)
open import Data.List.Relation.Unary.All as All using (All; _∷_; [])
open import Data.Vec.Base using (Vec)
open import Data.Vec.Relation.Unary.All as VecAll using () renaming (All to VecAll)
open import Data.Vec.Relation.Binary.Pointwise.Inductive as VecPw using (Pointwise; []; _∷_)
open import Data.Product using (_×_; _,_; proj₁; proj₂; Σ; ∃; Σ-syntax; ∃-syntax)
open import Relation.Binary.PropositionalEquality using (_≡_; _≢_; refl; subst; sym)
open import Relation.Nullary using (¬_; Dec; yes; no)
open import Relation.Binary using (Rel)
open import Relation.Binary.Construct.Closure.ReflexiveTransitive as Star using (Star; _◅_; _◅◅_) renaming (ε to refl)

variable
  Γ Δ : Context
  A₁ A₂ A₂′ B₁ B₂ : Typeᵛ
  P P₁ P₁′ P₂ P₂′ Q Q₁ Q₁′ Q₂ : Typeᶜ
  E₁ E₂ F₁ F₂ : Effects

infix 5 _≤□¹_ _≤□_

data _≤□¹_ {Γ Δ} : Γ ⊢⟦ Q₁ ⟧¹ P₁ → Δ ⊢⟦ Q₂ ⟧¹ P₂ → Set where
  □>>=_ : ∀ {M₁ M₂}
    → Γ ▷ A₁ ⊢ Q₁ ⦂ M₁ ≤ᴹ Δ ▷ A₂ ⊢ Q₂ ⦂ M₂
      ------------------------
    → (□>>= M₁) ≤□¹ (□>>= M₂)

  handle□by_ : ∀ {H₁ H₂}
    → Γ ⊢ P₁ ==> Q₁ ⦂ H₁ ≤ʰ Δ ⊢ P₂ ==> Q₂ ⦂ H₂
      ---------------------------------
    → (handle□by H₁) ≤□¹ (handle□by H₂)

  □＠⟨_⟩ : ∀ {q₁ : E₁ ⊆¿ F₁} {q₂ : E₂ ⊆¿ F₂}
    → F₁ ≤ᵉ F₂
      -----------------------------------------------------------
    → _≤□¹_ {Q₁ = ⟨ E₁ ⟩ A₁} {Q₂ = ⟨ E₂ ⟩ A₂} (□＠⟨ q₁ ⟩) (□＠⟨_⟩ {A = A₂} q₂)

data _≤□_ {Γ Δ Q₁ Q₂} : Γ ⊢⟦ Q₁ ⟧ P₁ → Δ ⊢⟦ Q₂ ⟧ P₂ → Set where
  □ : □ ≤□ □
  _¹⟦_⟧□ : ∀ {c₁ : Γ ⊢⟦ P₁′ ⟧¹ P₁} {c₂ : Δ ⊢⟦ P₂′ ⟧¹ P₂} {C₁ C₂}
    → c₁ ≤□¹ c₂
    → C₁ ≤□ C₂
      ------------------------------
    → (c₁ ¹⟦ C₁ ⟧□) ≤□ (c₂ ¹⟦ C₂ ⟧□)

  □＠₁⟨_⟩¹⟦_⟧□ : ∀ {C₁} {C₂ : Δ ⊢⟦ Q₂ ⟧ ⟨ E₂ ⟩ A₂}
    → {q₁ : E₁ ⊆¿ F₁}
    → F₁ ≤ᵉ E₂
    → C₁ ≤□ C₂
      -----------------------------
    → (□＠⟨_⟩ {A = A₁} q₁) ¹⟦ C₁ ⟧□ ≤□ C₂

  □＠₂⟨_⟩¹⟦_⟧□ : ∀ {C₁ : Γ ⊢⟦ Q₁ ⟧ ⟨ E₁ ⟩ A₁} {C₂}
    → {q₂ : E₂ ⊆¿ F₂}
    → E₁ ≤ᵉ F₂
    → C₁ ≤□ C₂
      -----------------------------
    → C₁ ≤□ (□＠⟨_⟩ {A = A₂} q₂) ¹⟦ C₂ ⟧□

⊢≤-□ : ∀ {C₁ : Γ ⊢⟦ Q₁ ⟧ P₁} {N₁ M₂}
  → C₁ ⟦ N₁ ⟧ ≤ᴹ M₂
  → ∃[ Q₂ ] Σ[ C₂ ∈ Δ ⊢⟦ Q₂ ⟧ P₂ ] ∃[ N₂ ]
        C₂ ⟦ N₂ ⟧ ≡ M₂
      × C₁ ≤□ C₂
      × N₁ ≤ᴹ N₂
⊢≤-□ {C₁ = □} M≤ = _ , □ , _ , refl , □ , M≤
⊢≤-□ {C₁ = ⟦ C₁ ⟧>>= _} (M≤ >>= K≤) with ⊢≤-□ {C₁ = C₁} M≤
... | _ , C₂ , N₂ , refl , C≤ , N≤ = _ , (□>>= _) ¹⟦ C₂ ⟧□ , N₂ , refl , (□>>= K≤) ¹⟦ C≤ ⟧□ , N≤
⊢≤-□ {C₁ = handle⟦ C₁ ⟧by _} (handle M≤ by H≤) with ⊢≤-□ {C₁ = C₁} M≤
... | _ , C₂ , N₂ , refl , C≤ , N≤ = _ , (handle□by _) ¹⟦ C₂ ⟧□ , N₂ , refl , (handle□by H≤) ¹⟦ C≤ ⟧□ , N≤
⊢≤-□ {C₁ = ⟦ _ ⟧＠⟨ _ ⟩} (M≤ ＠₁⟨ E≤ ⟩) with ⊢≤-□ M≤
... | _ , C₂ , N₂ , eq , C≤ , N≤ = _ , C₂ , N₂ , eq , □＠₁⟨ E≤ ⟩¹⟦ C≤ ⟧□ , N≤
⊢≤-□ (M≤ ＠₂⟨ E≤ ⟩) with ⊢≤-□ M≤
... | _ , C₂ , N₂ , refl , C≤ , N≤ = _ , (□＠⟨ _ ⟩) ¹⟦ C₂ ⟧□ , N₂ , refl , □＠₂⟨ E≤ ⟩¹⟦ C≤ ⟧□ , N≤
⊢≤-□ {C₁ = C₁} (M≤ ＠₂ p) with ⊢≤-□ {C₁ = C₁} M≤
... | _ , □ , _ , refl , C≤ , N≤ = _ , □ , _ ＠ _ , refl , ? , ?
... | _ , (□>>= _) ¹⟦ _ ⟧□ , _ , () , _
⊢≤-□ {C₁ = ⟦ C₁ ⟧>>= _} (M≤ !＠₁ p) with ⊢≤-□ {C₁ = C₁} M≤
... | _ , C₂ , N₂ , refl , C≤ , N≤ = _ , C₂ , N₂ , refl , ? , ?
⊢≤-□ {C₁ = C₁} (M≤ !＠₂ p) with ⊢≤-□ {C₁ = C₁} M≤
... | _ = ?

infix 3 _—↠_

_—↠_ : (_ _ : ∅ ⊢ᶜ P) → Set
_—↠_ = Star _—→_

infix 9 _⟦_⟧¹—→ _⟦_⟧¹—↠

_⟦_⟧¹—→ : ∀ (c : ∅ ⊢⟦ P ⟧¹ Q) {M N} → M —→ N → c ⟦ M ⟧¹ —→ c ⟦ N ⟧¹
c ⟦ ξξ C red ⟧¹—→ = ξξ (c ¹⟦ C ⟧□) red

_⟦_⟧¹—↠ : ∀ (c : ∅ ⊢⟦ P ⟧¹ Q) {M N} → M —↠ N → c ⟦ M ⟧¹ —↠ c ⟦ N ⟧¹
c ⟦ refl ⟧¹—↠ = refl
c ⟦ z ◅  zs ⟧¹—↠ = (c ⟦ z ⟧¹—→) ◅ (c ⟦ zs ⟧¹—↠)

postulate
 _[_]≤ : ∀ {M₁ M₂ N₁ N₂}
  → Γ ▷ A₁ ⊢ P₁ ⦂ M₁ ≤ᴹ Δ ▷ A₂ ⊢ P₂ ⦂ M₂
  → Γ ⊢ A₁ ⦂ N₁ ≤ⱽ Δ ⊢ A₂ ⦂ N₂
    ------------------------------
  → Γ ⊢ P₁ ⦂ (M₁ [ N₁ ]) ≤ᴹ Δ ⊢ P₂ ⦂ (M₂ [ N₂ ])

Wrapper-≤G : ∀ {A B G} → Ground G → Wrapper A B → B ≤ G → A ≤ G
Wrapper-≤G () (upcast _) ★
Wrapper-≤G ★⇒¿★ (ƛ-wrap _ _ _) (_ ⇒ _) = A≤★ ⇒ ⟨ ≤¿ ⟩ A≤★

upcast-inv : ∀ {G} {V₁ : ∅ ⊢ᵛ A₁} {V₂ : ∅ ⊢ᵛ ★}
  → (g : Ground G)
  → A₁ ≤ G
  → V₁ ≤ⱽ V₂
  → ∃[ V₂′ ] (V₂′ ⇑ g) ≡ V₂ × (V₁ ≤ⱽ V₂′)
upcast-inv g A≤ (wrap₁ w V≤ B≤) =
  let V₂′ , eq , V′≤ = upcast-inv g (Wrapper-≤G g w A≤) V≤ in
  V₂′ , eq , wrap₁ w V′≤ A≤
upcast-inv g A≤ (wrap₂ (upcast g′) V≤ B≤) =
  case unique-grounding g A≤ g′ (≤ⱽ-≤ ∅ V≤) of λ{ refl
    → case uniqueG g g′ of λ{ refl
    → _ , refl , V≤ }}

-- * Cast lemma

cast+ : ∀ {V₁ : ∅ ⊢ᵛ A₁} {V₂ : ∅ ⊢ᵛ A₂} (p : A₂ ≤ A₂′)
  → ∅ ⊢ A₁ ⦂ V₁ ≤ⱽ ∅ ⊢ A₂ ⦂ V₂
  → A₁ ≤ A₂′ →  -- Note: A₂′, not A₂ !
    ∃[ V₂′ ] V₂ ＠ (+ p) —↠ return V₂′
      × V₁ ≤ⱽ V₂′
cast+ p V≤ A≤ with split (+ p) in eq
cast+ p V≤ A≤ | id = _ , ξξ □ (＠-id eq) ◅ refl , V≤
cast+ p V≤ A≤ | ∓a ⇒⟨ ±e ⟩ ±b = _ , ξξ □ (＠-⇒ eq) ◅ refl , wrap₂ (ƛ-wrap _ _ _) V≤ A≤
cast+ (p ⇑ g) V≤ A≤ | other =
  let V₂′ , steps , V′≤ = cast+ p V≤ (≤-trans (≤ⱽ-≤ ∅ V≤) p) in
  _ ⇑ g , ξξ □ (expand g) ◅ (□>>= _) ⟦ steps ⟧¹—↠ ◅◅ Star.return (ξξ □ return->>=) , wrap₂ (upcast g) V′≤ A≤
cast+ (sum _) _ _ | _ = _
cast+ (_ ′× _) _ _ | _ = _

cast : ∀ {V₁ V₂} (±p : A₂ => A₂′)
  → ∅ ⊢ A₁ ⦂ V₁ ≤ⱽ ∅ ⊢ A₂ ⦂ V₂
  → A₁ ≤ A₂′ →
    ---------------------
    ∃[ V₂′ ] V₂ ＠ ±p —↠ return V₂′
      × V₁ ≤ⱽ V₂′
cast ±p V≤ A≤ with split ±p in eq
cast ±p V≤ A≤ | id = _ , ξξ □ (＠-id eq) ◅ refl , V≤
cast ±p V≤ A≤ | ∓a ⇒⟨ ±e ⟩ ±b = _ , ξξ □ (＠-⇒ eq) ◅ refl , wrap₂ (ƛ-wrap _ _ _) V≤ A≤
cast (+ p ⇑ g) V≤ A≤ | other = cast+ (p ⇑ g) V≤ A≤
{- That clause should be this but Agda's termination checker is confused,
-- as a workaround, we use the duplicate cast+ above
cast (+ p ⇑ g) V≤ A≤ | other =
  let V₂′ , steps , V′≤ = cast (+ p) V≤ (≤-trans (≤ⱽ-≤ ∅ V≤) p) in
  _ ⇑ g , ξξ □ (expand g) ◅ (□>>= _) ⟦ steps ⟧¹—↠ ◅◅ Star.return (ξξ □ return->>=) , wrap₂ (upcast g) V′≤ A≤
-}
cast (- p ⇑ g) V≤ A≤ | other with upcast-inv g (≤-trans A≤ p) V≤
... | _ , refl , V′≤ =
  let V₂′ , steps , V′≤ = cast (- p) V′≤ A≤ in
  V₂′ , ξξ □ (collapse g) ◅ steps , V′≤
cast (+ sum _) _ _ | _ = _
cast (+ _ ′× _) _ _ | _ = _
cast (- sum _) _ _ | _ = _
cast (- _ ′× _) _ _ | _ = _
cast (* sum _) _ _ | _ = _
cast (* _ ′× _) _ _ | _ = _

-- * Catch up lemma

catchup : ∀ {V₁ M₂}
  → ∅ ⊢ ⟨ E₁ ⟩ A₁ ⦂ return V₁ ≤ᴹ ∅ ⊢ ⟨ E₂ ⟩ A₂ ⦂ M₂
  → ∃[ V₂ ] M₂ —↠ return V₂
      × V₁ ≤ⱽ V₂
catchup (return _ V≤) = _ , refl , V≤
catchup (M≤ ＠₂⟨ E≤ ⟩) =
  let V₂ , steps , V≤ = catchup M≤ in
  V₂ , ((□＠⟨ _ ⟩) ⟦ steps ⟧¹—↠) ◅◅ Star.return (ξξ □ ＠⟨⟩-return) , V≤
catchup (return E≤ V≤ ＠₂ A≤) =
  let V₂′ , steps′ , V′≤ = cast _ V≤ A≤ in
  ?
catchup (M≤ !＠₂ A≤) =
  ?

graduality′ : ∀ {M₁ N₁ M₂}
  → M₁ ↦ N₁
  → ∅ ⊢ P₁ ⦂ M₁ ≤ᴹ ∅ ⊢ P₂ ⦂ M₂
    ----------------------
  → ∃[ N₂ ] M₂ —↠ N₂
      × ∅ ⊢ P₁ ⦂ N₁ ≤ᴹ ∅ ⊢ P₂ ⦂ N₂
graduality′ red (M≤ ＠₂⟨ E≤ ⟩) =
  let N₂ , steps , N≤ = graduality′ red M≤ in
  □＠⟨ _ ⟩ ⟦ N₂ ⟧¹ , □＠⟨ _ ⟩ ⟦ steps ⟧¹—↠ , N≤ ＠₂⟨ E≤ ⟩
graduality′ return->>= (return E≤ V≤ >>= N≤) = _ , (ξξ □ return->>=) ◅ refl , N≤ [ V≤ ]≤
graduality′ (＠-id _) (return≤ ＠₁ A≤) =
  let V₂ , steps , V≤ = catchup return≤ in
  return V₂ , steps , return (_≤ᶜ_.effects (≤ᴹ-≤ ∅ return≤)) V≤
graduality′ (＠-⇒ _) (return≤ ＠₁ A≤) = ?
-- graduality′ (＠-× _) (_ , _) = ?
graduality′ red _ = _

postulate
 graduality : ∀ {M₁ N₁ M₂}
  → M₁ —→ N₁
  → ∅ ⊢ P₁ ⦂ M₁ ≤ᴹ ∅ ⊢ P₂ ⦂ M₂
  → ∃[ N₂ ] (∅ ⊢ P₁ ⦂ N₁ ≤ᴹ ∅ ⊢ P₂ ⦂ N₂)
      × M₂ —↠ N₂
-- graduality (ξξ C₁ red) M₁≤ with ⊢≤-□ {C₁ = C₁} M₁≤
-- ... | e = ?
