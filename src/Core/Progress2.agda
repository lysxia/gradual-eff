open import Type.Constants
module Core.Progress2 (ℂ : Constants) where

open Constants ℂ

open import Type ℂ
open import Type.Precision ℂ
open import Type.Subtyping ℂ
open import Type.Context ℂ
open import Core ℂ
open import Core.Substitution ℂ
open import Core.Progress ℂ using (_⊢⟦_⟧¹_; □>>=_; handle□by_; □＠⟨_⟩; _⟦_⟧¹; unhandled¹; handled-⊆¿; ren□¹; split; id; _⇒⟨_⟩_; other; handled-⊆¿-?; bad-downcast; _′×_; sum; wrap-λ)
open import Utils

open import Data.Nat.Base using (ℕ; zero; suc; _+_)
open import Data.Fin.Base using (Fin)
open import Data.Bool.Base using (true; false; if_then_else_) renaming (Bool to 𝔹)
open import Data.List.Base using (List; _∷_; []; _++_)
open import Data.List.Relation.Unary.All as All using (All; _∷_; [])
open import Data.Vec.Base using (Vec)
open import Data.Vec.Relation.Unary.All as VecAll using () renaming (All to VecAll)
open import Data.Vec.Relation.Binary.Pointwise.Inductive as VecPw using (Pointwise; []; _∷_)
open import Data.Product using (_×_; _,_; proj₁; proj₂; Σ; ∃; Σ-syntax; ∃-syntax)
open import Relation.Binary.PropositionalEquality using (_≡_; _≢_; refl; subst; sym)
open import Relation.Nullary using (¬_; Dec; yes; no)

private
  variable
    Γ : Context
    A B : Typeᵛ
    P Q : Typeᶜ
    E F : Effects
    ±q : E ⊆¿ F
    ±p : A => B
    V : Γ ⊢ᵛ A

infix 9 _⟦_⟧
infix 1 _⊢⟦_⟧_
infix 9 _⟦_⟧□¹
infix 9 _⟦_⟧□
infix 9 _⟦&_⟧□

data handled¹ (e : 𝔼) {Γ} : ∀ {P Q} → Γ ⊢⟦ P ⟧¹ Q → Set where
  handle□by_ : ∀ {P Q} {H : Γ ⊢ʰ P ==> Q}
    → e ∈ Hooks H
    → handled¹ e (handle□by H)

  □＠⟨_⟩ : {q : E ⊆¿ F}
    → handled-⊆¿ e q
    → handled¹ e {P = ⟨ E ⟩ A} (□＠⟨ q ⟩)

-- Holes are stacks of frames, with the innermost frame at the top of the stack,
-- which is why the outer type is the parameter (so it comes first in the signature of Hole,
-- and the type arguments are flipped in the _⊢⟦_⟧_ notation), as it remains the same in all
-- suffixes of the stack.
data Hole (Γ : Context) (Q₀ : Typeᶜ) : Typeᶜ → Set

_⊢⟦_⟧_ : Context → Typeᶜ → Typeᶜ → Set
Γ ⊢⟦ P ⟧ Q = Hole Γ Q P

data Hole Γ Q₀ where

  □ : Γ ⊢⟦ Q₀ ⟧ Q₀

  _⟦_⟧□¹ : ∀ {P Q}
    → Γ ⊢⟦ P ⟧ Q₀
    → Γ ⊢⟦ Q ⟧¹ P
      ------
    → Γ ⊢⟦ Q ⟧ Q₀

-- Plug a hole with a computation
_⟦_⟧ : ∀{Γ P Q} → Γ ⊢⟦ P ⟧ Q → Γ ⊢ᶜ P → Γ ⊢ᶜ Q
□ ⟦ M ⟧         = M
C ⟦ c ⟧□¹ ⟦ M ⟧ = C ⟦ c ⟦ M ⟧¹ ⟧

-- Plug a hole with a hole
_⟦_⟧□ : ∀{Γ P₁ P₂ P₃} → Γ ⊢⟦ P₂ ⟧ P₁ → Γ ⊢⟦ P₃ ⟧ P₂ → Γ ⊢⟦ P₃ ⟧ P₁
C ⟦ □ ⟧□ = C
C ⟦ C₁ ⟦ c ⟧□¹ ⟧□ = C ⟦ C₁ ⟧□ ⟦ c ⟧□¹

plug-assoc : ∀{Γ P₁ P₂ P₃}
    {C₁ : Γ ⊢⟦ P₂ ⟧ P₁} {C₂ : Γ ⊢⟦ P₃ ⟧ P₂} {M : Γ ⊢ᶜ P₃}
  → C₁ ⟦ C₂ ⟧□ ⟦ M ⟧ ≡ C₁ ⟦ C₂ ⟦ M ⟧ ⟧
plug-assoc {C₂ = □} = refl
plug-assoc {C₂ = C ⟦ c ⟧□¹} = plug-assoc {C₂ = C}

data unhandled (e : 𝔼) {Γ Q₀} : ∀ {P} → Γ ⊢⟦ P ⟧ Q₀ → Set where
  □ : unhandled e □

  _⟦_⟧□¹ : ∀ {P Q C} {c : Γ ⊢⟦ P ⟧¹ Q}
    → unhandled e C
    → unhandled¹ e c
      -----
    → unhandled e (C ⟦ c ⟧□¹)

-- C ⟦ C' ⟧□  -- composition of contexts
-- _⟦&_⟧□ lemma that composition of unhandled e contexts is handled e
-- The body of this definition is identical to _[_]□ !
_⟦&_⟧□ : ∀{Γ P₁ P₂ P₃} {e} {C : Γ ⊢⟦ P₂ ⟧ P₁} {C′ : Γ ⊢⟦ P₃ ⟧ P₂} → unhandled e C → unhandled e C′ → unhandled e (C ⟦ C′ ⟧□)
C ⟦& □ ⟧□ = C
C ⟦& C′ ⟦ c ⟧□¹ ⟧□ = C ⟦& C′ ⟧□ ⟦ c ⟧□¹

ren□ : Renaming₂ _⊢⟦_⟧_
ren□ ρ □ = □
ren□ ρ (C ⟦ c ⟧□¹) = ren□ ρ C ⟦ ren□¹ ρ c ⟧□¹

lift□ : ∀ {Γ A} → Reframing₂ _⊢⟦_⟧_ Γ (Γ ▷ A)
lift□ = ren□ S_

infixr 1 _↦_
data _↦_ {Γ : Context} : ∀ {P} → Γ ⊢ᶜ P → Γ ⊢ᶜ P → Set where
  β : ∀ {A P} {M} {N : Γ ▷ A ⇒ P ▷ A ⊢ᶜ P}
      ----------
    → (ƛ N) · M ↦ N [ liftᵛ M ] [ ƛ N ]

  δ : ∀ {ι ι′ ι″} {_⊕_ : rep ι → rep ι′ → rep ι″} {k : rep ι} {k′ : rep ι′}
      --------------------------------------------
    → _⦅_⦆_ ($ k) _⊕_ ($ k′) ↦ return ($ (k ⊕ k′))

  unpair-pair : ∀ {A B P} {V W} {M : Γ ▷ A ▷ B ⊢ᶜ P}
      ------
    → unpair (V , W) M ↦ M [ liftᵛ W ] [ V ]

  if-bool : ∀ {b P} {M N : Γ ⊢ᶜ P}
      ------
    → (if ($ b) M N) ↦ (if b then M else N)

  return->>= : ∀ {A P V} {M : Γ ▷ A ⊢ᶜ P}
      ------
    → return V >>= M ↦ M [ V ]

  handle-return : ∀ {E A P} {V : Γ ⊢ᵛ A}
      {H : Γ ⊢ʰ ⟨ E ⟩ A ==> P}
      ------
    → handle return V by H ↦ H .return-clause [ V ]

  handle-op : ∀ {e P Q C V}
      {H : Γ ⊢ʰ P ==> Q}
    → (e∈Hooks : e ∈ H .Hooks)
      -- TODO: Hooks may contain multiple clauses for the same effect,
      -- and the current semantics let you pick any clause here.
      -- We should probably forbid the list of handled effects from containing duplicates.
    → unhandled e C
      ------
    → handle C ⟦ perform e V ⟧ by H
      ↦  All.lookup (H .perform-clauses) e∈Hooks [ liftᵛ (ƛ renᶜ (ren▷ S_) (handle lift□ C ⟦ return (` Z) ⟧ by liftʰ H)) ] [ V ]
    -- handle C[perform e V] by H     (where H = { perform e X K -> M })
    -- ↦ M[{ƛ Y -> handle C[return Y] by H}/K][V/X]
    --
    -- DeBruijn forces us to substitute the continuation first:
    -- each clause is in a context Γ |> request e |> (response e -> R)

  ＠⟨⟩-blame : ∀ {e E} {V} {C : Γ ⊢⟦ _ ⟧ ⟨ ¿ ⟩ A}
    → ¬ e ∈ E
    → unhandled e C
      ------
    → C ⟦ perform e V ⟧ ＠⟨ ¿¡ {E = E} ⟩ ↦ blame

  ＠⟨⟩-return : ∀ {±q : E ⊆¿ F}
      --------
    → return V ＠⟨ ±q ⟩ ↦ return V

  ＠-id : ∀ {V} {±p : A => A}
    → split ±p ≡ id
      ------
    → V ＠ ±p ↦ return V

  ＠-× : ∀ {A B A′ B′} {V : Γ ⊢ᵛ A} {W} {±p} {±a : A => A′} {±b : B => B′}
    → split ±p ≡ ±a ′× ±b
      -----
    → (V , W) ＠ ±p ↦ (V ＠ ±a >>= (liftᵛ W ＠ ±b >>= return (` (S Z) , ` Z)))

  -- close (apply cast on function)
  ＠-⇒ : ∀ {E E′ A A′ B B′} {M : Γ ⊢ᵛ A ⇒ ⟨ E ⟩ B}
      {∓s : A′ => A} {±t : B => B′} {±e} {±p : A ⇒ ⟨ E ⟩ B => A′ ⇒ ⟨ E′ ⟩ B′}
    → split ±p ≡ ∓s ⇒⟨ ±e ⟩ ±t
      ------
    → M ＠ ±p ↦ return (wrap-λ ∓s ±e ±t M)

  ＠-sum : ∀ {Cs As Bs} {±p : sum Cs As => sum Cs Bs} {±ps} {i} {M}
    → split ±p ≡ sum ±ps
      -----
    → (con i M) ＠ ±p ↦ M ＠ VecPw.lookup ±ps i >>= return (con i (` Z))

  expand : ∀{A G} {V : Γ ⊢ᵛ A} {p : A ≤ G}
    → (g : Ground G)
      -------------------------------
    → V ＠ (+ p ⇑ g) ↦ V ＠ (+ p) >>= return (` Z ⇑ g)

  collapse : ∀{G A} {V : Γ ⊢ᵛ G} {p : A ≤ G}
    → (g : Ground G)
      --------------------------------
    → (V ⇑ g) ＠ (- p ⇑ g) ↦ V ＠ (- p)

  collide  : ∀{G H A} {V : Γ ⊢ᵛ G} {p : A ≤ H}
    → (g : Ground G)
    → (h : Ground H)
    → G ≢ H
      -----------------------------
    → (V ⇑ g) ＠ (- p ⇑ h) ↦ blame

  Case-con : ∀ {P} {Cs} {As} {i} {M} {Ms : VecAll (λ A → Γ ▷ A ⊢ᶜ P) As}
    → Case (con {Cs = Cs} {As = As} i M) Ms ↦ (VecAll.lookup i Ms) [ M ]

  Open-close : ∀ {E} {T} {As} {M}
    → Open {E = E} (close {T = T} {As = As} M) ↦ return M

infix 1 _—→_

data _—→_ {Γ P} : Γ ⊢ᶜ P → Γ ⊢ᶜ P → Set where

  ξξ : ∀ {Q} {M N : Γ ⊢ᶜ Q}
    → (C : Γ ⊢⟦ Q ⟧ P)
    → M ↦ N
      --------
    → C ⟦ M ⟧ —→ C ⟦ N ⟧

-- ** CEK style

-- The main function is step:
--   step : ∀ {E A} (M : ∅ ⊢ᶜ ⟨ E ⟩ A) → Step M
-- (note that we evaluate computations M only in the empty context ∅)

-- The type (Step M) characterizes the possible behaviors of a computation M.
-- Note: we may want to just set E to ε,
-- but this generalization would be useful for a "direct-style" progress proof.
data Step {P} : ∅ ⊢ᶜ P → Set where

  -- M steps to some new term N.
  Red : ∀ {M N} → (M —→ N) → Step M

  -- Otherwise, M is a normal form, of the following well-defined shapes (it is not "stuck")

  -- M = return V   terminates with a value.
  Return : ∀ {V} → Step (return V)

  -- M = C ⟦ blame ⟧   raises blame.
  Blame : ∀ {Q} (C : ∅ ⊢⟦ Q ⟧ P) → Step (C ⟦ blame ⟧)

  -- M = C ⟦ perform e V ⟧   raises an unhandled effect.
  -- (Note: unhandled e C implies that e ∈¿ E (TODO: prove this), in other words, effects not in E
  -- must be handled, so they cannot lead to this outcome)
  Unhandled : ∀ {e V C} → unhandled e C → Step (C ⟦ perform e V ⟧)

-- The auxiliary function raise is called when the computation does (perform e V).
-- Look for a frame (handler or downcast) which handles e in the evaluation context C.
-- Accumulate skipped frames on the resumption R (which has the same structure as the evaluation context C),
-- while remembering that the resumption does not handle e.
raise : ∀ e (V : ∅ ⊢ᵛ request e) {P Q}
  → (C : ∅ ⊢⟦ Q ⟧ P)
  → {R : ∅ ⊢⟦ ⟨ ¡ (e ∷ []) ⟩ response e ⟧ Q} → unhandled e R
  → Step (C ⟦ R ⟦ perform e V ⟧ ⟧)

-- Wrapper around the recursive call to raise:
-- Separate the top frame C′ from the evaluation context C and move it to the resumption R
continue-raise : ∀ e (V : ∅ ⊢ᵛ request e) {P Q Q′}
  → (C : ∅ ⊢⟦ Q′ ⟧ P)
  → {c : ∅ ⊢⟦ Q ⟧¹ Q′} → unhandled¹ e c
  → {R : ∅ ⊢⟦ ⟨ ¡ (e ∷ []) ⟩ response e ⟧ Q} → unhandled e R
  → Step (C ⟦ c ⟦ R ⟦ perform e V ⟧ ⟧¹ ⟧)
continue-raise e V C e//C′ {R} e//R
  = subst (λ X → Step (C ⟦ X ⟧))
      (plug-assoc {C₂ = R})
      (raise e V C (□ ⟦ e//C′ ⟧□¹ ⟦& e//R ⟧□))
-- TODO: can we make this look simpler

-- e//R denotes a proof that e is unhandled by R

-- TODO: try factoring C separately
-- Conjectures
-- - PRO: it's as easy with C inside-out as outside-in
-- - PRO: less work for the reader
-- - CON: more work for the executor
raise e V □ e//R = Unhandled e//R
raise e V (C ⟦ □>>= M ⟧□¹) e//R = continue-raise e V C (□>>= M) e//R
raise e V (C ⟦ handle□by H ⟧□¹) e//R with e ∈? H .Hooks
... | yes e∈H = Red (ξξ C (handle-op e∈H e//R))
... | no ¬e∈H = continue-raise e V C (handle□by ¬e∈H) e//R
raise e V (C ⟦ □＠⟨ p ⟩ ⟧□¹) e//R with handled-⊆¿-? e p
... | yes (bad-downcast ¬e∈F) = Red (ξξ C (＠⟨⟩-blame ¬e∈F e//R))
... | no e//p = continue-raise e V C (□＠⟨ e//p ⟩) e//R

-- An alternative way of defining raise
module FactoringHoles where

  -- Given an effect e and a hole C₀, find the longest prefix of C₀ that doesn′t handled e.
  data Factor (e : 𝔼) {Γ : Context} {P Q : Typeᶜ} : Γ ⊢⟦ P ⟧ Q → Set where

    -- Either all of C₀ (renamed to R) leaves e unhandled...
    Unhandled : ∀ {R}
      → unhandled e R
        -----
      → Factor e R

    -- Or we can split C₀ in three, C₀ = C ⟦ c ⟧□¹ ⟦ R ⟧□,
    -- where the resumption R doesn't handle e and the frame c handles e.
    Handled : ∀ {P₁ P₂} C {c : Γ ⊢⟦ P₁ ⟧¹ P₂} {R}
      → handled¹ e c
      → unhandled e R
        -----
      → Factor e (C ⟦ c ⟧□¹ ⟦ R ⟧□)

  -- We can extend a factorization with an unmatching frame c.
  _⟦&_⟧□□¹ : ∀ {e Γ P Q Q′} {C : Γ ⊢⟦ Q ⟧ P} {c : Γ ⊢⟦ Q′ ⟧¹ Q}
    → Factor e C
    → unhandled¹ e c
      -----
    → Factor e (C ⟦ c ⟧□¹)
  Unhandled e//R ⟦& e//c ⟧□□¹ = Unhandled (e//R ⟦ e//c ⟧□¹)
  Handled C e//c₀ e//R ⟦& e//c ⟧□□¹ = Handled C e//c₀ (e//R ⟦ e//c ⟧□¹)

  -- While the next frame c doesn't handle e, we call factor recursively on the
  -- remaining hole and extend the result with c.
  -- When c handles e, we have a trivial factorization of the current hole,
  --     C ⟦ c ⟧□    (using R = □)
  -- Otherwise, we reach the top and e is unhandled.
  factor : ∀ e {Γ P Q} (C : Γ ⊢⟦ P ⟧ Q) → Factor e C
  factor e □ = Unhandled □
  factor e (C ⟦ □>>= M ⟧□¹) = factor e C ⟦& □>>= M ⟧□□¹
  factor e (C ⟦ handle□by H ⟧□¹) with e ∈? Hooks H
  ... | yes e∈H = Handled C (handle□by e∈H) □
  ... | no ¬e∈H = factor e C ⟦& handle□by ¬e∈H ⟧□□¹
  factor e (C ⟦ □＠⟨ p ⟩ ⟧□¹) with handled-⊆¿-? e p
  ... | yes ¬e//p = Handled C (□＠⟨ ¬e//p ⟩) □
  ... | no e//p = factor e C ⟦& □＠⟨ e//p ⟩ ⟧□□¹

  -- Once we have a factorization, it is straightforward to determine the next step
  -- by pattern-matching on the matching frame.
  step-factor : ∀ {e V Q} {C : ∅ ⊢⟦ _ ⟧ Q} → Factor e C → Step (C ⟦ perform e V ⟧)
  step-factor (Unhandled e//R) = Unhandled e//R
  step-factor (Handled C {c} {R} (handle□by e∈H) e//R)
    = subst Step (sym (plug-assoc {C₂ = R})) (Red (ξξ C (handle-op e∈H e//R)))
  step-factor (Handled C {c} {R} (□＠⟨ bad-downcast ¬e∈F ⟩) e//R)
    = subst Step (sym (plug-assoc {C₂ = R})) (Red (ξξ C (＠⟨⟩-blame ¬e∈F e//R)))

  raise′ : ∀ e (V : ∅ ⊢ᵛ request e) {P} → (C : ∅ ⊢⟦ _ ⟧ P) → Step (C ⟦ perform e V ⟧)
  raise′ e V C = step-factor (factor e C)

--

step± : ∀ {A B}
  → (V : ∅ ⊢ᵛ A)
  → (±p : A => B)
    --------------------
  → ∃[ M ](V ＠ ±p ↦ M)
step± v ±p with split ±p in eq
step± v     _   | id            = _ , ＠-id eq
step± (_ , _) _ | _ ′× _        = _ , ＠-× eq
step± (ƛ _) _   | _ ⇒⟨ _ ⟩ _    = _ , ＠-⇒ eq
step± (con i M) _ | sum as      = _ , ＠-sum eq
step± v       (+ _ ⇑ g) | other = _ , expand g
step± (v ⇑ g) (- _ ⇑ h) | other
    with ground g ≡? ground h
... | yes refl rewrite uniqueG g h = _ , collapse h
... | no  G≢H                      = _ , collide g h G≢H
step± v     (* p) | other with eq
step± v     (* ★) | other | ()  -- ???

-- Main loop for the step function.
-- Find the redex in the computation M and reduce it.
-- Accumulate frames on the evaluation context C while traversing the computation.
step□ : ∀ {P Q} (C : ∅ ⊢⟦ P ⟧ Q) (M : ∅ ⊢ᶜ P) → Step (C ⟦ M ⟧)

step□ C (ƛ N · M) = Red (ξξ C β)
step□ C ($ k₁ ⦅ f ⦆ $ k₂) = Red (ξξ C δ)
step□ C (unpair (V , W) M) = Red (ξξ C unpair-pair)
step□ C (if ($ b) M N) = Red (ξξ C if-bool)

-- return
step□ □                     (return V) = Return
step□ (C ⟦ □>>= M ⟧□¹)      (return V) = Red (ξξ C return->>=)
step□ (C ⟦ handle□by H ⟧□¹) (return V) = Red (ξξ C handle-return)
step□ (C ⟦ □＠⟨ q ⟩ ⟧□¹)    (return V) = Red (ξξ C ＠⟨⟩-return)

-- frames
step□ C (M >>= N)       = step□ (C ⟦ □>>= N ⟧□¹) M
step□ C (handle M by H) = step□ (C ⟦ handle□by H ⟧□¹) M
step□ C (M ＠⟨ q ⟩)     = step□ (C ⟦ □＠⟨ q ⟩ ⟧□¹) M

step□ C (V ＠ p) with step± V p
... | M , red = Red (ξξ C red)

step□ C (perform e V) = raise e V C □
step□ C blame = Blame C
step□ C (Case (con _ _) _) = Red (ξξ C Case-con)
step□ C (Open (close _)) = Red (ξξ C Open-close)

-- The step function
step : ∀ {P} (M : ∅ ⊢ᶜ P) → Step M
step = step□ □

--

pattern $ℕ = $ ′ℕ
pattern $𝔹 = $ ′𝔹
pattern $𝕌 = $ ′𝕌
pattern ℕ≤★ = id ⇑ $ℕ
pattern 𝔹≤★ = id ⇑ $𝔹
pattern ℕ⇒¿ℕ≤★ = ℕ≤★ ⇒ ⟨ ¿ ⟩ ℕ≤★ ⇑ ★⇒¿★

infixr 0 _⟼_

data Trace : Set where
  END : Trace
  _⟼_ : ∀ {P} → ∅ ⊢ᶜ P → Trace → Trace

infix 1 _`eval`_

_`eval`_ : ∀ {P} → ℕ → (M : ∅ ⊢ᶜ P) → Trace
zero `eval` M = M ⟼ END
suc n `eval` M with step M
... | Red {N = N} _ = M ⟼ (n `eval` N)
... | _ = M ⟼ END
