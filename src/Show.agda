module Show where

open import Type.Constants
open import Type.Base
open import Type.Context using (∅)
open import SurfaceConstants
import Utils

open import Data.Fin.Base as Fin using (Fin)
open import Data.List.Base as List using (List; _∷_; [])
open import Data.List.Relation.Unary.All using (All; _∷_; [])
open import Data.Vec.Base as Vec using (Vec; _∷_; [])
open import Data.String.Base as String using (String; _++_)

import Data.Bool.Show as Bool
import Data.Nat.Show as Nat

showConstant : ∀ {ι} → rep ι → String
showConstant {ι = ′𝔹} b = Bool.show b
showConstant {ι = ′ℕ} n = Nat.show n
showConstant {ι = ′𝕌} _ = "tt"

module ShowType (ℂ₀ : PreConstants) where

  private
    ℂ : Constants
    ℂ = fromPreConstants ℂ₀

  open Constants ℂ
  open PreRawConstants (PreConstants.preraw ℂ₀) using (𝕋-names)
  open import Core ℂ
  open import Type ℂ

  showEffects : Effects → String
  showEffects ¿ = ""
  showEffects (¡ es) = "[" ++ commasep es ++ "]"
    where
      commasep : List 𝔼 → String
      commasep [] = ""
      commasep (e ∷ []) = e
      commasep (e ∷ es) = e ++ "," ++ commasep es

  showType : Typeᵛ → String
  showType ($ ′𝔹) = "bool"
  showType ($ ′ℕ) = "nat"
  showType ($ ′𝕌) = "unit"
  showType (A ′× B) = "(" ++ showType A ++ "×" ++ showType B ++ ")"
  showType (A ⇒ (⟨ E ⟩ B)) =
    "(" ++ showType A ++ "→" ++ showType B ++ showEffects E ++ ")"
  showType ★ = "?"
  showType (tycon T As) = Vec.lookup 𝕋-names T ++ showTypes As
    where
      showTypes : ∀ {n} → Vec Typeᵛ n → String
      showTypes [] = ""
      showTypes (A ∷ []) = "(" ++ showType A ++ ")"
      showTypes (A ∷ As@(_ ∷ _)) = "(" ++ showType A ++ ", " ++ showTypes As ++ ")"
  showType (sum Cs As) = "[" ++ showSum Cs As ++ "]"
    where
      showSum : ∀ Cs → Vec Typeᵛ (List.length Cs) → String
      showSum [] [] = ""
      showSum (C ∷ []) (A ∷ []) = C ++ " " ++ showType A
      showSum (C ∷ Cs@(_ ∷ _)) (A ∷ As) = C ++ " " ++ showType A ++ " | " ++ showSum Cs As

  showVal : ∀ {A} → ∅ ⊢ᵛ A → String
  showVal ($ κ) = showConstant κ
  showVal (ƛ _) = "λ(...)"
  showVal (V , W) = "(" ++ showVal V ++ "," ++ showVal W ++ ")"
  showVal (V ⇑ _) = "(" ++ showVal V ++ "⇑★)"
  showVal (con {Cs = Cs} i V) = List.lookup Cs i ++ " " ++ showVal V
  showVal (wrap V) = "(wrap" ++ showVal V ++ ")"
