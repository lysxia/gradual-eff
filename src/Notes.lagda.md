Blame calculus with effects
===========================

```
module Notes where
```

Syntax and typing
-----------------

(`Type.lagda.md`, `Core.lagda.md`)

Types are enriched with a type of computations

```txt
-- Value types
A ::= ★ | A ⇒ P | $ ι

-- Computation types
P ::= ⟨ E ⟩ A

-- Gradual effects: a statically known list of effects es, or a dynamic effect ¿
E ::= ¡ e* | ¿
```

Precision on computation types is lifted piecewise.

- `¿` is less precise than any `¡ E`.
- The only other rule is reflexivity.
- Subset inclusion could be added orthogonally as part of subtyping.

The (intrinsically typed) blame calculus is now indexed by a computation type:
`Γ ⊢ ⟨ E ⟩ A`.
It is then extended with effect calls (`perform`), handlers, and effect casts.

`perform`:

```txt
  perform- : ∀ {e}
    → e ∈¿ E
    → response e ≡ A
    → Γ ⊢ ⟨ E ⟩ request e
      --------------------
    → Γ ⊢ ⟨ E ⟩ A
```

List membership is lifted to gradual effects `∈¿` by considering the dynamic effect
`¿` as allowing any effect: `e ∈¿ ¿` for all `e`.

`handle`:

```txt
  handle :
      Γ ⊢ P ➡ Q
    → Γ ⊢ P
      ---------
    → Γ ⊢ Q
```

The judgement `Γ ⊢ P ➡ Q` denotes a handler, which consists of a list of "handled effects"
`Hooks`, with a "perform clause" for each of those effects, and a return clause.
`All` is a type of lists indexed by lists.

```txt
record _⊢_➡_ Γ P Q where
  inductive
  open Typeᶜ
  field
    Hooks : List 𝔼
    Hooks-handled : P .effects ≡ (Hooks ++¿ Q .effects)
    on-return : Γ ▷ P .returns ⊢ Q
    on-perform : On-Perform Γ Q Hooks

On-Perform : Context → Typeᶜ → List 𝔼 → Set
On-Perform Γ Q Hooks = All (λ e → Γ ▷ request e ▷ (response e ⇒ Q) ⊢ Q) Hooks
```

Casts:

```txt
  _＠⟨⟩_ : ∀ {Γ P Q}
    → Γ ⊢ P
    → P =>ᵉᵛ Q  -- Value or effect cast
      ------
    → Γ ⊢ Q
```

Casts are gathered under one sum type `=>ᵉᵛ`. Effect and value casts have quite
different operational semantics, but follow similar rules in the precision
relation on terms. Pattern synonyms let us differentiate between the two kinds of casts:

```txt
pattern _＠_ M ±p = M ＠⟨⟩ ⟨-⟩ ±p     -- Value cast (no ⟨⟩)
pattern _＠⟨_⟩ M ±p = M ＠⟨⟩ ⟨ ±p ⟩-  -- Effect cast (inside ⟨⟩)
```

Operational semantics
---------------------

(`Progress.lagda.md`)

Reduction will preserve types, so values may live under arbitrary effects, not only the
empty effect:

```txt
data Value {Γ E} : Γ ⊢ ⟨ E ⟩ A → Set where
```

Substitution (used in β-reduction and reduction of handlers) may put values under
arbitrary effects, so the `value` function is changed from just returning the underlying term
to reconstructing a term with the right effect:

```txt
value : ∀ {Γ A} {V : Γ ⊢ ⟨ E ⟩ A}
  → (v : Value V)
    -------------
  → ∀ {F} → Γ ⊢ ⟨ F ⟩ A
```

This is used as follows in the β rule:

```txt
  β : ∀ {N : Γ ▷ A ⊢ ⟨ E ⟩ B} {W : Γ ⊢ ⟨ E ⟩ A}
    → (w : Value W)
      --------------------
    → (ƛ N) · W ↦ N [ value w ]
```

Paradoxically, effect casts don't "do" much. Effect casts are like handlers
which let through effects that match their signature.
Unexpected effects are caught and raise blame.
The clause `¬ handled e 𝐸` ensures that effects are caught
by the innermost matching effect cast or handler.

```txt
  castᵉ-blame : ∀ {e} {e∈E′ : e ∈¿ E′} {𝐸 : Frame Γ (⟨ E′ ⟩ response e) (⟨ E ⟩ A)} {V} {M}
      {±p : E =>ᵉ F}
    → ¬ e ∈¿ F
    → ¬ handled e 𝐸
    → Value V
    → M ≡ 𝐸 ⟦ perform e∈E′ V ⟧
      ---------------------------
    → M ＠⟨ ±p ⟩ ↦ blame
```

The following rule applies when the computation under the effect cast is done:

```txt
  castᵉ-value : {V : Γ ⊢ ⟨ E ⟩ A} {±p : E =>ᵉ F}
    → (v : Value V)
      ----------------------
    → (V ＠⟨ ±p ⟩) ↦ value v
```

The progress theorem classifies terms (which are intrinsically typed)
into one of four categories: a term either steps, is a value, raises blame, or performs an effect.

It may be worth considering modelling blame as an effect, merging those last two cases.

```txt
data Progress {P} : (∅ ⊢ P) → Set where

  step : ∀ {M N : ∅ ⊢ P}
    → M —→ N
      ----------
    → Progress M

  done : ∀ {M : ∅ ⊢ P}
    → Value M
      ----------
    → Progress M

  blame : ∀ {Q}
   → (E : Frame ∅ Q P)
     ---------------------
   → Progress (E ⟦ blame ⟧)

  performing : ∀ {e} {V} 𝐸
    → (e∈E : e ∈¿ E)
    → Value V
    → ¬ handled e 𝐸
      ------------------
    → Progress (𝐸 ⟦ perform e∈E V ⟧)
```
