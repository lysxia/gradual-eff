open import Type.Constants

module Gradual2Core (ℂ : Constants) where

open Constants ℂ

open import Type ℂ
open import Type.Precision ℂ
open import Type.Consistency ℂ
open import Type.Subtyping ℂ
open import Type.ConsistentSubtyping ℂ
open import Type.Context ℂ
open import Core ℂ as Core
open import Core.Substitution ℂ
open import Gradual ℂ as Gradual

open import Function.Base using (_∘_)
open import Data.Fin.Base using (Fin; suc)
open import Data.List.Base using (List; []; _∷_)
open import Data.List.Relation.Unary.All as All using (All; []; _∷_)
open import Data.List.Relation.Unary.Any using (here; there)
open import Data.Vec.Base as Vec using (Vec)
open import Data.Vec.Relation.Unary.All as VecAll using ([]; _∷_) renaming (All to VecAll)
open import Data.Nat.Base using (ℕ; zero; suc)
open import Data.Product using (_,_)
open import Relation.Nullary using (yes; no)
open import Relation.Binary.PropositionalEquality using (_≡_; refl)

private
  variable
    Γ : Context
    A A₁ A₂ B A⇒B : Typeᵛ
    P Q : Typeᶜ
    E F : Effects
    e : 𝔼
    ι ι′ ι″ : Base

-- TODO: translate extrinsinc to intrinsic

casts : A ≲ B → Γ ⊢ᶜ ⟨ E ⟩ A → Γ ⊢ᶜ ⟨ E ⟩ B
casts {A} {B} A≲B N with ≲-≥⊑≤ A≲B
... | _ , ≤A , _ , A⊑B , ≤B = N !＠ (- ≤A) !＠ (* A⊑B) !＠ (+ ≤B)

castsᶜ : P ≲ᶜ Q → Γ ⊢ᶜ P → Γ ⊢ᶜ Q
castsᶜ {⟨ E ⟩ _} {⟨ F ⟩ _} (⟨ E⊆¿ ⟩ A≲) M = casts A≲ M ＠⟨ E⊆¿ ⟩

-- Translate gradual calculus to the blame calculus
translate : ∀ {Γ P} -> Γ ⊢ P -> Γ ⊢ᶜ P
translate-handler : ∀ {Γ Eh A Q} → Γ ⊢ A / Eh ==> Q → Γ ⊢ʰ ⟨ Eh ++¿ Q .effects ⟩ A ==> Q

translate (` X) = return (` X)
translate (_⦅_⦆_ M f N) = translate M >>= (liftᶜ (translate N) >>= pure (` S Z ⦅ f ⦆ ` Z))
translate (_·_ M N)
  = translate M >>= (liftᶜ (translate N) >>= (` S Z · ` Z))
translate (M ＠ P≲Q) = castsᶜ P≲Q (translate M)
translate (perform e∈¿E M) = translate M >>= perform _ (` Z) ＠⟨ ∈¿-⊆¿ e∈¿E ⟩
translate (ƛ M) = return (ƛ (translate M))
translate (Let M N) = translate M >>= translate N
translate ($ ι) = return ($ ι)
translate (if M N₁ N₂) = translate M >>= if (` Z) (liftᶜ (translate N₁)) (liftᶜ (translate N₂))
translate (handle M by H) = handle translate M by translate-handler H
translate (M , N) = translate M >>= (liftᶜ (translate N) >>= return ((` S Z) , (` Z)))
translate (unpair M N) = translate M >>= unpair (` Z) (renᶜ (ren▷ (ren▷ S_)) (translate N))
translate (con c M) = translate M >>= return (con c (` Z))
translate (Case M Ms) = translate M >>= Case (` Z) (translates Ms)
  where
    translates : ∀ {n} {As : Vec _ n} {B} →
      VecAll (λ A → (Γ ▷ A) ⊢ P) As →
      VecAll (λ A → (Γ ▷ B ▷ A) ⊢ᶜ P) As
    translates [] = []
    translates (N ∷ Ns) = renᶜ (ren▷ S_) (translate N) ∷ translates Ns
translate (wrap M) = translate M >>= return (close (` Z))
translate (Unwrap M) = translate M >>= Open (` Z)

translate-clauses : ∀ {E Γ Q} → Gradual.Perform-Clauses E Γ Q → Core.Perform-Clauses E Γ Q
translate-clauses [] = []
translate-clauses (M ∷ Ms) = translate M ∷ translate-clauses Ms

translate-handler {_} {Eh} H = record
  { Hooks = Eh
  ; Hooks-contained = ⊆¿-refl
  ; return-clause = translate (Gradual.return-clause H)
  ; perform-clauses = translate-clauses (Gradual.perform-clauses H)
  }
