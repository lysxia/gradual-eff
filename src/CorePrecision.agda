{-# OPTIONS --allow-unsolved-metas #-}
open import Type.Constants
module CorePrecision (ℂ : Constants) where

open Constants ℂ

open import Type ℂ
open import Type.Context ℂ
open import Type.Precision ℂ
open import Type.ConsistentSubtyping ℂ
open import Core ℂ
open import Core.Progress ℂ using (wrap-λ)
open import ContextPrecision ℂ

open import Data.Product using (_×_; _,_; proj₁; proj₂; Σ; ∃; Σ-syntax; ∃-syntax)
open import Relation.Binary.PropositionalEquality using (_≡_; refl)

private
  variable
    Γ Δ : Context
    A A′ A₁ A₁′ A₂ A₂′ B B′ B₁ B₁′ B₂ B₂′ A⇒B : Typeᵛ
    P P₁ P₂ Q Q₁ Q₂ : Typeᶜ
    E E′ E₁ E₁′ E₂ E₂′ F F₁ F₂ : Effects
    ι ι′ ι″ : Base
    V₁ : Γ ⊢ᵛ A₁
    V₂ : Δ ⊢ᵛ A₂
    M₁ : Γ ⊢ᶜ ⟨ E₁ ⟩ A₁
    M₂ : Δ ⊢ᶜ ⟨ E₂ ⟩ A₂

infix 3 _≤ⱽ_ _≤ᴹ_ _≤ʰ_
infix 3 _⊢_⦂_≤ⱽ_⊢_⦂_ _⊢_⦂_≤ᴹ_⊢_⦂_ _⊢_==>_⦂_≤ʰ_⊢_==>_⦂_
infix 4 _>>=_

data _≤ⱽ_ {Γ Δ} : Γ ⊢ᵛ A₁ → Δ ⊢ᵛ A₂ → Set
data _≤ᴹ_ {Γ Δ} : Γ ⊢ᶜ P₁ → Δ ⊢ᶜ P₂  → Set
data _≤ʰ_ {Γ Δ} : Γ ⊢ʰ P₁ ==> Q₁ → Δ ⊢ʰ P₂ ==> Q₂  → Set

_⊢_⦂_≤ⱽ_⊢_⦂_ : ∀ Γ A₁ → Γ ⊢ᵛ A₁ → ∀ Δ A₂ → Δ ⊢ᵛ A₂ → Set
Γ ⊢ A₁ ⦂ M₁ ≤ⱽ Δ ⊢ A₂ ⦂ M₂ = M₁ ≤ⱽ M₂

_⊢_⦂_≤ᴹ_⊢_⦂_ : ∀ Γ P₁ → Γ ⊢ᶜ P₁ → ∀ Δ P₂ → Δ ⊢ᶜ P₂ → Set
Γ ⊢ P₁ ⦂ M₁ ≤ᴹ Δ ⊢ P₂ ⦂ M₂ = M₁ ≤ᴹ M₂

_⊢_==>_⦂_≤ʰ_⊢_==>_⦂_ : ∀ Γ P₁ Q₁ → Γ ⊢ʰ P₁ ==> Q₁ → ∀ Δ P₂ Q₂ → Δ ⊢ʰ P₂ ==> Q₂ → Set
Γ ⊢ P₁ ==> Q₁ ⦂ H₁ ≤ʰ Δ ⊢ P₂ ==> Q₂ ⦂ H₂ = H₁ ≤ʰ H₂

data Wrapper : (_ _ : Typeᵛ) → Set where
  ƛ-wrap : ∀ (∓a : A′ => A) (±e : E ⊆¿ E′) (±b : B => B′) → Wrapper (A ⇒ ⟨ E ⟩ B) (A′ ⇒ ⟨ E′ ⟩ B′)
  upcast : Ground A → Wrapper A ★

wrap : Wrapper A B → Γ ⊢ᵛ A → Γ ⊢ᵛ B
wrap (ƛ-wrap ∓a ±e ±b) = wrap-λ ∓a ±e ±b
wrap (upcast g) = _⇑ g

data _≤ⱽ_ {Γ Δ} where
  `_ : ∀ {v₁ v₂}
    → Γ ∋ A₁ ⦂ v₁ ≤ˣ Δ ∋ A₂ ⦂ v₂
      ----------------------------
    → ` v₁ ≤ⱽ ` v₂

  ƛ_ : ∀ {M₁ M₂}
    → A₁ ⇒ P₁ ≤ A₂ ⇒ P₂
    → Γ ▷ _ ▷ A₁ ⊢ P₁ ⦂ M₁ ≤ᴹ Δ ▷ _ ▷ A₂ ⊢ P₂ ⦂ M₂
      -------------------------------------
    → (ƛ M₁) ≤ⱽ (ƛ M₂)

  wrap₁ : ∀ {V₂ : Δ ⊢ᵛ A₂} (w : Wrapper A₁ B₁)
    → V₁ ≤ⱽ V₂
    → (B₁ ≤ A₂)
    -------------------------------------
    → wrap w V₁ ≤ⱽ V₂

  wrap₂ : ∀ {V₁ : Γ ⊢ᵛ A₁} (w : Wrapper A₂ B₂)
    → V₁ ≤ⱽ V₂
    → (A₁ ≤ B₂)
      -------------------------------------
    → V₁ ≤ⱽ wrap w V₂

data _≤ᴹ_ {Γ Δ} where
  return :
      E₁ ≤ᵉ E₂
    → V₁ ≤ⱽ  V₂
      -------
    → Γ ⊢ ⟨ E₁ ⟩ A₁ ⦂ return V₁ ≤ᴹ Δ ⊢ ⟨ E₂ ⟩ A₂ ⦂ return V₂

  _>>=_ : ∀ {N₁ N₂}
    → M₁ ≤ᴹ M₂
    → Γ ▷ A₁ ⊢ ⟨ E₁ ⟩ B₁ ⦂ N₁ ≤ᴹ Δ ▷ A₂ ⊢ ⟨ E₂ ⟩ B₂ ⦂ N₂
      -------
    → M₁ >>= N₁ ≤ᴹ M₂ >>= N₂

  perform : ∀ {e V₁ V₂}
    → V₁ ≤ⱽ V₂
      -------
    → perform e V₁ ≤ᴹ perform e V₂

  _·_ : ∀ {W₁ W₂}
    → Γ ⊢ A₁ ⇒ ⟨ E₁ ⟩ B₁ ⦂ W₁ ≤ⱽ Δ ⊢ A₂ ⇒ ⟨ E₂ ⟩ B₂ ⦂ W₂
    → V₁ ≤ⱽ V₂
      -------------------------
    → W₁ · V₁ ≤ᴹ W₂ · V₂

  handle_by_ : ∀ {M₁ M₂ H₁ H₂}
    → M₁ ≤ᴹ M₂
    → Γ ⊢ P₁ ==> Q₁ ⦂ H₁ ≤ʰ Δ ⊢ P₂ ==> Q₂ ⦂ H₂
      ----------------------------
    → handle M₁ by H₁ ≤ᴹ handle M₂ by H₂

  _＠₁_ : ∀ {M₂ : Δ ⊢ᶜ ⟨ E₂ ⟩ A₂}
    → return {E = E₁} V₁ ≤ᴹ M₂
    → {p₁ : A₁ => B₁}
    → B₁ ≤ A₂
      -------------------
    → _＠_ {E = E₁} V₁ p₁ ≤ᴹ M₂

  _＠₂_ : ∀ {M₁ : Γ ⊢ᶜ ⟨ E₁ ⟩ A₁}
    → M₁ ≤ᴹ return {E = E₂} V₂
    → {p₂ : A₂ => B₂}
    → A₁ ≤ B₂
      -------------------
    → M₁ ≤ᴹ pure {E = E₂} (V₂ ＠ p₂)

  _!＠₁_ : ∀ {M₂ : Δ ⊢ᶜ ⟨ E₂ ⟩ A₂}
    → M₁ ≤ᴹ M₂
    → {p₁ : A₁ => B₁}
    → B₁ ≤ A₂
      -------------------
    → M₁ !＠ p₁ ≤ᴹ M₂

  _!＠₂_ : ∀ {M₁ : Γ ⊢ᶜ ⟨ E₁ ⟩ A₁}
    → M₁ ≤ᴹ M₂
    → {p₂ : A₂ => B₂}
    → A₁ ≤ B₂
      -------------------
    → M₁ ≤ᴹ M₂ !＠ p₂

  _＠₁⟨_⟩ : ∀ {M₂ : Δ ⊢ᶜ ⟨ E₂ ⟩ A₂}
    → M₁ ≤ᴹ M₂
    → {q₁ : E₁ ⊆¿ F₁}
    → F₁ ≤ᵉ E₂
      -------------------
    → M₁ ＠⟨ q₁ ⟩ ≤ᴹ M₂

  _＠₂⟨_⟩ : ∀ {M₁ : Γ ⊢ᶜ ⟨ E₁ ⟩ A₁}
    → M₁ ≤ᴹ M₂
    → {q₂ : E₂ ⊆¿ F₂}
    → E₁ ≤ᵉ F₂
      -------------------
    → M₁ ≤ᴹ M₂ ＠⟨ q₂ ⟩

data _≤ʰ_ where

≤ⱽ-≤ : ∀ {V₁ : Γ ⊢ᵛ A₁} {V₂ : Δ ⊢ᵛ A₂} → Γ ≤ᴳ Δ → V₁ ≤ⱽ V₂ → A₁ ≤ A₂
≤ᴹ-≤ : ∀ {M₁ : Γ ⊢ᶜ P₁} {M₂ : Δ ⊢ᶜ P₂} → Γ ≤ᴳ Δ → M₁ ≤ᴹ M₂ → P₁ ≤ᶜ P₂
≤ʰ-≤ : Γ ≤ᴳ Δ → ∀ {H₁ H₂}
  → Γ ⊢ P₁ ==> Q₁ ⦂ H₁ ≤ʰ Δ ⊢ P₂ ==> Q₂ ⦂ H₂
  → (P₁ ≤ᶜ P₂) × (Q₁ ≤ᶜ Q₂)

≤ⱽ-≤ Γ≤ (` x≤) = ≤ˣ-≤ Γ≤ x≤
≤ⱽ-≤ Γ≤ (ƛ_ A≤ M≤) = A≤
≤ⱽ-≤ Γ≤ (wrap₁ w M≤ A≤) = A≤
≤ⱽ-≤ Γ≤ (wrap₂ w M≤ A≤) = A≤

≤ᴹ-≤ Γ≤ (return E≤ V≤) = ⟨ E≤ ⟩ ≤ⱽ-≤ Γ≤ V≤
≤ᴹ-≤ Γ≤ (M≤ >>= N≤) with ≤ᴹ-≤ Γ≤ M≤
... | ⟨ _ ⟩ A≤ = ≤ᴹ-≤ (Γ≤ ▷ A≤) N≤
≤ᴹ-≤ Γ≤ (perform V≤) = ≤ᶜ-refl
≤ᴹ-≤ Γ≤ (W≤ · V≤) with ≤ⱽ-≤ Γ≤ W≤
... | _ ⇒ P≤ = P≤
≤ᴹ-≤ Γ≤ (handle M≤ by H≤) = proj₂ (≤ʰ-≤ Γ≤ H≤)
≤ᴹ-≤ Γ≤ (M≤ ＠₁ A≤) with ≤ᴹ-≤ Γ≤ M≤
... | ⟨ E≤ ⟩ _ = ⟨ E≤ ⟩ A≤
≤ᴹ-≤ Γ≤ (M≤ ＠₂ A≤) with ≤ᴹ-≤ Γ≤ M≤
... | ⟨ E≤ ⟩ _ = ⟨ E≤ ⟩ A≤
≤ᴹ-≤ Γ≤ (M≤ !＠₁ A≤) with ≤ᴹ-≤ Γ≤ M≤
... | ⟨ E≤ ⟩ _ = ⟨ E≤ ⟩ A≤
≤ᴹ-≤ Γ≤ (M≤ !＠₂ A≤) with ≤ᴹ-≤ Γ≤ M≤
... | ⟨ E≤ ⟩ _ = ⟨ E≤ ⟩ A≤
≤ᴹ-≤ Γ≤ (M≤ ＠₁⟨ E≤ ⟩) with ≤ᴹ-≤ Γ≤ M≤
... | ⟨ _ ⟩ A≤ = ⟨ E≤ ⟩ A≤
≤ᴹ-≤ Γ≤ (M≤ ＠₂⟨ E≤ ⟩) with ≤ᴹ-≤ Γ≤ M≤
... | ⟨ _ ⟩ A≤ = ⟨ E≤ ⟩ A≤

≤ʰ-≤ Γ≤ ()  -- TODO

≤ᴹ-refl : M₁ ≤ᴹ M₁

≤ⱽ-refl : V₁ ≤ⱽ V₁
≤ⱽ-refl {V₁ = ` _} = ` ≤ˣ-refl
≤ⱽ-refl {V₁ = ƛ x} = ƛ_ ≤-refl ≤ᴹ-refl
≤ⱽ-refl = _  -- need to extend ≤ⱽ

≤ᴹ-refl = _
