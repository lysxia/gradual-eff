module Surface2 where

open import Type.Constants
open import SurfaceConstants using (fromPreConstants; parsePreConstants)
open import Surface2Gradual using (infer0)
open import Surface.AST as Surface using (prog)

import Type
open import Type.Context
import Gradual

open import Level
open import Effect.Monad using (RawMonad)
open import Function.Base using (_∘_)
open import Data.Product using (_×_; _,_; proj₁; proj₂; Σ; ∃; Σ-syntax; ∃-syntax)
open import Data.String using (String)
open import Data.Sum.Base as Sum using (_⊎_; inj₁; inj₂)
open import Data.Sum.Effectful.Left as Either using () renaming (Sumₗ to Either)

Error : Set
Error = SurfaceConstants.Error ⊎ ∃[ ℂ₀ ] Surface2Gradual.Error ℂ₀

showError : Error → String
showError (inj₁ e) = SurfaceConstants.showError e
showError (inj₂ (_ , e)) = Surface2Gradual.showError _ e

open RawMonad (Either.monad Error Level.zero)

GradualProgram : Set
GradualProgram = ∃[ ℂ₀ ]
  let ℂ = fromPreConstants ℂ₀
      open Type ℂ
      open Gradual ℂ
  in ∃[ A ] (∅ ⊢ ⟨ ε ⟩ A)

gradualProgram : Surface.Program → Error ⊎ GradualProgram
gradualProgram (prog Ds E) = do
  ℂ₀ ← Sum.map₁ inj₁ (parsePreConstants Ds)
  (A , M) ← Sum.map₁ (inj₂ ∘ (ℂ₀ ,_)) (infer0 ℂ₀ E)
  pure (ℂ₀ , A , M)
