open import Type.Constants

module Core (ℂ : Constants) where

open Constants ℂ

open import Type ℂ
open import Type.Precision ℂ
open import Type.Context ℂ
open import Type.Subtyping ℂ

open import Data.Fin.Base using (Fin)
open import Data.List.Base as List using (List; _∷_; [])
open import Data.List.Relation.Unary.All as List using (All)
open import Data.List.Relation.Binary.Subset.Propositional.Properties
open import Data.Vec.Relation.Unary.All as VecAll using () renaming (All to VecAll)
import Data.Vec.Base as Vec

private
  variable
    Γ : Context
    E F : Effects 
    P Q : Typeᶜ
    A B : Typeᵛ
    ι ι′ ι″ : Base

-- * Casts

infix  6 _=>_
infix  4 +_
infix  4 -_
infix  4 *_

-- Casts go either up or down along the precision relation
data _=>_ : (A B : Typeᵛ) → Set where
  id  : A => A
  +_ : A ≤ B → A => B
  -_ : B ≤ A → A => B
  *_ : A ⊑ B → A => B

-- * Syntax

infix  1 _⊢ᵛ_
infix  1 _⊢ᶜ_
infix  1 _⊢ʰ_==>_
infixl 5 _＠_ _＠⟨_⟩
infix  6 _·_
infix  6 _⦅_⦆_
infix  8 `_
infixl 4 _>>=_

data _⊢ᵛ_ Γ : Typeᵛ → Set
data _⊢ᶜ_ Γ : Typeᶜ → Set
record _⊢ʰ_==>_ Γ P Q : Set

-- Values
data _⊢ᵛ_ Γ where

  `_ :
      Γ ∋ A
      ------- Variable
    → Γ ⊢ᵛ A

  ƛ_ :
      Γ ▷ (A ⇒ P) ▷ A ⊢ᶜ P
      ------- (Recursive) Function
    → Γ ⊢ᵛ A ⇒ P

  $_ :
      rep ι
      ------- Base value
    → Γ ⊢ᵛ $ ι

  _,_ :
      Γ ⊢ᵛ A
    → Γ ⊢ᵛ B
      ------- Pair
    → Γ ⊢ᵛ A ′× B

  _⇑_ : ∀ {G}
    → Γ ⊢ᵛ G
    → Ground G
      ------- Box(ed value)
    → Γ ⊢ᵛ ★

  con : ∀ {Cs As}
    → (i : Fin (List.length Cs))
    → Γ ⊢ᵛ Vec.lookup As i
    → Γ ⊢ᵛ sum Cs As

  close : {T : 𝕋} {As : TySub (arity T) ∅}
    → Γ ⊢ᵛ subTy As (tyDef T)
    → Γ ⊢ᵛ tycon T As

-- Computations
data _⊢ᶜ_ Γ where

  -- Return a value
  --
  -- - Having return be polymorphic in E allows
  --       return V >>= M
  --   to be well-typed.
  --
  -- - Computations that terminate normally reduce to a (return _)
  return :
      Γ ⊢ᵛ A
      ------
    → Γ ⊢ᶜ ⟨ E ⟩ A

  -- Sequence computations
  -- They must have the same effects E
  _>>=_ :
      Γ ⊢ᶜ ⟨ E ⟩ A
    → Γ ▷ A ⊢ᶜ ⟨ E ⟩ B
      -------
    → Γ ⊢ᶜ ⟨ E ⟩ B

  -- Perform an effect
  perform : ∀ e
    → Γ ⊢ᵛ request e
      ------
    → Γ ⊢ᶜ ⟨ ¡ (e ∷ []) ⟩ response e

  -- Handle some effects
  --
  -- - Note: handlers are second class syntactically (they are not values),
  --   but a handler P ==> Q can be wrapped as a function (Unit ⇒ P) ⇒ Q
  -- TODO: Add Unit type
  handle_by_ :
      Γ ⊢ᶜ P
    → Γ ⊢ʰ P ==> Q
      -----
    → Γ ⊢ᶜ Q

  -- Cast a value to another type
  -- - ＠ is FULLWIDTH COMMERCIAL AT,
  --   different from COMMERCIAL AT which is reserved by Agda.
  _＠_ :
      Γ ⊢ᵛ A
    → A => B
      ------
    → Γ ⊢ᶜ ⟨ E ⟩ B

  -- Cast on computations
  _＠⟨_⟩ :
      Γ ⊢ᶜ ⟨ E ⟩ A
    → E ⊆¿ F
      ------
    → Γ ⊢ᶜ ⟨ F ⟩ A

  -- Raise blame
  -- Should blame be an effect?
  -- What theorems get weaker if it is?
  blame : Γ ⊢ᶜ P

  -- Apply a function
  _·_ :
      Γ ⊢ᵛ A ⇒ P
    → Γ ⊢ᵛ A
      ---------
    → Γ ⊢ᶜ P

  -- Apply an operator (meta-level function on base types)
  _⦅_⦆_ :
      Γ ⊢ᵛ $ ι
    → (rep ι → rep ι′ → rep ι″)
    → Γ ⊢ᵛ $ ι′
      ----------------------
    → Γ ⊢ᶜ ⟨ ε ⟩ $ ι″

  -- Eliminate a pair
  unpair :
      Γ ⊢ᵛ A ′× B
    → Γ ▷ A ▷ B ⊢ᶜ P
      ------
    → Γ ⊢ᶜ P

  -- Eliminate bool
  if :
      Γ ⊢ᵛ $ ′𝔹
    → (M N : Γ ⊢ᶜ P)
      -------
    → Γ ⊢ᶜ P

  Case : ∀ {Cs As}
    → Γ ⊢ᵛ sum Cs As
    → VecAll (λ A → Γ ▷ A ⊢ᶜ P) As
      ------
    → Γ ⊢ᶜ P

  Open : ∀ {T As}
    → Γ ⊢ᵛ tycon T As
      --------------
    → Γ ⊢ᶜ ⟨ E ⟩ subTy As (tyDef T)

-- A handler contains a list of perform clauses of the form
--     { perform(e) x k -> M }
-- where  x : request e,  k : response e -> Q,  M : Q
-- for every effect e ∈ Hooks.
Perform-Clauses : List 𝔼 → Context → Typeᶜ → Set
Perform-Clauses Hooks Γ Q
  = All (λ e → Γ ▷ request e ▷ (response e ⇒ Q) ⊢ᶜ Q) Hooks

-- Handlers
-- Map computations of type P to Q
record _⊢ʰ_==>_ Γ P Q where
  inductive
  constructor handler
  field
    Hooks : List 𝔼
    Hooks-contained : P .effects ⊆¿ Hooks ++¿ Q .effects
    perform-clauses : Perform-Clauses Hooks Γ Q
    return-clause : Γ ▷ P .returns ⊢ᶜ Q

open _⊢ʰ_==>_ public

pure : Γ ⊢ᶜ ⟨ ε ⟩ A → Γ ⊢ᶜ ⟨ E ⟩ A
pure M = M ＠⟨ ε⊆¿ ⟩

infixl 5 _＠⟨_⟩_ _!＠_

_!＠_ : Γ ⊢ᶜ ⟨ E ⟩ A → A => B → Γ ⊢ᶜ ⟨ E ⟩ B
M !＠ ±p = M >>= (` Z) ＠ ±p

_＠⟨_⟩_ : Γ ⊢ᶜ ⟨ E ⟩ A → E ⊆¿ F → A => B → Γ ⊢ᶜ ⟨ F ⟩ B
M ＠⟨ ±q ⟩ ±p = (M ＠⟨ ±q ⟩) !＠ ±p
