open import Type.Constants
open import SurfaceConstants using (PreConstants; fromPreConstants)

module Surface2Gradual (ℂ₀ : PreConstants) where

ℂ : Constants
ℂ = fromPreConstants ℂ₀

open Constants ℂ

open import Type ℂ
open import Type.Context ℂ
open import Type.Precision ℂ
open import Type.Consistency ℂ
open import Type.Subtyping ℂ
open import Type.ConsistentSubtyping ℂ
open import Gradual ℂ as Gradual

open import Surface.AST as Surface
open import Show
open Show.ShowType ℂ₀
open import Utils

import Level
open import Effect.Monad using (RawMonad)
open import Data.Maybe.Base using (Maybe; just; nothing)
open import Data.Bool.Base as Bool using (Bool; true; false)
open import Data.Integer.Base using () renaming (∣_∣ to abs)
open import Function.Base using (_∘_)
open import Data.Fin as Fin using (Fin)
import Data.Fin.Properties as Fin
open import Data.List.Base as List using (List; []; _∷_; map)
open import Data.List.Relation.Unary.All as All using (All; []; _∷_)
import Data.List.Relation.Unary.Any as Any
open import Data.Vec.Base as Vec using (Vec; []; _∷_)
open import Data.Vec.Relation.Unary.All as VecAll using ([]; _∷_) renaming (All to VecAll)
import Data.Vec.Relation.Unary.Any as Vec using (index)
import Data.Vec.Membership.DecPropositional as VecDecMembership
open import Data.String as String using (String; _++_)
open import Data.Unit.Base using (⊤; tt)
open import Data.Sum.Base as Sum using (_⊎_; inj₁; inj₂)
open import Data.Sum.Effectful.Left as Either using () renaming (Sumₗ to Either)
open import Data.Product as Prod using (_×_; _,_; proj₁; proj₂; Σ; ∃; Σ-syntax; ∃-syntax)
open import Data.Nat.Base as Nat using (zero; suc)
open import Function.Base using (case_of_)
open import Relation.Binary.PropositionalEquality using (_≡_; _≢_; refl)
open import Relation.Binary using (Decidable)
open import Relation.Nullary using (¬_; Dec; yes; no)
open import Relation.Nullary.Decidable using (isYes)
open import Relation.Nullary.Negation.Core renaming (contraposition to ¬map)

private
  variable
    Γ : Context
    E : Effects
    P Q : Typeᶜ
    A B : Typeᵛ

-- * Typechecking monad

-- ** Errors

data Error : Set where
  UnboundVariable : String → Error
  TypeMismatch : ∀ (actual expected : Typeᵛ) → ¬ (actual ≲ expected) → Error
  ParseTypeError : SurfaceConstants.Error → Error
  Todo : String -> Error

open RawMonad (Either.monad Error Level.zero)

pattern Err e = inj₁ e

showError : Error → String
showError (UnboundVariable v) = "Unbound variable " ++ v
showError (TypeMismatch actual expected _) =
  "Mismatched types; actual: " ++ showType actual ++ " ; expected: " ++ showType expected
showError (ParseTypeError e) = SurfaceConstants.showError e
showError (Todo s) = "TODO " ++ s

module ParseType = SurfaceConstants.ParseType (PreConstants.preraw ℂ₀)

parseType : Surface.Type → Error ⊎ Typeᵛ
parseType = Sum.map₁ ParseTypeError ∘ ParseType.parseType []

parseTypeᶜ : Surface.CType → Error ⊎ Typeᶜ
parseTypeᶜ = Sum.map₁ ParseTypeError ∘ ParseType.parseTypeᶜ []

-- * Parsing operations

mkOp : Surface.Op → ∃[ κ₁ ] ∃[ κ₂ ] ∃[ κ₃ ] (rep κ₁ → rep κ₂ → rep κ₃)
mkOp opAdd = (′ℕ , ′ℕ , ′ℕ , Nat._+_)
mkOp opSub = (′ℕ , ′ℕ , ′ℕ , Nat._∸_)
mkOp opMul = (′ℕ , ′ℕ , ′ℕ , Nat._*_)
mkOp opEq =  (′ℕ , ′ℕ , ′𝔹 , Nat._≡ᵇ_)
mkOp opNEq = (′ℕ , ′ℕ , ′𝔹 , λ x y → Bool.not (x Nat.≡ᵇ y))
mkOp opAnd = (′𝔹 , ′𝔹 , ′𝔹 , Bool._∧_)
mkOp opOr  = (′𝔹 , ′𝔹 , ′𝔹 , Bool._∨_)

-- Patterns are either identifiers or wildcards
from-Pat : Pat → Maybe String
from-Pat (pId (ident i)) = just i
from-Pat pWild = nothing

-- * Type inference

infixl 6 _▷_/_

-- Mappings from source variables to target types
-- Indexed by the target context
data CxMap : Context → Set where
  ∅ : CxMap ∅
  _▷_/_ : CxMap Γ → Maybe String → ∀ A → CxMap (Γ ▷ A)

-- Lookup a variable in the context and get its type
lookup : CxMap Γ → String → Maybe (∃[ A ] Γ ∋ A)
lookup ∅ _ = nothing
lookup (ℾ ▷ y / A) x with match y x
  where
    match : Maybe String -> String -> _
    match (just y) x = isYes (y ≟ x)
    match nothing _ = false
... | true = just (_ , Z)
... | false with lookup ℾ x
...   | just (_ , Γ∋A) = just (_ , S Γ∋A)
...   | nothing = nothing

-- Cast the empty effect to any effect
ε⇑ : Γ ⊢ ⟨ ε ⟩ A → Γ ⊢ ⟨ E ⟩ A
ε⇑ M = M ＠ ⟨ ε⊆¿ ⟩ ≲-refl

infixl 5 _&_
_&_ = ≤-trans

-- Helper for Hnd-Hooks
HCl-Hooks : HCl → List 𝔼
HCl-Hooks hCnil = []
HCl-Hooks (hC (ident e) _ _ _ Ms) = e ∷ HCl-Hooks Ms

-- Get the list of effects handled by a (source) handler.
Hnd-Hooks : Hnd → List 𝔼
Hnd-Hooks (h _ _ HP) = HCl-Hooks HP

data _⊏_ (A : Typeᵛ) : Maybe Typeᵛ → Set where
  checked : A ⊏ just A
  inferred : A ⊏ nothing

infixl 5 _＠ᵛ_
_＠ᵛ_ : Γ ⊢ ⟨ E ⟩ A → A ≲ B → Γ ⊢ ⟨ E ⟩ B
M ＠ᵛ p = M ＠ ⟨ ⊆¿-refl ⟩ p

-- Cast to another type
constant : ∀ ?A {B} → Γ ⊢ ⟨ E ⟩ B → Error ⊎ (∃[ A ] A ⊏ ?A × Γ ⊢ ⟨ E ⟩ A)
constant nothing M = pure (_ , inferred , M)
constant (just A) {B} M with B ≲? A
... | yes ≲A = pure (_ , checked , M ＠ᵛ ≲A)
... | no ¬≲ = Err (TypeMismatch B A ¬≲)

_⊢⇑⟨_⟩_ : Context → Effects → Maybe Typeᵛ → Set
Γ ⊢⇑⟨ E ⟩ ?A = ∃[ A ] A ⊏ ?A × Γ ⊢ ⟨ E ⟩ A

infer : CxMap Γ → ∀ {E} ?A → Surface.Exp → Error ⊎ (Γ ⊢⇑⟨ E ⟩ ?A)
infer-handler : CxMap Γ → ∀ {E} ?B → ∀ (H : Surface.Hnd) → ∀ A → Error ⊎ (∃[ B ] B ⊏ ?B × (Γ ⊢ A / Hnd-Hooks H ==> ⟨ E ⟩ B))

infer ℾ ?A eUnit = constant ?A (ε⇑ ($ tt))
infer ℾ ?A eTrue = constant ?A (ε⇑ ($ true))
infer ℾ ?A eFalse = constant ?A (ε⇑ ($ false))
infer ℾ ?A (eVar (ident x)) with lookup ℾ x
... | just (A , Γ∋A) = constant ?A (ε⇑ (` Γ∋A))
... | nothing = Err (UnboundVariable x)
infer ℾ (just A⇒B) (eLam X A′ M) with A⇒B ≈? (★ ⇒ ⟨ ¿ ⟩ ★)
... | yes (A ⇒ ⟨ F ⟩ B , ≤A⇒B , (_ ⇒ _)) = do
  let unifyVarType : _ → Error ⊎ (∃[ A₀ ] A₀ ≤ A)
      unifyVarType = λ where
        (tAAnn A′) → do
          A′ ← parseType A′
          yes (A₀ , ≤A , _) ← pure (A ≈? A′)
            where (no ¬A≈A′) → Err (Todo "Mismatch in argument type")
          pure (A₀ , ≤A)
        tANo → pure (A , ≤-refl)
  (A₀ , ≤A) ← unifyVarType A′
  (_ , checked , M) ← infer (ℾ ▷ nothing / _ ▷ from-Pat X / A₀) {F} (just B) M
  pure (_ , checked , ε⇑ (ƛ M) ＠ᵛ ≤-≲ (≤-trans (≤A ⇒ ≤ᶜ-refl) ≤A⇒B))
... | no _ = Err (Todo "Expected: not a function ; Actual: a function")
infer ℾ nothing e@(eLam _ _ _) = Err (Todo ("Can't infer a lambda; in\n" ++ printExp e))
infer ℾ (just A⇒B) (eFix Xf X M) with A⇒B ≈? (★ ⇒ ⟨ ¿ ⟩ ★)
... | yes (A ⇒ ⟨ F ⟩ B , ≤A⇒B , (_ ⇒ _)) = do
  (_ , checked , M) ← infer (ℾ ▷ from-Pat Xf / A ⇒ ⟨ F ⟩ B ▷ from-Pat X / A) (just B) M
  pure (_ , checked , ε⇑ (ƛ M) ＠ᵛ ≤-≲ ≤A⇒B)
... | no _ = Err (Todo "Expected: not a function ; Actual: a recursive function")
infer ℾ nothing (eFix _ _ _) = Err (Todo "Can't infer a lambda-fix")
infer ℾ ?B (eLet X A M N) = do
  A ← parseType A
  (_ , checked , M) ← infer ℾ (just A) M
  (B , B⊏?B , N) ← infer (ℾ ▷ from-Pat X / A) ?B N
  pure (B , B⊏?B , Let M N)

infer ℾ (just (sum Cs As)) (eCon (constr c) M) with c ∈? Cs
... | yes c∈Cs = do
  let i = Any.index c∈Cs
      A = Vec.lookup As i
  (_ , checked , M) ← infer ℾ (just A) M
  pure (_ , checked , con i M)
... | no _ = Err (Todo "Unexpected constructor (not found in sum)")
infer ℾ (just _) (eCon _ _) = Err (Todo "Unexpected constructor (not a sum)")
infer ℾ nothing (eCon _ _) = Err (Todo "Can't infer sums")

infer ℾ (just (tycon T As)) (eWrap M) = do
  (_ , checked , M) ← infer ℾ (just _) M
  pure (_ , checked , wrap M)
infer ℾ (just _) (eWrap M) = Err (Todo "Unexpected wrap")
infer ℾ nothing (eWrap M) = Err (Todo "Can't infer wrap")

infer ℾ ?B (eLetU X M N) = do
  (A@(tycon T As) , inferred , M) ← infer ℾ nothing M
    where (_ , _ , _) → Err (Todo "Cannot unwrap a non-data type")
  (_ , ⊏?B , N) ← infer (ℾ ▷ from-Pat X / _) ?B N
  pure (_ , ⊏?B , Let (Unwrap M) N)

infer {Γ} ℾ {E} ?B (eApp N M) = do
  (A⇒B , inferred , N) ← infer ℾ {E} nothing N
  yes (A ⇒ ⟨ F ⟩ B , ≤A⇒B , (_ ⇒ _)) ← pure (A⇒B ≈? (★ ⇒ ⟨ ¿ ⟩ ★)) where
    no ¬≈ → Err (Todo "Applying not a function")
  yes F⊆¿E ← pure (F ⊆¿? E) where
    no ¬⊆¿ → Err (Todo "bad effect")
  (_ , checked , M)         ← infer ℾ (just A) M
  constant ?B (_·_ (N ＠ᵛ ≥-≲ ≤A⇒B ＠ᵛ (≲-refl ⇒ ⟨ F⊆¿E ⟩ ≲-refl)) M)

infer ℾ {E} ?A (eAnn M P) = do
  ⟨ F ⟩ B          ← parseTypeᶜ P
  (yes F⊆¿E)            ← pure (F ⊆¿? E) where
    (no _) → Err (Todo "Bad effect annotation")
  (_ , checked , M)       ← infer ℾ (just B) M
  constant ?A (M ＠ ⟨ F⊆¿E ⟩ ≲-refl)
infer ℾ ?A (eNat n) = constant ?A {B = $ ′ℕ} (ε⇑ ($ (abs n)))
infer ℾ ?A (eOp M op N) = do
  (A₁ , A₂ , A₃ , f) ← pure (mkOp op)
  (_ , checked , M)    ← infer ℾ (just ($ A₁)) M
  (_ , checked , N)    ← infer ℾ (just ($ A₂)) N
  constant ?A (_⦅_⦆_ M f N)
infer ℾ ?A (eIf M₁ M₂ M₃) = do
  (_ , checked , M₁)   ← infer ℾ (just ($ ′𝔹)) M₁
  (A , A⊏?A , M₂) ← infer ℾ ?A M₂
  (_ , checked , M₃) ← infer ℾ (just A) M₃
  pure (_ , A⊏?A , if M₁ M₂ M₃)
infer ℾ {E} ?A (ePerform (ident e) M) = do
  (_ , checked , M) ← infer ℾ (just (request e)) M
  case e ∈¿? E of λ where
    (no _) → Err (Todo "unexpected effect")
    (yes e∈¿E) → constant ?A (perform e∈¿E M)
infer ℾ {E} ?B (eHnd M H) = do
  let Hooks     = Hnd-Hooks H
  (A , inferred , M′)  ← infer ℾ {E = Hooks ++¿ E} nothing M
  (B , B⊏?B , H′) ← infer-handler ℾ ?B H A
  pure (B , B⊏?B , handle M′ by H′)
infer ℾ ?A×B (ePair M N) = do
  (?A , ?B , _?,_) ← ≈-× ?A×B
  M ← infer ℾ ?A M
  N ← infer ℾ ?B N
  pure (M ?, N)
  where
    ≈-× : ∀ ?A×B → Error ⊎ (∃[ ?A ] ∃[ ?B ] (Γ ⊢⇑⟨ E ⟩ ?A → Γ ⊢⇑⟨ E ⟩ ?B → Γ ⊢⇑⟨ E ⟩ ?A×B))
    ≈-× nothing = pure (nothing , nothing ,
      λ{ (_ , inferred , M) (_ , inferred , N) →
         (_ , inferred , M , N) })
    ≈-× (just A×B) with A×B ≈? (★ ′× ★)
    ... | yes (A ′× B , ≤A×B , _) = pure (just A , just B ,
      λ{ (_ , checked , M) (_ , checked , N) →
         (_ , checked , (M , N) ＠ᵛ ≤-≲ ≤A×B) })
    ... | no ¬≈ = Err (Todo "unexpected pair")
      
infer ℾ ?B (eLetP X₁ X₂ M N) = do
  (A , inferred , M) ← infer ℾ nothing M
  yes (A₁ ′× A₂ , ≤A , _ ′× _) ← pure (A ≈? (★ ′× ★)) where
    no ¬A≈× → Err (Todo "not a pair")
  (B , ⊏?B , N) ← infer (ℾ ▷ from-Pat X₁ / A₁ ▷ from-Pat X₂ / A₂) ?B N
  pure (B , ⊏?B , unpair (M ＠ᵛ ≥-≲ ≤A) N)

infer {Γ} ℾ {E} ?B@(just B) (eMch M Ns) = do
  (sum Cs As , inferred , M) ← infer ℾ nothing M
    where _ → Err (Todo "match on not a sum")
  Ns ← find-mchs Cs As (infer-mchs Ns)
  pure (_ , checked , Case M Ns)
  where
    Mchs = List (String × ∀ A → Error ⊎ (Γ ▷ A) ⊢⇑⟨ E ⟩ ?B)
    infer-mchs : List Mch → Mchs
    infer-mchs [] = []
    infer-mchs (mCl (constr C) X N ∷ Ns) = (C , λ A → infer (ℾ ▷ from-Pat X / A) ?B N) ∷ infer-mchs Ns

    remove : ∀ {A} → String → List (String × A) → Error ⊎ (A × List (String × A))
    remove C [] = Err (Todo ("Unhandled constructor: " ++ C))
    remove C (CN@(C′ , N) ∷ Ns) with C ≟ C′
    ... | yes _ = pure (N , Ns)
    ... | no _ = Sum.map₂ (Prod.map₂ (CN ∷_)) (remove C Ns)

    find-mchs : ∀ (Cs : List String) (As : Vec Typeᵛ (List.length Cs)) → Mchs → Error ⊎ VecAll (λ A → Γ ▷ A ⊢ ⟨ _ ⟩ B) As
    find-mchs [] [] [] = pure []
    find-mchs [] [] ((C , _) ∷ _) = Err (Todo ("Unreachable constructor: " ++ C))
    find-mchs (C ∷ Cs) (A ∷ As) Ns = do
      (N , Ns) ← remove C Ns
      (_ , checked , N) ← N A
      Ns ← find-mchs Cs As Ns
      pure (N ∷ Ns)

infer ℾ nothing (eMch _ _) = Err (Todo "can't infer match")

infer-HCl : CxMap Γ → ∀ (Ms : HCl) {E} B → Error ⊎ (Gradual.Perform-Clauses (HCl-Hooks Ms) Γ (⟨ E ⟩ B))
infer-HCl ℾ hCnil B = pure []
infer-HCl ℾ (hC (ident e) X K M Ms) B = do
  (_ , checked , M) ← infer (ℾ ▷ from-Pat X / _ ▷ from-Pat K / _) (just B) M
  Ms ← infer-HCl ℾ Ms B
  pure (M ∷ Ms)

infer-handler ℾ ?B (h X M HP) A = do
  (B , B⊏?B , H-return-clause) ← infer (ℾ ▷ from-Pat X / A) ?B M
  H-perform-clauses ← infer-HCl ℾ HP B
  pure (B , B⊏?B ,
    record
      { return-clause = H-return-clause
      ; perform-clauses = H-perform-clauses })

infer0 : Surface.Exp → Error ⊎ (∃[ A ] (∅ ⊢ ⟨ ε ⟩ A))
infer0 M = infer ∅ {ε} nothing M >>= λ { (_ , inferred , M) → pure (_ , M) }
