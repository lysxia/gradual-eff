{-# OPTIONS --overlapping-instances #-}
module CoreExamples where

open import Data.Bool using (true; false)
open import Data.Nat using (ℕ; zero; suc; _+_)
open import Data.List using (List; []; _∷_)
open import Data.List.Membership.Propositional using (_∈_)
open import Data.List.Relation.Unary.All using ([]; _∷_)
open import Data.List.Relation.Unary.Any using (here; there)
open import Data.List.Relation.Binary.Subset.Propositional using (_⊆_)
open import Data.List.Relation.Binary.Subset.Propositional.Properties as ListSubset
open import Data.Unit using (⊤; tt)
open import Relation.Nullary using (¬_; Dec; yes; no)
open import Relation.Binary.PropositionalEquality
     using (_≡_; _≢_; refl; trans; sym; cong; cong₂; cong-app; subst; inspect)
open import Core public
open import Progress public

-- * Sugar

record Prefix (Γ Δ : Context) : Set where
  field
    prefix : ∀ {A} → Γ ∋ A → Δ ∋ A

instance
  Prefix-id : ∀ {Γ} → Prefix Γ Γ
  Prefix-id = record { prefix = λ v → v }

  Prefix-▷ : ∀ {Γ Δ} → {{Prefix Γ Δ}} → ∀ {A} → Prefix Γ (Δ ▷ A)
  Prefix-▷ {{P}} = record { prefix = λ v → S (P .Prefix.prefix v) }

record Elem¿ (e : Effect) (E : Effects) : Set where
  constructor MkElem¿
  field
    elem¿ : ¡ (e ∷ []) =>! E

record Elem (e : Effect) (E : List Effect) : Set where
  constructor MkElem
  field
    elem : e ∈ E

instance
  Elem¿-¿ : ∀ {e} → Elem¿ e ¿
  Elem¿-¿ = MkElem¿ upcast!

  Elem¿-¡ : ∀ {e E} → {{Elem e E}} → Elem¿ e (¡ E)
  Elem¿-¡ {{e∈E}} = MkElem¿ (subtype! λ { (here refl) → Elem.elem e∈E })

  Elem-here : ∀ {e E} → Elem e (e ∷ E)
  Elem-here = MkElem (here refl)

  Elem-there : ∀ {e e′ E} → {{Elem e E}} → Elem e (e′ ∷ E)
  Elem-there {{e=>!E}} = MkElem (there (Elem.elem e=>!E))

infixl 4 _`>>=_
infix  6 _`⦅_⦆_
infix  8 ``_
infixr 0 ƛ-hoas
infixr 0 bind-hoas
infixr 0 bind2-hoas

_`>>=_ : ∀ {Γ E A B} → Γ ⊢ᶜ ⟨ E ⟩ A → Γ ▷ A ⊢ᶜ ⟨ E ⟩ B → Γ ⊢ᶜ ⟨ E ⟩ B
return N `>>= M = M [ N ]
N `>>= M = N >>= M

`perform : ∀ e {Γ E} → {{Elem¿ e E}} → Γ ⊢ᶜ ⟨ E ⟩ request e → Γ ⊢ᶜ ⟨ E ⟩ response e
`perform e {{e=>!E}} M = M `>>= (perform e (` Z) !＠ Elem¿.elem¿ e=>!E)

``_ : ∀ {Γ Δ} → {{Prefix Γ Δ}} → ∀ {E A} → Γ ∋ A → Δ ⊢ᶜ ⟨ E ⟩ A
``_ {{P}} v = return (` (P .Prefix.prefix v))

`$_ : ∀ {Γ E ι} → rep ι → Γ ⊢ᶜ ⟨ E ⟩ $ ι
`$ k = return ($ k)

_`⦅_⦆_ : ∀ {Γ E ι ι′ ι′′} → Γ ⊢ᶜ ⟨ E ⟩ $ ι → (rep ι → rep ι′ → rep ι′′) → Γ ⊢ᶜ ⟨ E ⟩ $ ι′ → Γ ⊢ᶜ ⟨ E ⟩ $ ι′′
M `⦅ f ⦆ N = M `>>= (liftᶜ N `>>= (` S Z ⦅ f ⦆ ` Z) !＠ ε=>!)

bind-hoas : ∀ {Γ A P} → (Γ ▷ A ∋ A → Γ ▷ A ⊢ᶜ P) → Γ ▷ A ⊢ᶜ P
bind-hoas f = f Z

bind2-hoas : ∀ {Γ A A′ P} → (Γ ▷ A ▷ A′ ∋ A → Γ ▷ A ▷ A′ ⊢ᶜ P) → Γ ▷ A ▷ A′ ⊢ᶜ P
bind2-hoas f = f (S Z)

ƛ-hoas : ∀ {Γ A E P} → (Γ ▷ A ∋ A → Γ ▷ A ⊢ᶜ P) → Γ ⊢ᶜ ⟨ E ⟩ A ⇒ P
ƛ-hoas f = return (ƛ (f Z))

syntax bind-hoas (λ v → M) = `bind v `→ M
syntax bind2-hoas (λ v → M) = `bind2 v `→ M
syntax ƛ-hoas (λ v → M) = `λ v `→ M

_`,_ : ∀ {Γ E A B} → Γ ⊢ᶜ ⟨ E ⟩ A → Γ ⊢ᶜ ⟨ E ⟩ B → Γ ⊢ᶜ ⟨ E ⟩ (A ′× B)
M `, N = M `>>= (liftᶜ N `>>= return ((` S Z) , (` Z)))

_`＠_ : ∀ {Γ E A B} → Γ ⊢ᶜ ⟨ E ⟩ A → (A => B) → Γ ⊢ᶜ ⟨ E ⟩ B
M `＠ p = M `>>= ((` Z) ＠ p) !＠ ε=>!

dummy : ∀ {Γ} → Γ ⊢ᵛ ★
dummy = $ tt ⇑ $𝕌

`dummy : ∀ {Γ E} → Γ ⊢ᶜ ⟨ E ⟩ ★
`dummy = return dummy !＠ ε=>!

`tt : ∀ {Γ E} → Γ ⊢ᶜ ⟨ E ⟩ $𝕌
`tt = return ($ tt)

-- * Basic examples

ex0 : Trace
ex0 = 100 `eval` $ 1 ⦅ _+_ ⦆ $ 1

inc : ∀ {Γ} → Γ ⊢ᵛ $ℕ ⇒ ⟨ ε ⟩ $ℕ
inc =  ƛ (` Z ⦅ _+_ ⦆ $ 1)

inc' : ∀ {Γ} → Γ ⊢ᶜ ⟨ ε ⟩ ★
inc' = inc ＠ (+ ℕ≤★ ⇒ ⟨ ⇑¿ _ ⟩ ℕ≤★ ⇑ ★⇒¿★)

⇑★_ : ∀ {Γ A} → Γ ⊢ᵛ A → Γ ⊢ᶜ ⟨ ε ⟩ ★
⇑★ V = V ＠ (+ A≤★)

-- Coerce a ★ to a ★⇒★
funny : ∀ {Γ} → Γ ⊢ᶜ ⟨ ε ⟩ ★ → Γ ⊢ᶜ ⟨ ε ⟩ ★ ⇒ ⟨ ε ⟩ ★
funny M = M >>= (` Z) ＠ (- id ⇒ ⟨ ⇑¿ _ ⟩ id ⇑ ★⇒¿★)

infixl 3 _`·_

_`·_ : ∀ {Γ E A B} → Γ ⊢ᶜ ⟨ E ⟩ A ⇒ ⟨ E ⟩ B → Γ ⊢ᶜ ⟨ E ⟩ A → Γ ⊢ᶜ ⟨ E ⟩ B
M `· N = M `>>= (liftᶜ N `>>= (` S Z) · (` Z))

ex1 : Trace
ex1 = 100 `eval` (funny inc' `· (⇑★ ($ 0)))


inc₂ : ∀ {Γ} → Γ ⊢ᶜ ⟨ ε ⟩ $ℕ ⇒ ⟨ ε ⟩ $ℕ
inc₂ = `λ x `→ (`` x) `⦅ _+_ ⦆ `$ 1

state-handler : ∀ {Γ A} → Γ ⊢ʰ ⟨ ¿ ⟩ A ==> ⟨ ¿ ⟩ ($ℕ ⇒ ⟨ ¿ ⟩ A)
state-handler = record
  { Hooks = "get" ∷ "set" ∷ []
  ; Hooks-contained = ¿-⊆¿
  ; perform-clauses =
    (`bind2 _ `→ `bind k `→ `λ s `→ `` k `· `` s `· `` s) ∷
    (`bind2 s `→ `bind k `→ `λ _ `→ `` k `· `tt `· `` s) ∷ []
  ; return-clause = `bind v `→ `λ _ `→ `` v }

-- You have to eta-expand to delay catching the first effect
run-state : ∀ {Γ A E} → Γ ⊢ᶜ ⟨ ¿ ⟩ A -> Γ ⊢ᶜ ⟨ E ⟩ $ℕ ⇒ ⟨ ¿ ⟩ A
run-state M = `λ s `→ liftᶜ (handle M by state-handler) `· `` s

sinc : ∀ {Γ} → Γ ⊢ᶜ ⟨ ¿ ⟩ $ℕ
sinc = `perform "get" `tt >>= (`bind s `→
  `perform "set" (`` s `⦅ _+_ ⦆ `$ 1) >>=
  `perform "get" `tt >>= (`bind s₂ `→
  `` s₂))

run-sinc : ∅ ⊢ᶜ ⟨ ε ⟩ $ℕ
run-sinc = (run-state sinc `· `$ 0) !＠ downcast! []

last : Trace → Trace
last t@(_ ⟼ END) = t
last (_ ⟼ t) = last t
last END = END

ex2 : Trace
ex2 = last (100 `eval` run-sinc)

infixr 7 _⇒ε_
infixr 7 _⇒¿_

_⇒ε_ : (A B : Typeᵛ) → Typeᵛ
_⇒ε_ A B = A ⇒ ⟨ ε ⟩ B

_⇒¿_ : (A B : Typeᵛ) → Typeᵛ
_⇒¿_ A B = A ⇒ ⟨ ¿ ⟩ B

-- Church encoding
-- list a = ∀ {r : Type} → r → (a → r → r) → r
`list : Typeᵛ
`list = (★ ⇒ε (★ ⇒ε ★ ⇒ε ★) ⇒ε ★)

-- nil = λ n _ → n
nil : ∀ {Γ} → Γ ⊢ᵛ `list
nil = ƛ return (ƛ return (` S Z))

`nil : ∀ {Γ E} → Γ ⊢ᶜ ⟨ E ⟩ `list
`nil = return nil

-- cons x xs = λ n c → c x (xs n c)
cons : ∀ {Γ E A} → Γ ⊢ᵛ A → Γ ⊢ᵛ `list → Γ ⊢ᶜ ⟨ E ⟩ `list
cons x xs = return (ƛ return (ƛ ((return (` Z) `· (liftᵛ (liftᵛ x) ＠ (+ A≤★)) `· (liftᵛ (liftᵛ xs) · (` S Z) `· return (` Z))))))
--                  λ n       λ c →         c                   x                 (             xs         n               c)

`cons : ∀ {Γ E A} → Γ ⊢ᶜ ⟨ E ⟩ A → Γ ⊢ᶜ ⟨ E ⟩ `list → Γ ⊢ᶜ ⟨ E ⟩ `list
`cons h t = h `>>= (liftᶜ t `>>= cons (` S Z) (` Z))

-- app xs₁ xs₂ = λ n c → xs₁ (xs₂ n c) c
app : ∀ {Γ E} → (xs₁ xs₂ : Γ ⊢ᵛ `list) → Γ ⊢ᶜ ⟨ E ⟩ `list
app xs₁ xs₂ = return (ƛ return (ƛ (return (liftᵛ (liftᵛ xs₁)) `· (liftᵛ (liftᵛ xs₂) · (` S Z) `· return (` Z)) `· return (` Z))))

`app : ∀ {Γ E} → (xs₁ xs₂ : Γ ⊢ᶜ ⟨ E ⟩ `list) → Γ ⊢ᶜ ⟨ E ⟩ `list
`app xs₁ xs₂ = xs₁ `>>= (liftᶜ xs₂ `>>= app (` S Z) (` Z))

`choose : ∀ {Γ E A} → {{ Elem¿ "choose" E }} → (M N : Γ ⊢ᶜ ⟨ E ⟩ A) → Γ ⊢ᶜ ⟨ E ⟩ A
`choose M N = `perform "choose" `tt `>>= if (` Z) (liftᶜ M) (liftᶜ N)

`fail : ∀ {Γ E A} → {{ Elem¿ "fail" E }} → Γ ⊢ᶜ ⟨ E ⟩ A
`fail = `perform "fail" `tt `>>= absurd (` Z)

-- choose () k = k true ++ k false
-- fail () _ = nil
nondet-handler : ∀ {Γ E F A} → (E - ("choose" ∷ "fail" ∷ []) ⊆¿ F) → Γ ⊢ʰ ⟨ E ⟩ A ==> ⟨ F ⟩ `list
nondet-handler Hooks-contained = record
  { Hooks = "choose" ∷ "fail" ∷ []
  ; Hooks-contained = Hooks-contained
  ; perform-clauses =
      (`bind2 _ `→ `bind k `→ `app (` k · $ true) (` k · $ false)) ∷
      (`bind2 _ `→ `bind _ `→ `nil) ∷ []
  ; return-clause = `bind v `→ cons (` v) nil }

drunk-coinflip : ∅ ⊢ᶜ ⟨ ¡ ("choose" ∷ "fail" ∷ []) ⟩ $𝔹
drunk-coinflip =
  `choose `fail  -- The coin falls in the gutter, or...
    (`choose (return ($ true))    -- we get heads or tails.
             (return ($ false)))

record _~~_ (A B : Typeᵛ) : Set where
  field
    C : Typeᵛ
    up : A ≤ C
    down : B ≤ C

instance
  ⇒~~⇒ : ∀ {A P A′ P′}
    → {{ A ~~ A′ }}
    → {{ P .effects ≡ P′ .effects }}
    → {{ P .returns ~~ P′ .returns }}
    → (A ⇒ P) ~~ (A′ ⇒ P′)
  ⇒~~⇒ {{A~~}} {{refl}} {{B~~}} = record
    { up = up A~~ ⇒ ⟨ refl ⟩ up B~~
    ; down = down A~~ ⇒ ⟨ refl ⟩ down B~~ }
    where open _~~_

  ★~~ : ∀ {A} → ★ ~~ A
  ★~~ = record
    { up = id
    ; down = A≤★ }
    where open _~~_

  ~~★ : ∀ {A} → A ~~ ★
  ~~★ = record { up = A≤★ ; down = id }
    where open _~~_

cast : ∀ {Γ E A B} → {{ A ~~ B }} → Γ ⊢ᵛ A → Γ ⊢ᶜ ⟨ E ⟩ B
cast {{c}} V = ((V ＠ (+ up)) `＠ (- down)) !＠ ε=>!
  where
    open _~~_ c using (up; down)

-- TODO: cast effects
`cast : ∀ {Γ E A B} → {{ A ~~ B }} → Γ ⊢ᶜ ⟨ E ⟩ A → Γ ⊢ᶜ ⟨ E ⟩ B
`cast M = M `>>= cast (` Z)

length : ∀ {Γ E} → Γ ⊢ᵛ `list → Γ ⊢ᶜ ⟨ E ⟩ $ℕ
length M =  cast {B = $ℕ ⇒ε (★ ⇒ε $ℕ ⇒ε $ℕ) ⇒ε $ℕ} M `>>= (((` Z) · $ 0) !＠ ε=>!) `>>= ((` Z) · (ƛ return inc) !＠ ε=>!)

`length : ∀ {Γ E} → Γ ⊢ᶜ ⟨ E ⟩ `list → Γ ⊢ᶜ ⟨ E ⟩ $ℕ
`length M = M `>>= length (` Z)

h-all : ∀ {E F} → (¡ E) - E ⊆¿ (¡ F)
h-all = ¡-⊆¿ (ListSubset.xs⊆xs++ys _ _)

ex3-term : ∅ ⊢ᶜ ⟨ ε ⟩ $ℕ
ex3-term = `length (handle drunk-coinflip by nondet-handler h-all)

ex3 : Trace
ex3 = last (1000 `eval` ex3-term)
