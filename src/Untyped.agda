open import Type.Constants
module Untyped (ℂ : Constants) where

open Constants ℂ

open import Type ℂ
open import Surface.AST using (Op)

open import Data.String.Base as String using (String)
open import Data.List.Base as List using (List; []; _∷_; _++_)
open import Data.Maybe.Base using (Maybe; nothing; just)
open import Data.Product using (_×_; _,_; proj₁; proj₂; Σ; ∃; Σ-syntax; ∃-syntax)

Name : Set
Name = String

ConName : Set
ConName = String

Pat : Set
Pat = Maybe Name

data Term : Set
record Handler : Set

data Term where
  Var : Name → Term
  Let : Pat → Term → Term → Term    -- let X = M in N
  $_ : ∀ {ι} → rep ι → Term         -- bool, nat, unit
  App : Term → Term → Term          -- N M
  Annᵉ : Term → Effects → Term      -- M : ⟨ E ⟩
  Annᵛ : Term → Typeᵛ → Term        -- M : A
  Lam : Pat → Typeᵛ → Term → Term   -- λ X → M
  Rec : Pat → Pat → Typeᵛ → Typeᶜ → Term → Term  -- λfix (Xf : A ⇒ B) X → M  (recursive functions)
  Han : Term → Handler → Term       -- handle M with H
  Prf : 𝔼 → Term → Term             -- perform e M
  _⦅_⦆_ : Term → Op → Term → Term
  Con : ConName → Term → Term
  Match : Term → List (ConName × Pat × Term) → Term
  Wrap : Term → Term
  LetUnwrap : Pat → Term → Term → Term
  Pair : Term → Term → Term
  LetPair : Pat → Pat → Term → Term → Term
  If : Term → Term → Term → Term

record Handler where
  inductive
  field
    return-clause : Pat × Term                     -- return X ⇒ M
    perform-clauses : List (𝔼 × Pat × Pat × Term)  -- perform e X K ⇒ M

open Handler
