open import Type.Constants
open import SurfaceConstants using (PreConstants; fromPreConstants; Error)

module Surface2Untyped (ℂ₀ : PreConstants) where

ℂ : Constants
ℂ = fromPreConstants ℂ₀

open Constants ℂ

open import Type ℂ
open import Untyped ℂ as Untyped
open import Surface.AST as Surface

import Level
open import Effect.Monad using (RawMonad)
open import Data.String.Base as String using (String)
open import Data.List.Base as List using (List; []; _∷_; _++_)
open import Data.Product using (_×_; _,_; proj₁; proj₂; Σ; ∃; Σ-syntax; ∃-syntax)
open import Data.Integer.Base using () renaming (∣_∣ to abs)
open import Data.Vec.Base using ([])
open import Data.Unit.Base using (tt)
open import Data.Maybe.Base using (Maybe; nothing; just)
open import Data.Bool.Base using (true; false)
open import Data.Sum.Base using (_⊎_; map₂)
open import Data.Sum.Effectful.Left as Either using ()

open RawMonad (Either.monad Error Level.zero)

parseTypeᶜ : Surface.CType → Error ⊎ Typeᶜ
parseTypeᶜ = SurfaceConstants.ParseType.parseTypeᶜ (PreConstants.preraw ℂ₀) []

parseType : Surface.Type → Error ⊎ Typeᵛ
parseType = SurfaceConstants.ParseType.parseType (PreConstants.preraw ℂ₀) []

to-Pat : Surface.Pat → Untyped.Pat
to-Pat (pId (ident i)) = just i
to-Pat pWild = nothing

ann : Term → Typeᶜ → Term
ann M (⟨ E ⟩ A) = Annᵛ (Annᵉ M E) A

from-TyAnn : Surface.TyAnn → Error ⊎ Typeᵛ
from-TyAnn (tAAnn A) = parseType A
from-TyAnn tANo = pure ★

to-Term : Surface.Exp → Error ⊎ Untyped.Term
to-Handler : Surface.Hnd → Error ⊎ Untyped.Handler

to-Term eUnit = pure ($ tt)
to-Term eTrue = pure ($ true)
to-Term eFalse = pure ($ false)
to-Term (eLam X A M) = zipWith (Lam (to-Pat X)) (from-TyAnn A) (to-Term M)
to-Term (eFix Xf X M) = Rec (to-Pat Xf) (to-Pat X) ★ (⟨ ¿ ⟩ ★) <$> to-Term M
to-Term (eIf M₁ M₂ M₃) = If <$> to-Term M₁ <*> to-Term M₂ <*> to-Term M₃
  where
    infixl 4 _<*>_
    _<*>_ : ∀ {A B} → Error ⊎ (A → B) → Error ⊎ A → Error ⊎ B
    _<*>_ = zipWith (λ f → f)
to-Term (eHnd M H) = zipWith Han (to-Term M) (to-Handler H)
to-Term (eMch M Ns) = zipWith Match (to-Term M) (to-Clauses Ns)
  where
    to-Clauses : List Surface.Mch → Error ⊎ List (ConName × Untyped.Pat × Untyped.Term)
    to-Clauses [] = pure []
    to-Clauses (mCl (constr c) X N ∷ Ns) =
      zipWith (λ N Ns → (c , to-Pat X , N) ∷ Ns) (to-Term N) (to-Clauses Ns)
to-Term (eNat n) = pure ($ (abs n))
to-Term (eLet X A M N) = zipWith (Let (to-Pat X)) (zipWith Annᵛ (to-Term M) (parseType A)) (to-Term N)
to-Term (ePerform (ident e) M) = map₂ (Prf e) (to-Term M)
to-Term (eApp N M) = zipWith App (to-Term N) (to-Term M)
to-Term (eCon (constr c) M) = Con c <$> to-Term M
to-Term (eOp M o N) = zipWith _⦅ o ⦆_ (to-Term M) (to-Term N)
to-Term (eVar (ident X)) = pure (Var X)
to-Term (eWrap M) = Wrap <$> to-Term M
to-Term (eLetU X M N) = zipWith (LetUnwrap (to-Pat X)) (to-Term M) (to-Term N)
to-Term (ePair M N) = zipWith Pair (to-Term M) (to-Term N)
to-Term (eLetP X₁ X₂ M N) = zipWith (LetPair (to-Pat X₁) (to-Pat X₂)) (to-Term M) (to-Term N)
to-Term (eAnn M A) = zipWith ann (to-Term M) (parseTypeᶜ A)

to-Handler _ = ?
