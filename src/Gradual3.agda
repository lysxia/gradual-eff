open import Type.Constants
module Gradual3 (ℂ : Constants) where

open Constants ℂ

open import Type ℂ
import Type.Context
open import Type.Precision ℂ
open import Type.ConsistentSubtyping ℂ
open import Untyped ℂ
import Surface.AST as Surface

open import Function.Base using (_∘_; case_of_)
open import Data.String.Base as String using (String)
open import Data.List.Base as List using (List; []; _∷_; _++_)
open import Data.List.Relation.Unary.All using (All; []; _∷_)
open import Data.Maybe.Base using (Maybe; nothing; just)
open import Data.Empty using (⊥)
open import Data.Product using (_×_; _,_; proj₁; proj₂; Σ; ∃; Σ-syntax; ∃-syntax)
open import Relation.Binary.PropositionalEquality using (_≡_; _≢_; refl)
open import Data.Vec.Base using (Vec; []; _∷_)

import Data.Nat.Base as Nat
import Data.Bool.Base as Bool

module _ where
  open Surface
  mkOp : Surface.Op → ∃[ κ₁ ] ∃[ κ₂ ] ∃[ κ₃ ] (rep κ₁ → rep κ₂ → rep κ₃)
  mkOp opAdd = (′ℕ , ′ℕ , ′ℕ , Nat._+_)
  mkOp opSub = (′ℕ , ′ℕ , ′ℕ , Nat._∸_)
  mkOp opMul = (′ℕ , ′ℕ , ′ℕ , Nat._*_)
  mkOp opEq =  (′ℕ , ′ℕ , ′𝔹 , Nat._≡ᵇ_)
  mkOp opNEq = (′ℕ , ′ℕ , ′𝔹 , λ x y → Bool.not (x Nat.≡ᵇ y))
  mkOp opAnd = (′𝔹 , ′𝔹 , ′𝔹 , Bool._∧_)
  mkOp opOr  = (′𝔹 , ′𝔹 , ′𝔹 , Bool._∨_)

infixl 2 _▷_⦂_

data Context : Set where
  ∅ : Context
  _▷_⦂_ : Context → Pat → Typeᵛ → Context

private
  variable
    Γ : Context
    A B : Typeᵛ
    P Q : Typeᶜ
    E Eh F : Effects
    X Y : Name
    ?X ?Xf ?Y : Pat
    M N : Term
    H : Handler
    e : 𝔼
    ι ι′ ι″ : Base

data _≢ᵖ_ (X : Name) : Pat -> Set where
  just : X ≢ᵖ just X
  nothing : X ≢ᵖ nothing

infixl 2 _∋_⦂_

data _∋_⦂_ : Context → Name → Typeᵛ → Set where
  Z :
    Γ ▷ just X ⦂ A ∋ X ⦂ A
  S_⦂_ :
    X ≢ᵖ ?Y →
    Γ         ∋ X ⦂ B →
    -----------------
    Γ ▷ ?Y ⦂ A ∋ X ⦂ B

Hooks : Handler → List 𝔼
Hooks H = List.map proj₁ (Handler.perform-clauses H)

infixl 2 _⊢_⦂_ _⊢ʰ_⦂_⟹_

data _⊢_⦂_ (Γ : Context) : Term → Typeᶜ → Set
data _⊢ʰ_⦂_⟹_ (Γ : Context) : Handler → Typeᵛ → Typeᶜ → Set

data _⊢_⦂_ Γ where
  Var :
    Γ ∋ X ⦂ A →
    -------------------
    Γ ⊢ Var X ⦂ ⟨ ε ⟩ A

  App :
    Γ ⊢ N ⦂ ⟨ E ⟩ (A ⇒ ⟨ F ⟩ B) →
    F ⊆¿ E →
    Γ ⊢ M ⦂ ⟨ E ⟩ A →
    ---------------------
    Γ ⊢ App N M ⦂ ⟨ E ⟩ B

  Prf :
    e ∈¿ E →
    Γ ⊢ M ⦂ ⟨ E ⟩ request e →
    ------------------------------
    Γ ⊢ Prf e M ⦂ ⟨ E ⟩ response e

  $_ : ∀ (κ : rep ι) →
    -------------------
    Γ ⊢ $ κ ⦂ ⟨ ε ⟩ $ ι

  Lam :
    Γ ▷ ?X ⦂ A ⊢ M ⦂ P →
    ----------------------------
    Γ ⊢ Lam ?X A M ⦂ ⟨ ε ⟩ (A ⇒ P)

  Rec :
    Γ ▷ ?Xf ⦂ A ⇒ P ▷ ?X ⦂ A ⊢ M ⦂ P →
    ----------------------------
    Γ ⊢ Rec ?Xf ?X A P M ⦂ ⟨ ε ⟩ (A ⇒ P)

  Let :
    Γ         ⊢ M ⦂ ⟨ E ⟩ A →
    Γ ▷ ?X ⦂ A ⊢ N ⦂ ⟨ E ⟩ B →
    -------------------------
    Γ ⊢ Let ?X M N ⦂ ⟨ E ⟩ B

  Han :
    (let Eh = Hooks H) →
    Γ ⊢ M ⦂ ⟨ Eh ++¿ E ⟩ A →
    Γ ⊢ʰ H ⦂ A ⟹ ⟨ E ⟩ B →
    ----------------------
    Γ ⊢ Han M H ⦂ ⟨ E ⟩ B

  Bop : ∀ {o f} →
    mkOp o ≡ (ι , ι′ , ι″ , f) →
    Γ ⊢ M ⦂ ⟨ E ⟩ $ ι →
    Γ ⊢ N ⦂ ⟨ E ⟩ $ ι′ →
    --------------------
    Γ ⊢ M ⦅ o ⦆ N ⦂ ⟨ E ⟩ $ ι″

  Con : ∀ {C} →
    Γ ⊢ M ⦂ ⟨ E ⟩ A →
    -----------------------
    Γ ⊢ Con C M ⦂ ⟨ E ⟩ sum (C ∷ []) (A ∷ [])

  Match : ∀ {Cs As Ns} →
    Γ ⊢ M ⦂ ⟨ E ⟩ sum Cs As →
    Cs ⊆ List.map proj₁ Ns →
    All (λ{ (C , ?X , A , N) → Γ ▷ ?X ⦂ A ⊢ N ⦂ ⟨ E ⟩ B }) Ns →
    -----------------------
    Γ ⊢ Match M Ns ⦂ ⟨ E ⟩ B

  -- Annotations are where a type can become less precise.
  -- They can technically be encoded as an application of an identity function
  -- ((λ x : P → P) M).
  --
  -- We separate effect annotations from annotations on values:
  -- if a users only specifies an effect, we don't want to set the value type to ★.
  Annᵛ :
    Γ ⊢ M ⦂ ⟨ E ⟩ B →
    B ≤ A →
    ----------------------
    Γ ⊢ Annᵛ M A ⦂ ⟨ E ⟩ A

  Annᵉ :
    Γ ⊢ M ⦂ ⟨ F ⟩ A →
    F ≤ᵉ E →
    ----------------------
    Γ ⊢ Annᵉ M E ⦂ ⟨ E ⟩ A

  _⇑ :
    Γ ⊢ M ⦂ Q →
    P ≤ᶜ Q →
    ---------
    Γ ⊢ M ⦂ P

PerformClause : ∀ (Γ : Context) (P : Typeᶜ) → 𝔼 × Pat × Pat × Term -> Set
PerformClause Γ P = (λ{ (e , ?X , ?K , M) → (Γ ▷ ?X ⦂ request e ▷ ?K ⦂ response e ⇒ P) ⊢ M ⦂ P})

data _⊢ʰ_⦂_⟹_ Γ where
  MkHandler :
    (let record { return-clause = (X , M) ; perform-clauses = Ms } = H) →
    Γ ▷ X ⦂ A ⊢ M ⦂ P →
    All (PerformClause Γ P) Ms →
    ---------------------------------
    Γ ⊢ʰ H ⦂ A ⟹ P
