open import Type.Constants

module Type (ℂ : Constants) where

open Constants ℂ

open import Type.Base public
open import Type.Raw (Constants.raw ℂ) as Raw public
  hiding (Typeᵛ; Typeᶜ)

Typeᵛ Typeᶜ : Set
Typeᵛ = Raw.Typeᵛ ∅
Typeᶜ = Raw.Typeᶜ ∅

request response : 𝔼 → Typeᵛ
request e = 𝔼-signature.request (𝔼-sig e)
response e = 𝔼-signature.response (𝔼-sig e)

