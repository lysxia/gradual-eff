module SurfaceConstants where

open import Type.Base
open import Type.Constants
open import Type.DefaultConstants using (𝔼-String-sig)
import Type.Raw
open import Surface.AST as Surface
open import Utils using (_≟_)

import Level
open import Effect.Monad
open import Function.Base using (_∘_; case_of_)
open import Data.Sum.Base as Sum using (_⊎_; inj₁; inj₂)
open import Data.Sum.Effectful.Left as Either using () renaming (Sumₗ to Either)
open import Data.String as String using (String; _++_)
open import Data.Nat.Base using (ℕ; suc)
open import Data.List.Base as List using (List; []; _∷_; length)
open import Data.Fin.Base as Fin using (Fin; zero; suc)
import Data.Fin.Properties as Fin
open import Data.Vec.Base as Vec using (Vec; []; _∷_)
open import Data.Vec.Relation.Unary.All as VecAll using ([]; _∷_) renaming (All to VecAll)
import Data.Vec.Relation.Unary.Any as Vec using (index)
import Data.Vec.Membership.DecPropositional as VecDecMembership
import Data.Vec.Effectful as Vec
open import Data.Product using (_×_; _,_; proj₁; proj₂; Σ; ∃; Σ-syntax; ∃-syntax)
open import Data.Unit.Base using (tt)
open import Relation.Nullary using (¬_; Dec; yes; no)
open import Relation.Binary.PropositionalEquality using (_≡_; _≢_; refl; subst; sym)

open import Data.Nat.Show as Nat

data Error : Set where
  UnknownType : String → Error
  BadArity : String → (expected actual : ℕ) → Error

showError : Error → String
showError (UnknownType T) = "Unkown type " ++ T
showError (BadArity T n m) = "Type " ++ T ++ " has arity " ++ Nat.show n ++ " but is applied to " ++ Nat.show m ++ " arguments"

index : List String → Set
index Ts = Fin (length Ts)

record PreRawConstants : Set where
  field 𝕋-num : ℕ
  𝕋 = Fin 𝕋-num
  field
    𝕋-names : Vec String 𝕋-num
    arities : Vec ℕ 𝕋-num
  arity = Vec.lookup arities
  raw = record { 𝔼 = String ; 𝕋 = Fin 𝕋-num ; arity = arity }

record PreConstants : Set where
  field preraw : PreRawConstants
  open PreRawConstants preraw
  open Type.Raw raw using (Typeᵛ)
  field
    tyDefs : VecAll Typeᵛ arities

fromPreConstants : PreConstants → Constants
fromPreConstants pre = record
  { 𝔼-sig = 𝔼-String-sig
  ; tyDef = λ i → VecAll.lookup i tyDefs
  }
  where
    open PreConstants pre

tyArgs-List : TyParams → List String
tyArgs-List tPnone = []
tyArgs-List (tPone (tP (ident a))) = a ∷ []
tyArgs-List (tPmore (tP (ident a)) xs) = List.reverse (a ∷ List.map (λ{ (tP (ident b)) → b }) xs)

length-tyArgs : TyParams → ℕ
length-tyArgs = length ∘ tyArgs-List

open RawMonad (Either.monad Error Level.zero)

Vec-traverse : ∀ {n} {A B : Set} → (A → Error ⊎ B) → Vec A n → Error ⊎ Vec B n
Vec-traverse f [] = pure []
Vec-traverse f (x ∷ xs) = do y ← f x ; ys ← Vec-traverse f xs ; pure (y ∷ ys)

List-traverse : ∀ {A B : Set} → (A → Error ⊎ B) → List A → Error ⊎ List B
List-traverse f [] = pure []
List-traverse f (x ∷ xs) = do y ← f x ; ys ← List-traverse f xs ; pure (y ∷ ys)

module ParseType (preraw : PreRawConstants) where
  open PreRawConstants preraw
  open Type.Raw raw

  parseEff : Surface.Eff → Error ⊎ Effects
  parseEff effDyn = pure ¿
  parseEff effDef = pure (¡ [])
  parseEff (effSta E) = pure (¡ (parseEffList E))
    where
      parseEffList : EffList → List String
      parseEffList effNil = []
      parseEffList (effSing (ident e)) = e ∷ []
      parseEffList (effCons (ident e) E) = e ∷ parseEffList E

  parseType : ∀ {Γ} → Vec String Γ → Surface.Type → Error ⊎ Typeᵛ Γ
  parseTypeᶜ : ∀ {Γ} → Vec String Γ → Surface.CType → Error ⊎ Typeᶜ Γ
  parseTyApp : ∀ {Γ n} → Vec String Γ → String → Vec (Typeᵛ Γ) n → Error ⊎ Typeᵛ Γ
  parseTypes : ∀ {Γ} → Vec String Γ → (As : List Surface.Type) → Error ⊎ Vec (Typeᵛ Γ) (length As)

  parseType ℾ tAny = pure ★
  parseType ℾ (tArr A P) = do
    A ← parseType ℾ A
    P ← parseTypeᶜ ℾ P
    pure (A ⇒  P)
  parseType ℾ (tProd A B) = do
    A ← parseType ℾ A
    B ← parseType ℾ B
    pure (A ′× B)
  parseType ℾ (tVar (ident "nat")) = pure ($ ′ℕ)
  parseType ℾ (tVar (ident "bool")) = pure ($ ′𝔹)
  parseType ℾ (tVar (ident "unit")) = pure ($ ′𝕌)
  parseType ℾ (tVar (ident a)) =
    case a Vec-∈? ℾ of λ where
      (yes a∈) → pure (` Vec.index a∈)
      (no _) → parseTyApp ℾ a []
    where
      _Vec-∈?_ = VecDecMembership._∈?_ _≟_
  parseType ℾ (tApp (ident a) A As) = do
    A ← parseType ℾ A
    As ← parseTypes ℾ As
    parseTyApp ℾ a (A ∷ As)
  parseType ℾ (tSum CAs) = do
    (Cs , As) ← parseTyAlts CAs
    pure (sum Cs As)
    where
      parseTyAlts : List TyAlt → Error ⊎ ∃[ Cs ] Vec (Typeᵛ _) (List.length Cs)
      parseTyAlts [] = pure ([] , [])
      parseTyAlts (tAlt (constr C) A ∷ As) = do
        A ← parseType ℾ A
        Cs , As ← parseTyAlts As
        pure (C ∷ Cs , A ∷ As)

  parseTypes ℾ [] = pure []
  parseTypes ℾ (A ∷ As) = do
    A ← parseType ℾ A
    As ← parseTypes ℾ As
    pure (A ∷ As)

  parseTyApp {n = n} ℾ a As with Fin.any? (λ i → a ≟ Vec.lookup 𝕋-names i)
  ... | yes (i , _) = case arity i ≟ n of λ where
          (yes refl) → pure (tycon i As)
          (no _) → inj₁ (BadArity a (arity i) n)
  ... | no _ = inj₁ (UnknownType a)

  parseTypeᶜ ℾ (cTyp E A) = do
    E ← parseEff E
    A ← parseType ℾ A
    pure (⟨ E ⟩ A)

private module ParsePreConstants (Ds : List TyDecl) where
  arity-TyDecl : TyDecl → ℕ
  arity-TyDecl (tD _ ta _) = length-tyArgs ta

  arities-TyDecls = Vec.map arity-TyDecl ∘ Vec.fromList

  preraw : PreRawConstants
  preraw = record
    { 𝕋-num = length Ds
    ; 𝕋-names = Vec.map (λ { (tD (ident name) _ _) → name }) (Vec.fromList Ds)
    ; arities = arities-TyDecls Ds }

  open PreRawConstants preraw

  open ParseType preraw
  open import Type.Raw raw

  private parseTyDef : (D : TyDecl) → Error ⊎ Typeᵛ (arity-TyDecl D)
  parseTyDef (tD _ TyParams A) = parseType (Vec.fromList (tyArgs-List TyParams)) A

  parseTyDefs : Error ⊎ VecAll Typeᵛ arities
  parseTyDefs = itraverse parseTyDef Ds
    where
      itraverse :
          (f : (D : TyDecl) → Error ⊎ Typeᵛ (arity-TyDecl D))
        → (Ds : List TyDecl) → Error ⊎ VecAll Typeᵛ (arities-TyDecls Ds)
      itraverse f [] = pure []
      itraverse f (D ∷ Ds) = do
        D  ← f D
        Ds ← itraverse f Ds
        pure (D ∷ Ds)

parsePreConstants : OTyDecls → Error ⊎ PreConstants
parsePreConstants oTnone = inj₂ (record
  { preraw = record { 𝕋-num = 0 ; 𝕋-names = [] ; arities = [] }
  ; tyDefs = [] })
parsePreConstants (oTsome Ds) = do
  tyDefs ← ParsePreConstants.parseTyDefs Ds
  inj₂ (record { tyDefs = tyDefs ; ParsePreConstants Ds })
