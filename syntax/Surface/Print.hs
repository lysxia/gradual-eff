-- File generated by the BNF Converter (bnfc 2.9.4).

{-# LANGUAGE CPP #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
#if __GLASGOW_HASKELL__ <= 708
{-# LANGUAGE OverlappingInstances #-}
#endif

-- | Pretty-printer for Surface.

module Surface.Print where

import Prelude
  ( ($), (.)
  , Bool(..), (==), (<)
  , Int, Integer, Double, (+), (-), (*)
  , String, (++)
  , ShowS, showChar, showString
  , all, elem, foldr, id, map, null, replicate, shows, span
  )
import Data.Char ( Char, isSpace )
import qualified Surface.Abs
import qualified Data.Text

-- | The top-level printing method.

printTree :: Print a => a -> String
printTree = render . prt 0

type Doc = [ShowS] -> [ShowS]

doc :: ShowS -> Doc
doc = (:)

render :: Doc -> String
render d = rend 0 False (map ($ "") $ d []) ""
  where
  rend
    :: Int        -- ^ Indentation level.
    -> Bool       -- ^ Pending indentation to be output before next character?
    -> [String]
    -> ShowS
  rend i p = \case
      "["      :ts -> char '[' . rend i False ts
      "("      :ts -> char '(' . rend i False ts
      "{"      :ts -> onNewLine i     p . showChar   '{'  . new (i+1) ts
      "}" : ";":ts -> onNewLine (i-1) p . showString "};" . new (i-1) ts
      "}"      :ts -> onNewLine (i-1) p . showChar   '}'  . new (i-1) ts
      [";"]        -> char ';'
      ";"      :ts -> char ';' . new i ts
      t  : ts@(s:_) | closingOrPunctuation s
                   -> pending . showString t . rend i False ts
      t        :ts -> pending . space t      . rend i False ts
      []           -> id
    where
    -- Output character after pending indentation.
    char :: Char -> ShowS
    char c = pending . showChar c

    -- Output pending indentation.
    pending :: ShowS
    pending = if p then indent i else id

  -- Indentation (spaces) for given indentation level.
  indent :: Int -> ShowS
  indent i = replicateS (2*i) (showChar ' ')

  -- Continue rendering in new line with new indentation.
  new :: Int -> [String] -> ShowS
  new j ts = showChar '\n' . rend j True ts

  -- Make sure we are on a fresh line.
  onNewLine :: Int -> Bool -> ShowS
  onNewLine i p = (if p then id else showChar '\n') . indent i

  -- Separate given string from following text by a space (if needed).
  space :: String -> ShowS
  space t s =
    case (all isSpace t', null spc, null rest) of
      (True , _   , True ) -> []              -- remove trailing space
      (False, _   , True ) -> t'              -- remove trailing space
      (False, True, False) -> t' ++ ' ' : s   -- add space if none
      _                    -> t' ++ s
    where
      t'          = showString t []
      (spc, rest) = span isSpace s

  closingOrPunctuation :: String -> Bool
  closingOrPunctuation [c] = c `elem` closerOrPunct
  closingOrPunctuation _   = False

  closerOrPunct :: String
  closerOrPunct = ")],;"

parenth :: Doc -> Doc
parenth ss = doc (showChar '(') . ss . doc (showChar ')')

concatS :: [ShowS] -> ShowS
concatS = foldr (.) id

concatD :: [Doc] -> Doc
concatD = foldr (.) id

replicateS :: Int -> ShowS -> ShowS
replicateS n f = concatS (replicate n f)

-- | The printer class does the job.

class Print a where
  prt :: Int -> a -> Doc

instance {-# OVERLAPPABLE #-} Print a => Print [a] where
  prt i = concatD . map (prt i)

instance Print Char where
  prt _ c = doc (showChar '\'' . mkEsc '\'' c . showChar '\'')

instance Print String where
  prt _ = printString

printString :: String -> Doc
printString s = doc (showChar '"' . concatS (map (mkEsc '"') s) . showChar '"')

mkEsc :: Char -> Char -> ShowS
mkEsc q = \case
  s | s == q -> showChar '\\' . showChar s
  '\\' -> showString "\\\\"
  '\n' -> showString "\\n"
  '\t' -> showString "\\t"
  s -> showChar s

prPrec :: Int -> Int -> Doc -> Doc
prPrec i j = if j < i then parenth else id

instance Print Integer where
  prt _ x = doc (shows x)

instance Print Double where
  prt _ x = doc (shows x)

instance Print Surface.Abs.Ident where
  prt _ (Surface.Abs.Ident i) = doc $ showString (Data.Text.unpack i)
instance Print Surface.Abs.Constr where
  prt _ (Surface.Abs.Constr i) = doc $ showString (Data.Text.unpack i)
instance Print Surface.Abs.Program where
  prt i = \case
    Surface.Abs.Prog otydecls exp -> prPrec i 0 (concatD [prt 0 otydecls, prt 0 exp])

instance Print Surface.Abs.Exp where
  prt i = \case
    Surface.Abs.ELam pat tyann exp -> prPrec i 0 (concatD [doc (showString "\955"), prt 0 pat, prt 0 tyann, doc (showString "\8658"), prt 0 exp])
    Surface.Abs.EFix pat1 pat2 exp -> prPrec i 0 (concatD [doc (showString "\955fix"), prt 0 pat1, prt 0 pat2, doc (showString "\8658"), prt 0 exp])
    Surface.Abs.ELet pat type_ exp1 exp2 -> prPrec i 0 (concatD [doc (showString "let"), prt 0 pat, doc (showString ":"), prt 0 type_, doc (showString "="), prt 0 exp1, doc (showString "in"), prt 0 exp2])
    Surface.Abs.EIf exp1 exp2 exp3 -> prPrec i 0 (concatD [doc (showString "if"), prt 0 exp1, doc (showString "then"), prt 0 exp2, doc (showString "else"), prt 0 exp3])
    Surface.Abs.ELetP pat1 pat2 exp1 exp2 -> prPrec i 0 (concatD [doc (showString "let"), doc (showString "("), prt 0 pat1, doc (showString ","), prt 0 pat2, doc (showString ")"), doc (showString "="), prt 0 exp1, doc (showString "in"), prt 0 exp2])
    Surface.Abs.ELetU pat exp1 exp2 -> prPrec i 0 (concatD [doc (showString "let"), doc (showString "#"), prt 0 pat, doc (showString "="), prt 0 exp1, doc (showString "in"), prt 0 exp2])
    Surface.Abs.EHnd exp hnd -> prPrec i 9 (concatD [doc (showString "handle"), prt 0 exp, doc (showString "with"), prt 0 hnd, doc (showString "end")])
    Surface.Abs.EMch exp mchs -> prPrec i 9 (concatD [doc (showString "match"), prt 0 exp, doc (showString "with"), prt 0 mchs, doc (showString "end")])
    Surface.Abs.ENat n -> prPrec i 9 (concatD [prt 0 n])
    Surface.Abs.ETrue -> prPrec i 9 (concatD [doc (showString "true")])
    Surface.Abs.EFalse -> prPrec i 9 (concatD [doc (showString "false")])
    Surface.Abs.EVar id_ -> prPrec i 9 (concatD [prt 0 id_])
    Surface.Abs.EUnit -> prPrec i 9 (concatD [doc (showString "("), doc (showString ")")])
    Surface.Abs.EPerform id_ exp -> prPrec i 8 (concatD [doc (showString "!"), prt 0 id_, prt 9 exp])
    Surface.Abs.EApp exp1 exp2 -> prPrec i 8 (concatD [prt 8 exp1, prt 9 exp2])
    Surface.Abs.ECon constr exp -> prPrec i 8 (concatD [prt 0 constr, prt 9 exp])
    Surface.Abs.EOp exp1 op exp2 -> prPrec i 7 (concatD [prt 8 exp1, prt 7 op, prt 7 exp2])
    Surface.Abs.EWrap exp -> prPrec i 3 (concatD [doc (showString "#"), prt 3 exp])
    Surface.Abs.EPair exp1 exp2 -> prPrec i 2 (concatD [prt 3 exp1, doc (showString ","), prt 2 exp2])
    Surface.Abs.EAnn exp ctype -> prPrec i 1 (concatD [prt 1 exp, doc (showString ":"), prt 0 ctype])

instance Print Surface.Abs.Pat where
  prt i = \case
    Surface.Abs.PId id_ -> prPrec i 0 (concatD [prt 0 id_])
    Surface.Abs.PWild -> prPrec i 0 (concatD [doc (showString "_")])

instance Print Surface.Abs.Hnd where
  prt i = \case
    Surface.Abs.H pat exp hcl -> prPrec i 0 (concatD [doc (showString "|"), prt 0 pat, doc (showString "\8658"), prt 0 exp, prt 0 hcl])

instance Print Surface.Abs.HCl where
  prt i = \case
    Surface.Abs.HC id_ pat1 pat2 exp hcl -> prPrec i 0 (concatD [doc (showString "|"), doc (showString "!"), prt 0 id_, prt 0 pat1, prt 0 pat2, doc (showString "\8658"), prt 0 exp, prt 0 hcl])
    Surface.Abs.HCnil -> prPrec i 0 (concatD [])

instance Print [Surface.Abs.Mch] where
  prt _ [] = concatD []
  prt _ (x:xs) = concatD [doc (showString "|"), prt 0 x, prt 0 xs]

instance Print Surface.Abs.Mch where
  prt i = \case
    Surface.Abs.MCl constr pat exp -> prPrec i 0 (concatD [prt 0 constr, prt 0 pat, doc (showString "\8658"), prt 0 exp])

instance Print Surface.Abs.CType where
  prt i = \case
    Surface.Abs.CTyp eff type_ -> prPrec i 0 (concatD [prt 0 eff, prt 0 type_])

instance Print Surface.Abs.Eff where
  prt i = \case
    Surface.Abs.EffDyn -> prPrec i 0 (concatD [doc (showString "{"), doc (showString "?"), doc (showString "}")])
    Surface.Abs.EffDef -> prPrec i 0 (concatD [])
    Surface.Abs.EffSta efflist -> prPrec i 0 (concatD [doc (showString "{"), prt 0 efflist, doc (showString "}")])

instance Print Surface.Abs.EffList where
  prt i = \case
    Surface.Abs.EffNil -> prPrec i 0 (concatD [])
    Surface.Abs.EffCons id_ efflist -> prPrec i 0 (concatD [prt 0 id_, doc (showString ","), prt 0 efflist])
    Surface.Abs.EffSing id_ -> prPrec i 0 (concatD [prt 0 id_])

instance Print Surface.Abs.TyAnn where
  prt i = \case
    Surface.Abs.TAAnn type_ -> prPrec i 0 (concatD [doc (showString ":"), prt 0 type_])
    Surface.Abs.TANo -> prPrec i 0 (concatD [])

instance Print Surface.Abs.Type where
  prt i = \case
    Surface.Abs.TAny -> prPrec i 9 (concatD [doc (showString "?")])
    Surface.Abs.TVar id_ -> prPrec i 9 (concatD [prt 0 id_])
    Surface.Abs.TApp id_ type_ types -> prPrec i 8 (concatD [prt 0 id_, doc (showString "("), prt 0 type_, prt 0 types, doc (showString ")")])
    Surface.Abs.TArr type_ ctype -> prPrec i 0 (concatD [prt 1 type_, doc (showString "\8594"), prt 0 ctype])
    Surface.Abs.TProd type_1 type_2 -> prPrec i 1 (concatD [prt 2 type_1, doc (showString "\215"), prt 1 type_2])
    Surface.Abs.TSum tyalts -> prPrec i 9 (concatD [doc (showString "["), prt 0 tyalts, doc (showString "]")])

instance Print [Surface.Abs.Type] where
  prt _ [] = concatD []
  prt _ (x:xs) = concatD [doc (showString ","), prt 0 x, prt 0 xs]

instance Print [Surface.Abs.TyAlt] where
  prt _ [] = concatD []
  prt _ [x] = concatD [prt 0 x]
  prt _ (x:xs) = concatD [prt 0 x, doc (showString "|"), prt 0 xs]

instance Print Surface.Abs.TyAlt where
  prt i = \case
    Surface.Abs.TAlt constr type_ -> prPrec i 0 (concatD [prt 0 constr, prt 9 type_])

instance Print Surface.Abs.Op where
  prt i = \case
    Surface.Abs.OpMul -> prPrec i 7 (concatD [doc (showString "*")])
    Surface.Abs.OpAdd -> prPrec i 6 (concatD [doc (showString "+")])
    Surface.Abs.OpSub -> prPrec i 6 (concatD [doc (showString "-")])
    Surface.Abs.OpEq -> prPrec i 5 (concatD [doc (showString "==")])
    Surface.Abs.OpNEq -> prPrec i 5 (concatD [doc (showString "!=")])
    Surface.Abs.OpAnd -> prPrec i 4 (concatD [doc (showString "&&")])
    Surface.Abs.OpOr -> prPrec i 3 (concatD [doc (showString "||")])

instance Print Surface.Abs.OTyDecls where
  prt i = \case
    Surface.Abs.OTsome tydecls -> prPrec i 0 (concatD [prt 0 tydecls, doc (showString ";")])
    Surface.Abs.OTnone -> prPrec i 0 (concatD [])

instance Print [Surface.Abs.TyDecl] where
  prt _ [] = concatD []
  prt _ (x:xs) = concatD [prt 0 x, prt 0 xs]

instance Print Surface.Abs.TyDecl where
  prt i = \case
    Surface.Abs.TD id_ typarams type_ -> prPrec i 0 (concatD [doc (showString "newtype"), prt 0 id_, prt 0 typarams, doc (showString "="), prt 0 type_])

instance Print Surface.Abs.TyParams where
  prt i = \case
    Surface.Abs.TPnone -> prPrec i 0 (concatD [])
    Surface.Abs.TPone typaram -> prPrec i 0 (concatD [prt 0 typaram])
    Surface.Abs.TPmore typaram typarams -> prPrec i 0 (concatD [doc (showString "("), prt 0 typaram, prt 0 typarams, doc (showString ")")])

instance Print [Surface.Abs.TyParam] where
  prt _ [] = concatD []
  prt _ (x:xs) = concatD [doc (showString ","), prt 0 x, prt 0 xs]

instance Print Surface.Abs.TyParam where
  prt i = \case
    Surface.Abs.TP id_ -> prPrec i 0 (concatD [prt 0 id_])
