module Main where

open import SurfaceConstants using (PreConstants; fromPreConstants)

open import Type.Context using (∅)
open import Gradual2Core using (translate)
open import Surface2Gradual using (infer0)
open import Surface2 using (gradualProgram; showError)
open import Show
import Type
import Core
import Core.Progress2

open import Data.String.Base using (String; _++_)
open import Data.List.Base using (_∷_; [])
open import Data.Nat using (ℕ; zero; suc)
open import Data.Product using (_×_; _,_; proj₁; proj₂; Σ; ∃; Σ-syntax; ∃-syntax)
open import Surface.Parser using (Err; parseProgram)
open import Agda.Builtin.String using () renaming (primStringFromList to stringFromList)

open import Data.Sum.Base as Sum using (_⊎_; inj₁; inj₂)
import Data.Nat.Show as Nat

open import Function.Base using (case_of_)

open import IO.Base as IO
open import IO.Finite using (readFile; putStrLn)

open import System.Environment using (getArgs)
open import System.Exit using (die; exitFailure)

untilRightN : {A B : Set} → (A → IO (A ⊎ B)) → A → IO (ℕ × B)
untilRightN f x =
  let go = λ { (n , x) → Sum.map (suc n ,_) (n ,_) IO.<$> f x } in
  untilRight go (zero , x)

module _ (ℂ₀ : PreConstants) where
  ℂ = fromPreConstants ℂ₀

  open Type ℂ
  open Core ℂ
  open Core.Progress2 ℂ using (step; Red; Return; Blame; Unhandled)

  eval : ∀ {P : Typeᶜ} (let ⟨ _ ⟩ A = P) → ∅ ⊢ᶜ P → IO (ℕ × (∅ ⊢ᵛ A))
  eval = untilRightN λ M → case step M of λ where
    (Red {N = N} _) → pure (inj₁ N)
    (Return {V = V}) → pure (inj₂ V)
    (Blame _) → die "BLAME!"
    (Unhandled {e = e} _) → die ("Unhandled effect " ++ e)

main : Main
main = run do
  (fp ∷ []) ← getArgs
    where _ → putStrLn "Pass a single filepath"
  txt  ← readFile fp
  Err.ok t ← pure (parseProgram txt) where
    Err.bad msg → do
      putStrLn "PARSE FAILED\n"
      putStrLn (stringFromList msg)
      exitFailure
  inj₂ (ℂ₀ , A , M) <- pure (gradualProgram t) where
    inj₁ e → die ("Typecheck error: " ++ showError e)
  let open ShowType ℂ₀ using (showType; showVal)
      tC = translate _ M
  (n , v) <- eval ℂ₀ tC
  putStrLn (": " ++ showType A)
  putStrLn (showVal v)
  putStrLn (Nat.show n ++ " steps")
