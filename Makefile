OPTS_INCLUDE:=--include-path=syntax --include-path=src --include-path=exe
OPTS_COMPILE:=--compile --compile-dir=_build --ghc-flag=-Wwarn=incomplete-patterns --ghc-flag=-isyntax/ --ghc-flag=-outputdir --ghc-flag=_build

check:
	agda ${OPTS_INCLUDE} src/Everything.agda
	agda ${OPTS_INCLUDE} exe/Main.agda

build: happy
	agda ${OPTS_COMPILE} ${OPTS_INCLUDE} --ghc-flag="-o" --ghc-flag="efg" exe/Main.agda

build-parser: happy
	agda $(OPTS_COMPILE) --ghc-flag="-o" --ghc-flag="test-parser" syntax/Surface/Main.agda

happy:
	$(MAKE) -C syntax ./Surface/Lex.hs ./Surface/Par.hs

bnfc:
	bnfc --agda -m -d -o syntax syntax/surface.cf

.PHONY: build check happy bnfc build-parser
