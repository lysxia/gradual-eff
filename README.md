Gradual Eff
===========

A simple calculus with algebraic effects and gradual types,
formalized in Agda.

## References

- [*Well-Typed Programs Can't Be Blamed*](https://link.springer.com/content/pdf/10.1007/978-3-642-00590-9_1.pdf), by Philip Wadler and Robert Bruce Findler
- [*Threesomes, With and Without Blame*](https://era.ed.ac.uk/bitstream/handle/1842/3684/Threesomes,%20With%20and%20Without%20Blame.pdf), by Jeremy G. Siek and Philip Wadler
- [*Inferring Algebraic Effects*](https://lmcs.episciences.org/1004/pdf), by Matija Pretnar
- [The Eff language](https://www.eff-lang.org/)
