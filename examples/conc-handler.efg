-- Thread scheduler based on
-- Effective Concurrency through Algebraic Effects, Dolan et al., OCaml Workshop 2015
-- https://kcsrk.info/papers/effects_ocaml15.pdf
newtype item = queue → {print} unit
newtype queue = [ Empty unit | Cons (item × queue) ]
;
let empty : queue = # Empty () in
let push : item → {} queue → {} queue = λ x ⇒ λfix pushx q ⇒
  let # q = q in
  match q with
  | Empty _ ⇒ # Cons (x , empty)
  | Cons yq ⇒ let (y, q) = yq in # Cons (y, pushx q)
  end
in
let pop : queue → {fail} item × queue = λ q ⇒
  let # q = q in
  match q with
  | Empty _ ⇒ match ! fail () with end
  | Cons yq ⇒ yq
  end
in
let pop_run : queue → {print} unit = λ q ⇒
  handle pop q with
  | v ⇒ let (f, q) = v in let # f = f in f q
  | ! fail _ _ ⇒ ()
  end
in
let run_conc : (unit → {spawn, yield, print} unit) → {print} unit =
  let run_with : (unit → {spawn, yield, print} unit) → {} queue → {print} unit =
    λfix run_with f ⇒ λ q ⇒
      let go : queue → {print} unit = handle f () with
      | v ⇒ λ q ⇒ pop_run q
      | ! spawn g k ⇒ λ q ⇒ k () (push (# run_with g) q)
      | ! yield _ k ⇒ λ q ⇒ pop_run (push (# (λ q ⇒ k () q)) q)
      end in go q
  in λ f : unit → {spawn,yield,print} unit ⇒
  run_with f empty
in
let run_print : (unit → {print} unit) → {} nat = λ f : unit → {print} unit ⇒
  let go : nat → {} nat = handle f () with
  | _ ⇒ λ n : nat ⇒ n
  | ! print i k ⇒ λ n : nat ⇒ k () ((n * 10) + i)
  end in go 0
in
let print : nat → {spawn,yield, print} unit = λ n : nat ⇒
  let _ : unit = ! print n in
  ! yield ()
in
let thread : nat → {} unit → {spawn,yield, print} unit = λ n : nat ⇒ λ _ : unit ⇒
  let _ : unit = print n in
  let _ : unit = print n in
  let _ : unit = print n in
  ()
in
run_print (λ _ : unit ⇒
  run_conc (λ _ : unit ⇒
    let _ : unit = ! spawn (thread 1) in
    let _ : unit = ! spawn (thread 2) in
    let _ : unit = ! spawn (thread 3) in
    ()))
